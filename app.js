var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var timeout = require('connect-timeout');
var compression = require('compression');


var index = require('./routes/index');
var users = require('./routes/users');
var reports = require('./routes/reports');
var cctv = require('./routes/cctv');
var routes = require('./routes/routes');
var pois = require('./routes/poi')
var app = express();

//compress data for faster request
app.use(compression()); 


//set app timeout
app.use(timeout(300000));


//setting socket.io
var server = require('http').Server(app);
var io = require('socket.io')(server);
app.use(function(req, res, next) {
    res.io = io;
    next();
});



//key
app.set('key', 'mykey12345');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


// app.use((req,res,next)=>{
//     console.log(req.headers)
//     next();
// })

app.use('/users', users);
app.use('/reports', reports);
app.use('/cctv', cctv);
app.use('/routes', routes);
app.use('/poi', pois);


//set for reacrt env
app.get('*', function (request, response){
  response.sendFile(path.resolve(__dirname, 'public', 'index.html'))
})

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = { app: app, server: server };