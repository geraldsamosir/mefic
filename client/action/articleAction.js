export const SET_ARTICLE = 'SET_ARTICLE';
export const SET_ARTICLE_BY_ID = 'SET_ARTICLE_BY_ID';

///client extendtion for medium
//import Clay from 'clay-client'


function handleResponse(response) {
    if (response.ok) {
        return response.json();
    } else {
        let error = new Error(response.statusText);
        error.response = response;
        throw error;
    }
}



export function setArticle(articles) {
    return {
        type: SET_ARTICLE,
        articles
    }
}

export function setArticlebyid(article) {
    return {
        type: SET_ARTICLE_BY_ID,
        article
    }
}



export function fetchArticle() {
    return dispatch => {
        Clay.run('geraldsamosir/new-list-medium', { "username": "geraldhalomoansamosir" })
            .then((result) => {
                let post = result.payload.references.Post
                result.payload.references.Post = Object.keys(post).map((k) => post[k])
                let data = {
                    articles: result.payload.references.Post,
                }
                dispatch(setArticle(data.articles))
            })
    }
}

export function fetchArticlebyId(id) {
    return dispatch => {
        Clay.run('geraldsamosir/medium-full-page', {
                "username": "geraldhalomoansamosir",
                "idurl": id
            })
            .then((result) => {
                let data = {
                    article: result
                }
                dispatch(setArticlebyid(data.article))
            })
    }
}