export default new(class notif {
    comment(history){
     
     socket.on("comment",(data)=>{
            let audio =  new Audio("/sound/notif.mp3")
            if(data.conten_creator_email == localStorage.email && data.comment_users_id != localStorage.id){
                  
                 let urlParams = new URLSearchParams(window.location.search);
                 let notif =  urlParams.get("notif")
                 if(notif == undefined && data.comment_users_id != localStorage.id){
                     var notification = new Notification('Notifikasi', {
                        icon: data.users_urlphoto,
                        body: data.users_name+" mengomentari "+data.body+" pada report anda",
                    });
                    
                    notification.onclick = function () {
                        history.push(window.location.search+"&notif=true")
                        this.close()
                    }
                    audio.play()
                 }
                 else if(notif != undefined && data.comment_users_id != localStorage.id){
                      var notification = new Notification('Notifikasi', {
                        icon: data.users_urlphoto,
                        body: data.users_name+" mengomentari "+data.body+" pada report anda",
                    });
                    
                    notification.onclick = function () {
                        history.push(window.location.search+"&notif=true")
                        this.close()
                    }
                    audio.play()
                 }

            }   
    })
}
})
 