import React ,{Component} from 'react'
import ReactDOM from 'react-dom'

//import style
import "./index.css"

//app
import App from  './router'


ReactDOM.render(<App />, document.getElementById('app'))