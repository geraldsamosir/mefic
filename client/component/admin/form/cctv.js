import React ,{Component} from 'react'
import linkState from 'react-link-state';

export default class cctv extends Component{
    constructor(props){
        super(props)
        this.state ={
            form :{
                id : "",
                name_cctv :"",
                url_cctv :"",
                address :"",
                cctv_status :"",
                lat :"",
                long:"",
               cctv_groups_id:"1",
               cctv_condition :"",

            },
            category :[]
        }
    }

    componentDidMount() {
        this.getcctvbyid()
        this.getcategorycctv()
    }


    closecomponent(e){
         e.preventDefault();
        this.props.history.goBack()
    }

    getcategorycctv(){
        fetch('/cctv/group', {
            method :'GET',
        })
        .then(response => response.json())
        .then((response)=>{
             this.setState({
                 category : this.state.category.concat(response.result)
             })
        })
    }


    getcctvbyid(){
       
        let urlParams = new URLSearchParams(window.location.search);
        let edit =  urlParams.get('cctvid')||undefined
        if(edit != undefined){
            let _data = JSON.parse(localStorage.admin_list_cctv)
            let data =  _data.filter((data)=>{
                return data.cctv_id == edit
            })
            // data[0]
            this.setState({
                form :{
                    id : data[0].cctv_id,
                    name_cctv :data[0].cctv_name_cctv,
                    url_cctv :data[0].cctv_url_cctv,
                    address :data[0].cctv_address,
                    cctv_status :data[0].cctv_cctv_status,
                    lat :data[0].cctv_lat,
                    long:data[0].cctv_long,
                    cctv_groups_id:data[0].cctv_cctv_groups_id,
                    cctv_condition :data[0].cctv_cctv_condition
                }
            })
            let good = document.getElementById("good");
            let bad = document.getElementById("bad");
            if(data[0].cctv_cctv_condition == 2){
                bad.checked = true;
            }
            else if(data[0].cctv_cctv_condition ==1){

            }
            else{
                good.checked = true;
                this.setState({
                    cctv_cctv_condition :  0
                })
            }
        }
       
    }

    newcctv(e){
        e.preventDefault();
        fetch('/cctv',{
            method :'POST',
            headers : {
                "Content-Type" :"application/json", 
                email  :localStorage.email,
                token :  localStorage.token,
                roles: localStorage.roles
            },
            body : JSON.stringify(this.state.form)
        })
        .then(response => response.json())
        .then((response)=>{
            if(response.code == 201){
                this.props.history.goBack()
            }
        })
    }

    cctvstatus(status,value){
         this.setState({
              form:{
              cctv_condition :value,
              cctv_status : 0
              }
           })
    }

    updatecctv(e){
         e.preventDefault();
        let urlParams = new URLSearchParams(window.location.search);
        let edit =  urlParams.get('cctvid')||undefined
        fetch('/cctv/'+edit,{
            method :'PUT',
            headers : {
                "Content-Type" :"application/json", 
                email  :localStorage.email,
                token :  localStorage.token,
                roles: localStorage.roles
            },
            body : JSON.stringify(this.state.form)
        })
        .then(response => response.json())
        .then((response)=>{
            if(response.code == 200){
                this.props.history.goBack()
            }
        })
    }

    render(){
        let urlParams = new URLSearchParams(window.location.search);
        let edit =  urlParams.get('cctvid')||undefined
        return(
            <form>
                Nama cctv :<input className="form-control" valueLink={linkState(this,'form.name_cctv')}></input>
                <br/>
                URl cctv<input className="form-control" valueLink={linkState(this,'form.url_cctv')}></input>
                <br/>
                Alamat cctv:
                <input className="form-control" valueLink={linkState(this,'form.address')}></input>
                <br/>
                <div className="col-md-6">
                    Lat : 
                    <input className="form-control" valueLink={linkState(this,'form.lat')}></input>
                </div>
                 <div className="col-md-6">
                     Lng : 
                     <input className="form-control" valueLink={linkState(this,'form.long')}></input>
                </div>
                <br/><br/><br/>
                {/*<div>
                    CCTV Status : (jika lebih besar dari 0 maka cctv rusak)
                    <input  className="form-control" valueLink={linkState(this,'form.cctv_status')}></input>
                </div>*/}
                <br/>
             
                {
               
                (edit != undefined)?
                    <div>
                           Kondisi CCTV :
                        {
                        (this.state.form.cctv_condition == 2)?
                            <div>
                                
                            <input type="radio"  name="status"  id="good" onChange={this.cctvstatus.bind(this,this.state.form.cctv_condition,0)}/>
                             Bagus &nbsp;
                             <input type="radio"  name="status"  id="bad" onChange={this.cctvstatus.bind(this,this.state.form.cctv_condition ,2)} />
                             Rusak
                            </div>
                        :  <div>
                            <input type="radio"  name="status"  id="good" onChange={this.cctvstatus.bind(this,this.state.form.cctv_condition , 0)} />
                            bagus &nbsp;
                            <input type="radio" name="status" id="bad" onChange={this.cctvstatus.bind(this,this.state.form.cctv_condition,2)}/>
                            rusak
                            </div>
                        }
                    </div>
                    :""
                
                }
                <br/><br/>
                <div>
                    <button onClick={this.closecomponent.bind(this)} className="fl btn btn-danger">Cancel</button>
                    &nbsp;
                    <a href={"/apps?action=cctv&url="+edit} target="_blank" className="btn btn-info">Lihat CCTV</a>
                    {
                     (edit != undefined)?
                     
                    <button onClick={this.updatecctv.bind(this)} className="fr btn btn-primary">Simpan</button>

                    :<button onClick={this.newcctv.bind(this)} className="fr btn btn-primary">Tambahkan</button>
                    }
                </div>
                
            </form>
        )
    }
}