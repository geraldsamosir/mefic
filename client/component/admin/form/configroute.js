import React ,{Component} from 'react'
import linkState from 'react-link-state';


export default class confvalue extends Component{

    constructor(props){
        super(props)
        this.state = {
            form : {
                googlevalue : "",
                reportvalue :""
            }
        }
    }

    componentDidMount() {
       this.getconfig()
    }

    getconfig(){
         fetch("/routes/conf/confroute",{
            method :"GET",
             headers : {
                "Content-Type" :"application/json", 
                email  :localStorage.email,
                token :  localStorage.token,
                roles: localStorage.roles
            }
        })
         .then(response => response.json())
         .then((response)=>{
            
             if(response.code == 200){
                 this.setState({
                     form :{
                        googlevalue : response.result.googlevalue,
                        reportvalue  : response.result.reportvalue
                     }
                 })
             }
         })
    }


    updateconf(e){
         e.preventDefault();
         fetch("/routes/conf/confroute",{
            method :"PUT",
             headers : {
                "Content-Type" :"application/json", 
                email  :localStorage.email,
                token :  localStorage.token,
                roles: localStorage.roles
            },
            body : JSON.stringify(this.state.form)
        })
         .then(response => response.json())
         .then((response)=>{
              swal("updated!", "Bobot berhasil di Update", "success")
         })
    }
    render(){
        return (
            <div>
                <br/><br/><br/><br/><br/>
                <div className="container Asap" style={{background:"white"}}>
                    <h1>Configurasi Bobot</h1>
                    <form className="form" onSubmit={this.updateconf.bind(this)}>
                        <div className="col-md-6" style={{marginLeft:"auto", marginRight:"auto"}}>
                        Bobot Google map :<input className="form-control" valueLink={linkState(this,'form.googlevalue')}></input>
                        <br/>
                        Bobot Users Report<input className="form-control" valueLink={linkState(this,'form.reportvalue')}></input>
                        </div>
                        <br/>
                        <div className="col-md-12">
                            <br/><br/>
                        <button onClick={this.updateconf.bind(this)} className="btn btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        )
    }




}