import React ,{Component} from 'react'
import {Link} from "react-router-dom"
export default  class header extends Component{

    dologout(){
        if(localStorage.token != undefined){
            localStorage.removeItem("token");
            localStorage.removeItem("email");
            localStorage.removeItem("roles");
        }
         this.props.history.push('/')
    }

    render () {
        return (
            <div>
            <header className="bg-black-90 fixed w-100 ph3 pv3 pv4-ns Bowlby" style={{zIndex:15,padding:"1%"}}>
                <nav className="f6 fw6 ttu tracked">
                    { (localStorage.token == undefined)?
                     <Link to="/auth?action=login" className="f5 no-underline white bg-animate hover-bg-white hover-black inline-flex items-center pa3 ba border-box mr4 fr" style={{textDecoration:"none"}}>
                        <h5 className="pl1">Login</h5>
                     </Link>
                     :<a  onClick={this.dologout.bind(this)} className="f5 no-underline white bg-animate hover-bg-white hover-black inline-flex items-center pa3 ba border-box mr4 fr" style={{textDecoration:"none"}}>
                        <h5 className="pl1">Logout</h5>
                     </a>
                    }
                     <Link to="/management?action=users">
                     <img src="/images/logo.png" alt="" style={{width:"80px",height:"50px",marginRight:"30px"}}/>
                     </Link>

                     {/*right menu*/}
                     <Link to="/apps?action=beranda" className="f5 no-underline white bg-animate hover-bg-white hover-black inline-flex items-center pa3 ba border-box mr4 fr" style={{textDecoration:"none"}}>
                         <h5 className="pl1">Ke beranda aplikasi</h5>
                    </Link>
                    <Link to="/management?action=config" className="f5 no-underline white bg-animate hover-bg-white hover-black inline-flex items-center pa3 ba border-box mr4 fr" style={{textDecoration:"none"}}>
                         <h5 className="pl1">Config</h5>
                    </Link>
                     <Link to="/management?action=cctv" className="f5 no-underline white bg-animate hover-bg-white hover-black inline-flex items-center pa3 ba border-box mr4 fr" style={{textDecoration:"none"}}>
                         <h5 className="pl1">Cctv</h5>
                    </Link>
                     <Link to="/management?action=post" className="f5 no-underline white bg-animate hover-bg-white hover-black inline-flex items-center pa3 ba border-box mr4 fr" style={{textDecoration:"none"}}>
                         <h5 className="pl1">Report</h5>
                    </Link>
                     <Link to="/management?action=users" className="f5 no-underline white bg-animate hover-bg-white hover-black inline-flex items-center pa3 ba border-box mr4 fr" style={{textDecoration:"none"}}>
                         <h5 className="pl1">Users</h5>
                    </Link>
                </nav>
                 
            </header>
            </div>
        )
    }
}

