import React ,{Component} from 'react'
import {Link} from 'react-router-dom'
export default class cctv extends Component{

    constructor(props){
        super(props)
        this.state = {
            cctv : [],
             filter : {
                type :"",
                keyword: "",
                searchdisable :"true",
            },
            max: 5,
            pagefilter : {
                start : 0,
                end : 5
            }
        }
    }
    componentDidMount() {
        this.getcctv()

        socket.on("cctvbroken",()=>{
            this.setState({
                cctv :[]
            })
            this.getcctv()
        })
    }

     componentWillReceiveProps() {
         this.setState({
             cctv :[]
         })
         this.getcctv()
     }

    filterstatus(e){
        let _data = this.state.cctv.sort((a,b)=>{
            if(e.target.value > 0){
                return b.cctv_cctv_status  - a.cctv_cctv_status
            }
            else{
                return a.cctv_cctv_status  - b.cctv_cctv_status
            }
        })
        this.setState({
            cctv  : []
        })
        this.setState({
            cctv  : _data
        })
    }
    typeselect(e){
        
        this.setState({
            filter : {
                searchdisable  :false,
                type : e.target.value
            }
        })
        
    }
    searchcctv(e){
        this.setState({
            filter:{
                type: this.state.filter.type,
            }
        })
        if(e.target.value ==""){
            this.setState({
                cctv :[]
            })
            
            this.getcctv()
        }
        else{

            let _data  = this.state.cctv.filter((data)=>{
                return data[this.state.filter.type].match(e.target.value.toUpperCase())
            })
            this.setState({
                cctv :[]
            })
            this.setState({
                cctv : _data
            })
            
        }
        
    }

    deletecctv(id){
       
        fetch('/cctv/'+id,{
            method:"delete",
                headers : {
                    email  :localStorage.email,
                    token :  localStorage.token,
                    roles: localStorage.roles
                }
        })
        .then((response)=>{
            this.setState({
                cctv  :[]
            })
            
            this.getcctv()
            
        })
    }

    getcctv(){
        fetch('/cctv/',{
                method : "GET",
                headers : {
                    email  :localStorage.email,
                    token :  localStorage.token,
                    roles: localStorage.roles
                }
            })
            .then(response => response.json())
            .then((response)=>{
                if(response.code =200){
                   
                    this.setState({
                        cctv: this.state.cctv.concat(response.result)
                    })    
                    localStorage.setItem("admin_list_cctv",JSON.stringify(response.result))
                }
            })
    }

    pagechange(_start,_end){
        console.log("range")
        console.log(_start)
        console.log(_end)
        this.setState({
            pagefilter : {
                start : _start,
                end : _end
            }
        })
    }

    render(){
        let datapage =  this.state.cctv.slice(this.state.pagefilter.start,this.state.pagefilter.end)
        return(
            <div className="container Asap">
                <div style={{background:"white",marginTop:"10%"}}>
                    <Link className="btn btn-primary" to={"/management?action=cctv&form=new"}>Tambah CCTV</Link>
                    <br/><br/><br/>
                     <form className="form col-md-12">
                        <h5>Filter list cctv:</h5>
                        <div  className="col-md-3">
                            <select onChange={this.typeselect.bind(this)} className="form-control">
                                 <option value="" selected disabled>Pilih tipe filter</option>
                                 <option value="cctv_name_cctv"  >Nama CCTV</option>
                                 <option value="cctv_address"  >Alamat cctv</option>
                            </select>
                        </div>
                        <div className="col-md-6">
                             <input  disabled={this.state.filter.searchdisable} onChange={this.searchcctv.bind(this)} className="form-control" type="text" placeholder="Cari Report"/>
                        </div>
                        <div className="col-md-3">
                            <div className="">
                                <select onChange={this.filterstatus.bind(this)} className="form-control">
                                    <option value="" selected disabled>Urutkan dengan</option>
                                     <option value="0"  >Report Rusak tersedikit </option>
                                     <option value="1"  >Report Rusak terbanyak</option>
                                </select>
                            </div>
                        </div>
                        <div>
                            
                        </div>
                    </form>
                    <br/><br/><br/><br/><br/><br/>
                    <div >    
                   <table className="table">
                       <thead className="bg-info">
                           <th>No</th>
                           <th>Nama Cctv</th>
                           <th>Alamat Cctv</th>
                           <th>status cctv</th>
                           <th>Jumllah Report</th>
                           <th>Aksi</th>
                       </thead>
                       <tbody>
                           {
                               
                               datapage.map((data,index)=>{
                                   let statuscolor = "";
                                   if(data.cctv_cctv_condition == 0 && data.cctv_cctv_status == 0){
                                       statuscolor =""
                                   }
                                   else if (data.cctv_cctv_condition == 1 && data.cctv_cctv_status  > 0){
                                       statuscolor="bg-warning"
                                   }
                                   else if(data.cctv_cctv_condition == 2 ){
                                       statuscolor = "bg-danger"
                                   }
                                   return(
                                       <tr className={statuscolor}>
                                           <td>{this.state.pagefilter.start + index + 1}</td>
                                           <td>{data.cctv_name_cctv}</td>
                                           <td>{data.cctv_address}</td>
                                           <td>
                                               {
                                                   (data.cctv_cctv_condition > 0 )?
                                                      (data.cctv_cctv_condition == 2)?
                                                       "rusak"
                                                       :"Report rusak"
                                                   :"Bagus"
                                               }
                                            </td>
                                            <td>{data.cctv_cctv_status}</td>
                                           <td>
                                                <a href={"/apps?action=cctv&url="+data.cctv_url_cctv} target="_blank" className="btn btn-info"><i className="ion-eye"></i></a>
                                                &nbsp;
                                                <Link  to={"/management?action=cctv&form=edit&cctvid="+data.cctv_id} className="btn btn-success"><i className="ion-edit"></i></Link>
                                                 &nbsp;
                                                <button onClick={this.deletecctv.bind(this,data.cctv_id)} className="btn btn-danger"><i className="ion-trash-a"></i></button>
                                           </td>
                                       </tr>
                                   )
                               })
                           }
                       </tbody>
                   </table>
                   </div>
                   <div className="fr">
                   {
                       this.state.cctv.map((data,index)=>{
                            return (index % this.state.max == 0)?
                            <button className="btn btn-primary " style={{margin:"2px"}} onClick={this.pagechange.bind(this,index ,this.state.max * (index/this.state.max + 1))}>
                                {index/this.state.max + 1}
                               
                            </button>:""
                       })
                   }
                   </div>
                </div>
            </div>
        )
    }
}