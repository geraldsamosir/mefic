import React ,{Component} from 'react'

const moment = require('moment')

export default class Reports extends Component{

    constructor(props){
        super(props)
        this.state = {
            reports : [],
            filter : {
                type :"",
                keyword: "",
                searchdisable :"true",
                sortby:""
            },
            max: 5,
            pagefilter : {
                start : 0,
                end : 5
            }
        }
    }

    componentDidMount() {
        this.getreport()
        socket.on("reported",()=>{
            this.setState({
                reports :[]
            })
            this.getreport()
        })
    }

    typeselect(e){
        
        this.setState({
            filter : {
                searchdisable  :false,
                type : e.target.value
            }
        })
        
    }

    oderby(e){
     let _data = this.state.reports.sort((a,b)=>{
            return b[e.target.value]  - a[e.target.value]
     })
     this.setState({
         reports  : []
     })
     this.setState({
         reports  : _data
     })
    }
    searchreport(e){
        this.setState({
            filter:{
                type: this.state.filter.type,
            }
        })
        if(e.target.value ==""){
            this.setState({
                reports :[]
            })
            
            this.getreport()
        }
        else{

            let _data  = this.state.reports.filter((data)=>{
                return data[this.state.filter.type].match(e.target.value)
            })
            this.setState({
                reports :[]
            })
            this.setState({
                reports : _data
            })
            
        }
        
    }

    deletereports(id){
       
        fetch('/reports/'+id,{
            method:"delete",
                headers : {
                    email  :localStorage.email,
                    token :  localStorage.token,
                    roles: localStorage.roles
                }
        })
        .then((response)=>{
            this.setState({
                reports  :[]
            })
            this.getreport()
            
        })
    }
    getreport(){
        fetch('/reports?type=admin',{
                method : "GET",
                headers : {
                    email  :localStorage.email,
                    token :  localStorage.token,
                    roles: localStorage.roles
                }
            })
            .then(response => response.json())
            .then((response)=>{
                if(response.code =200){
                   
                    this.setState({
                        reports : this.state.reports.concat(response.result)
                    })    
                }
            })
    }

    pagechange(_start,_end){
        console.log("range")
        console.log(_start)
        console.log(_end)
        this.setState({
            pagefilter : {
                start : _start,
                end : _end
            }
        })
    }

    render(){
        let datapage =  this.state.reports.slice(this.state.pagefilter.start,this.state.pagefilter.end)
        return(
            <div className="container Asap">
                <div style={{background:"white",marginTop:"10%"}}>
                    <form className="form col-md-12">
                        <h5>Filter List report:</h5>
                        <div  className="col-md-3">
                            <select onChange={this.typeselect.bind(this)} className="form-control">
                                 <option value="" selected disabled>Pilih tipe filter</option>
                                 <option value="reports_name_report"  >Nama report</option>
                                 <option value="category_reports_name_category_report"  >Kategory Report</option>
                                 <option value="users_username"> Username Reporter</option>
                            </select>
                        </div>
                        <div className="col-md-6">
                             <input  disabled={this.state.filter.searchdisable} onChange={this.searchreport.bind(this)} className="form-control" type="text" placeholder="Cari Report"/>
                        </div>
                        <div className="col-md-3">
                            <div className="">
                                <select onChange={this.oderby.bind(this)} className="form-control">
                                    <option value="" selected disabled>Urutkan dengan</option>
                                     <option value="reports_like"  >Like</option>
                                     <option value="reports_dislike"  >Dislike</option>
                                </select>
                            </div>
                        </div>
                        <div>
                            
                        </div>
                    </form>
                    <br/><br/><br/><br/><br/><br/>
                  <div >    
                   <table className="table">
                       <thead className="bg-info">
                            <th>No</th>
                            <th>Nama Report</th>
                            <th>Kategory Report</th>
                            <th> Username Reporter</th>
                            <th> Like </th>
                            <th> Dislike </th>
                            <th> Waktu Report </th>
                            <th>Aksi</th>
                       </thead>
                       <tbody>
                            {
                                datapage.map((data,index)=>{
                                    return(
                                        <tr key={index}>
                                            <td>
                                                { this.state.pagefilter.start +index +1}
                                            </td>
                                            <td>
                                                
                                                    {data.reports_name_report}
                                                
                                            </td>
                                            <td>
                                                {data.category_reports_name_category_report}
                                            </td>
                                            <td>
                                                {data.users_username}
                                            </td>
                                            <td>
                                                {data.reports_like}
                                            </td>   
                                            <td>
                                                {data.reports_dislike}
                                            </td>
                                            <td>
                                                {moment(data.reports_created_at).format('DD-MM-YYYY HH:mm:ss')}  
                                            </td>
                                            <td>
                                                {/*<button  className="btn btn-success"><i className="ion-edit"></i></button>*/}
                                                <a href={"/apps?action=beranda&reportid="+data.reports_id+"&comment=true"} target="_blank" className="btn btn-info"><i className="ion-eye"></i></a>
                                                &nbsp;&nbsp;
                                                <button onClick={this.deletereports.bind(this,data.reports_id)} className="btn btn-danger"><i className="ion-trash-a"></i></button>
                                            </td>
                                        </tr>
                                    )
                                })
                            }
                       </tbody>
                   </table>
                   </div>
                    <div className="fr">
                   {
                       this.state.reports.map((data,index)=>{
                            return (index % this.state.max == 0)?
                            <button className="btn btn-primary " style={{margin:"2px"}} onClick={this.pagechange.bind(this,index ,this.state.max * (index/this.state.max + 1))}>
                                {index/this.state.max + 1}
                               
                            </button>:""
                       })
                   }
                   </div>
                </div>
            </div>
        )
    }
}