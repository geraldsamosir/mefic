import React ,{Component} from 'react'

export default class Users extends Component{

    constructor(props){
        super(props)
        this.state = {
            users : [],
            filter : {
                type :"",
                keyword: "",
                searchdisable :"true",
                sortby:""
            },
            max: 5,
            pagefilter : {
                start : 0,
                end : 5
            }
        }
    }
    componentDidMount() {
        this.getusers()
        socket.on("reported",()=>{
            this.setState({
                users :[]
            })
            this.getusers()
        })
    }


    typeselect(e){
        
        this.setState({
            filter : {
                searchdisable  :false,
                type : e.target.value
            }
        })
        
    }

    oderby(e){
     let _data = this.state.users.sort((a,b)=>{
            return b[e.target.value]  - a[e.target.value]
     })
     this.setState({
         users  : []
     })
     this.setState({
         users  : _data
     })
    }
    searchusers(e){
        this.setState({
            filter:{
                type: this.state.filter.type,
            }
        })
        if(e.target.value ==""){
            this.setState({
                users :[]
            })
            
            this.getusers()
        }
        else{

            let _data  = this.state.users.filter((data)=>{
                return data[this.state.filter.type].match(e.target.value)
            })
            this.setState({
                users :[]
            })
            this.setState({
                users : _data
            })
            
        }
        
    }

    deleteusers(id){
       
        fetch('/users/'+id,{
            method:"delete",
                headers : {
                    email  :localStorage.email,
                    token :  localStorage.token,
                    roles: localStorage.roles
                }
        })
        .then((response)=>{
            this.setState({
                users  :[]
            })
            this.getusers()
            
        })
    }
    getusers(){
        fetch('/users/',{
                method : "GET",
                headers : {
                    email  :localStorage.email,
                    token :  localStorage.token,
                    roles: localStorage.roles
                }
            })
            .then(response => response.json())
            .then((response)=>{
                if(response.code =200){
                    this.setState({
                        users : response.result
                    })    
                }
            })
    }

      pagechange(_start,_end){
        console.log("range")
        console.log(_start)
        console.log(_end)
        this.setState({
            pagefilter : {
                start : _start,
                end : _end
            }
        })
    }

    render(){
        let datapage =  this.state.users.slice(this.state.pagefilter.start,this.state.pagefilter.end)
        return(
            <div className="container Asap" >
                <div style={{background:"white",marginTop:"10%"}}>
                     <form className="form col-md-12" >
                        <h5>Filter List User yang telah report:</h5>
                        <div  className="col-md-3">
                            <select onChange={this.typeselect.bind(this)} className="form-control">
                                 <option value="" selected disabled>Pilih tipe filter</option>
                                 <option value="username"  >Username</option>
                                 <option value="email"  >Email</option>
                            </select>
                        </div>
                        <div className="col-md-6">
                             <input  disabled={this.state.filter.searchdisable} onChange={this.searchusers.bind(this)} className="form-control" type="text" placeholder="Cari Report"/>
                        </div>
                        <div className="col-md-3">
                            <div className="">
                                <select onChange={this.oderby.bind(this)} className="form-control">
                                    <option value="" selected disabled>Urutkan dengan</option>
                                     <option value="Like"  >Like</option>
                                     <option value="Dislike"  >Dislike</option>
                                </select>
                            </div>
                        </div>
                        <div>
                            
                        </div>
                    </form>
                    <br/><br/><br/><br/><br/><br/>
                    <div style={{height:"300px"}}>    
                   <table className="table">
                       <thead className="bg-info">
                           <th>No</th>
                           <th>username</th>
                           <th>Email</th>
                           <th>Status</th>
                           <th>Like</th>
                           <th>Dislike</th>
                           <th>no telephone/ hp</th>
                           <th>alamat</th>
                           <th>aksi</th>
                       </thead>
                       <tbody>
                           {
                            datapage.map((data, index)=>{
                                return(
                                    <tr key={index}>
                                    <td>
                                        {this.state.pagefilter.start + index + 1}
                                    </td>
                                    <td>
                                        
                                        {data.username}
                                        
                                    </td>

                                    <td>
                                        {data.email}
                                    </td>
                                    <td>
                                        {
                                            (data.roles ==2) ?"Admin":"Users"
                                        }
                                    </td>
                                    <td>
                                        {data.Like}
                                    </td>
                                    <td>
                                        {data.Dislike}
                                    </td>
                                    <td>
                                        {data.phone || "-"}
                                    </td>
                                    

                                    <td>
                                        {data.address || "-"}
                                    </td>
                                    <td>
                                        <button onClick={this.deleteusers.bind(this,data.id)} className="btn btn-danger"><i className="ion-trash-a"></i></button>
                                    </td>
                                    </tr>
                                )
                            })
                           }
                       </tbody>
                   </table>
                   </div>
                    <div className="fr">
                   {
                       this.state.users.map((data,index)=>{
                            return (index % this.state.max == 0)?
                            <button className="btn btn-primary " style={{margin:"2px"}} onClick={this.pagechange.bind(this,index ,this.state.max * (index/this.state.max + 1))}>
                                {index/this.state.max + 1}
                               
                            </button>:""
                       })
                   }
                   </div>
                </div>
            </div>
        )
    }
}