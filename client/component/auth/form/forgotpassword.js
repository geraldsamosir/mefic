import React ,{Component} from 'react'
import {Link} from "react-router-dom"
import linkState from 'react-link-state';


export default class ForgotForm extends Component{
    constructor(props){
        super(props)
        this.state  ={
            form :{
                email : ""
            }
        }
    }
    doreset(){
        if(this.state.form.email == ""){
            swal("Error !","Email tidak boleh  kosong!", "error")
        }
        else{
            fetch('/users/forgotpassword/'+this.state.form.email,{
               method: 'GET',
               headers :{
                   "Content-Type" :"application/json"
               }
            })
            .then(response => response.json())
            .then((response)=>{
                if(response.code == 200){
                     swal("Sukses !",""+response.message+"!", "success")
                }
                if(response.code ==404){
                     swal("Error !",""+response.message+"!", "error")
                }
               
            })
            
        }

    }
    render(){
        return(
            <div className="container">
                <center className="row">
                 <from onSubmit={this.doreset.bind(this)} className="col-md-5 ml30" style={{marginLeft:"30%"}}>
                        <center>
                            <h3><b>Lupa password</b></h3>
                        </center>
                        <br/>
                        <h5>Reset Password</h5>
                        <br/>
                        <center>
                        <div className="form-group">
                            <input valueLink={linkState(this,'form.email')} type="email" className="form-control" id="email" placeholder="Email"/>
                        </div>
                            <button onClick={this.doreset.bind(this)} className="f7 no-underline black bg-animate hover-bg-black hover-white inline-flex items-center pa3 ba border-box mr4 ">
                                <h5>Reset</h5>
                            </button>
                        </center>
                    </from>
                    </center>
                </div>
        )
    }
}