import React ,{Component} from 'react'
import {Link} from "react-router-dom"
import linkState from 'react-link-state';


export default class Login extends Component{
    constructor(props){
        super(props)
        this.state={
            form : {
                email : "",
                password : ""
            }
        }

    }

    dologin(){
        if(this.state.form.email == ''  || this.state.form.password == ''){
               swal("Error !","Email dan   Password tidak boleh kosong!", "error")
        }
        else{
             fetch("/users/login",{
               method: 'POST',
               headers :{
                   "Content-Type" :"application/json"
               },
                body : JSON.stringify({
                    email :this.state.form.email,
                    password  : this.state.form.password
                })
            })
            .then(response => response.json())
            .then((response)=>{
                
                if(response.code ==200){
                    //swal("Sukses!", " Berhasil login!", "success")

                    //set token and mail in localstorage
                    localStorage.setItem("token",response.result.authtoken)
                    localStorage.setItem("email" , response.result.email)
                    localStorage.setItem("roles",response.result.roles)
                    localStorage.setItem("id",response.result.id)
                    localStorage.setItem("urlphoto", (response.result.urlphoto =="")?"/images/user-default.png":response.result.urlphoto)
                    localStorage.setItem("username",response.result.username)
                    //kembali ke halaman sebelumnya
                    this.props.history.goBack()

                }
                else if(response.code = 403){
                    swal("Error !",""+response.message+"", "error")
                }
            })
            .catch((err)=>{
                console.log(err)
                swal("Error !","Server Eror", "error")
            })
        }
    }

    render(){
        return(
            <div className="fl w-50 ">
                 <from onSubmit={this.dologin.bind(this)} className="col-md-10">
                            <center>
                                <h3><b>Sudah punya akun ?</b></h3>
                            </center>
                            <br/>
                            <h5>Login</h5>
                            <br/>
                            <center>
                            <div className="form-group">
                                <input valueLink={linkState(this,'form.email')} type="email" className="form-control" id="email" placeholder="Email"/>
                            </div>
                            <div className="form-group">
                                <input valueLink={linkState(this,'form.password')} type="password" className="form-control" id="password" placeholder="Password"/>
                            </div>
                            <Link className="fl" to="/auth?action=forgotpassword">
                                Lupa Password ?
                            </Link>
                              <button onClick={this.dologin.bind(this)} className="f5 no-underline black bg-animate hover-bg-black hover-white inline-flex items-center pa3 ba border-box mr4 fr">
                                  <h5>Login</h5>
                            </button>
                            </center>
                        </from>
                </div>
        )
    }
}