import React ,{Component} from 'react'
import {Link} from "react-router-dom"
import linkState from 'react-link-state';
 
export default class Register extends Component{
    constructor(props){
        super(props)
        this.state = {
            form : {
                username :"",
                email  :"",
                password :"",
                confirmpassword :""
            }
        }
    }

    doregister(){
        if(this.state.form.username == "" || 
            this.state.form.email == ""||
            this.state.form.password == "" ||
            this.state.confirmpassword ==""){
                 swal("Error !","Form Register tidak boleh ada yang kosong!", "error")
        }
        else if(this.state.form.password.length < 4){   
            swal("Error !","Password harus lebih dari 4 karakter", "error")
        }
        else if(this.state.form.password != this.state.form.confirmpassword){
                swal("Error !","Password dan  Konformasi Password tidak sama!", "error")
        }
        else{
             fetch("/users",{
               method: 'POST',
               headers :{
                   "Content-Type" :"application/json"
               },
                body : JSON.stringify({
                    email :this.state.form.email,
                    password  : this.state.form.password,
                    username :this.state.form.username
                })
             })
            .then(response => response.json())
            .then((response)=>{
                if(response.code ==201){
                    swal("Sukses!", " Silahkan Cek Email anda untuk aktivasi akun!", "success")
                }
                else if(response.code == 409){
                    swal("Error !",""+response.message+"", "error")
                }
            })
             .catch((err)=>{
                console.log(err)
                swal("Error !","Server Eror", "error")
            })
            
        }

    }

    render(){
        return(
             <div className="fl w-50 ">
                <from onSubmit={this.doregister.bind(this)} className="col-md-10">
                            <center>
                                <h3><b>Pengguna baru ?</b></h3>
                            </center>
                            <br/>
                            <h5>Register</h5>
                            <br/>
                            <center>
                             <div className="form-group">
                                <input valueLink={linkState(this,'form.username')} type="text" className="form-control" id="nama" placeholder="nama"/>
                            </div>  
                            <div className="form-group">
                                <input valueLink={linkState(this,'form.email')} type="email" className="form-control" id="email" placeholder="Email"/>
                            </div>
                            <div className="form-group">
                                <input valueLink={linkState(this,'form.password')} type="password" className="form-control" id="password" placeholder="Password"/>
                            </div>
                            <div className="form-group">
                                <input valueLink={linkState(this,'form.confirmpassword')} type="password" className="form-control" id="password" placeholder="Konfirmasi password"/>
                            </div>
                              <button onClick={this.doregister.bind(this)} className="f5 no-underline black bg-animate hover-bg-black hover-white inline-flex items-center pa3 ba border-box mr4 fr">
                                  <h5>Register</h5>
                            </button>
                            </center>
                        </from>
                    </div>
        )
    }
}
                      