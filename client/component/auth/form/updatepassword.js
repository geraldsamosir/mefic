import React ,{Component} from 'react'
import {Link} from "react-router-dom"

//linking value from form 
import linkState from 'react-link-state';




export default class ForgotForm extends Component{

    constructor(props){
        super(props)
        this.state = {
            email : "Email",
            urlphoto : "",
            username : "Username",
            form  : {
                password : "",
                confirmpassword: ""
            },
            token :""
        }
    }
    
    componentWillMount() {

        //next change to redux
        let urlParams = new URLSearchParams(window.location.search);
        let  _token = urlParams.get('token')
        console.log(this.state.token);
        
        fetch('/users/password/'+_token, {
            method: 'get'
        })
        .then(response => response.json())
        .then((response)=>{
            if(response.result){
                this.setState({
                    email : response.result.email,
                    urlphoto : (response.result.urlphoto!='' && response.result.urlphoto!=null )?response.result.urlphoto : "/images/user-default.png",
                    username : response.result.username,
                    token  : response.result.token

                })
            }
            else{
                this.setState({
                    urlphoto :  "/images/user-default.png",
                    token : _token
                })
                this.props.history.push('/apps?action=beranda');

                
            }
        
        
        })
        
    }

    updatepasssword(){
        if(this.state.form.password != this.state.form.confirmpassword){
          swal("Error !","Password dan  Konformasi Password tidak sama!", "error")
        }
        else if(this.state.form.password =="" || this.state.form.confirmpassword ==""){
            swal("Error !","Password dan  Konformasi Password tidak boleh kosong!", "error")
        }
        else if(this.state.form.password.length < 4){
            
            swal("Error !","Password harus lebih dari 4 karakter", "error")
        }
        else{
            fetch("/users/password/update",{
               method: 'PUT',
               headers :{
                   "Content-Type" :"application/json"
               },
                body : JSON.stringify({
                    password :this.state.form.password,
                    token  : this.state.token
                })
            })
            .then(response => response.json())
            .then((response)=>{
                if(response.code ==200){
                    swal("updated!", "Password berhasil di update silahkan login!", "success")
                }
            })
            .catch((err)=>{
                console.log(err)
                swal("Error !","Server Eror", "error")
            })
        }
    }   
    render(){
        return(
            <div className="container">
                <center className="row">
                 <from onSubmit={this.updatepasssword.bind(this)} className="col-md-5 ml30" style={{marginLeft:"30%"}}>
                        
                        <img src={this.state.urlphoto} className="br-100 ba  dib"
                            style={{width:"200px",height:"200px"}}/>
                        <center>
                            <h3><b>{this.state.username}</b></h3>
                            <p><b>{this.state.email}</b></p>
                        </center>
                        <center>
                        <div className="form-group">
                            <input valueLink={linkState(this,'form.password')} type="password" className="form-control" id="email" placeholder="Password"/>
                        </div>
                        <div className="form-group">
                            <input valueLink={linkState(this,'form.confirmpassword')} type="password" className="form-control" id="email" placeholder="Konfirmasi password"/>
                        </div>
                            <button onClick={this.updatepasssword.bind(this)} className="f5 no-underline black bg-animate hover-bg-black hover-white inline-flex items-center pa3 ba border-box mr4 fr">
                                <h5>Update password</h5>
                            </button>
                        </center>
                    </from>
                    </center>
                </div>
        )
    }
}