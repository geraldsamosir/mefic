import React ,{Component} from 'react'
import {Link} from "react-router-dom"

//component
import LoginForm  from  './form/login'
import RegisterForm  from  './form/register'

export default  class Home extends Component{
    render () {
        return (
            <div className="container">
                <LoginForm history={this.props.history} />                
                <RegisterForm history={this.props.history} />
            </div>
        )
    }
}

