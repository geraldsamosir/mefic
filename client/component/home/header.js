import React ,{Component} from 'react'
import {Link} from "react-router-dom"
export default  class header extends Component{

    dologout(){
        if(localStorage.token != undefined){
            localStorage.removeItem("token");
            localStorage.removeItem("email");
            localStorage.removeItem("roles");
        }
        this.forceUpdate();
    }

    render () {
        return (
            <div>
            <header className="bg-black-90 fixed w-100 ph3 pv3 pv4-ns ph4-m ph5-l" style={{zIndex:15,padding:"1%",background:"#3FBAC2"}}>
                <nav className="f6 fw6 ttu tracked">
                    { (localStorage.token == undefined)?
                     <Link to="/auth?action=login" className="f5 no-underline white bg-animate hover-bg-white hover-black inline-flex items-center pa3 ba border-box mr4 fr" style={{textDecoration:"none"}}>
                        <h5 style={{marginTop:"25%",marginBottom:"25%",textAlign:"center"}} className="pl1 Bowlby">Login</h5>
                     </Link>
                     :<a  onClick={this.dologout.bind(this)} className="f5 no-underline white bg-animate hover-bg-white hover-black inline-flex items-center pa2 ba border-box mr4 fr" style={{textDecoration:"none"}}>
                        <h5 style={{marginTop:"20%",textAlign:"center"}} className="pl1 Bowlby">Logout <br/><br/></h5>
                     </a>
                    }
                     <Link to="/">
                         {/*<h1 className="link dim white dib mr3">Logo</h1>*/}
                         <img src="/images/logo.png" alt="" style={{width:"80px",height:"50px",marginRight:"30px"}}/>
                     </Link>
                     &nbsp;&nbsp;&nbsp;
                     <Link to="/apps?action=beranda"><h5 className="link dim white dib mr3 Bowlby">Beranda</h5></Link>

                     {/*right menu*/}
                     {
                         (localStorage.roles ==2)?
                         <Link to="/management?action=users" className="f5 no-underline white bg-animate hover-bg-white hover-black inline-flex items-center pa2 ba border-box mr4 fr" style={{textDecoration:"none"}}>
                            <h5 style={{marginTop:"13%",textAlign:"center"}} className="pl1 Bowlby">Ke halaman <br/> admin</h5>
                        </Link>
                        :""
                     }
                       <a href="http://www.smartsurvey.co.uk/s/BKDX4/" target="_blank" className="f5 no-underline white bg-animate hover-bg-white hover-black inline-flex items-center pa2 ba border-box mr4 fr" style={{textDecoration:"none"}}>
                         <h5 style={{marginTop:"15%",textAlign:"center"}} className="pl1 Bowlby">Kuesioner <br/><br/></h5>
                    </a>
                     <Link to="/apps?action=pointfinder" className="f5 no-underline white bg-animate hover-bg-white hover-black inline-flex items-center pa2 ba border-box mr4 fr" style={{textDecoration:"none"}}>
                         <h5 style={{marginTop:"20%",textAlign:"center"}}  className="pl1 Bowlby">Point <br/> Finder</h5>
                    </Link>
                     {
                    (localStorage.token != undefined)?
                    <span>
                    
                    <Link to="/auth?action=updateprofil" className="f7 no-underline white bg-animate hover-bg-white hover-black inline-flex items-center pa2 border-box mr4 fr" style={{textDecoration:"none"}}>
                         <h1 className="pl1 ion-person" style={{fontSize: "35px",margin:0}}></h1>
                    </Link>
                    {
                     (location.pathname !="/")?
                    <Link to={window.location.search+"&notif=true"} className="f7 no-underline white bg-animate hover-bg-white hover-black inline-flex items-center pa3 border-box mr4 fr" style={{textDecoration:"none"}}>
                         <h1 className="pl1 ion-ios-bell" style={{fontSize: "35px",margin:0}}></h1>
                    </Link>
                    :""
                    }
                    </span>
                    :
                    ""
                     }
                      <Link to="/apps?action=cctv" className="f5 no-underline white bg-animate hover-bg-white hover-black inline-flex items-center pa3  border-box mr4 fr" style={{textDecoration:"none"}}>
                         <h5 className="pl1 ion-videocamera" style={{fontSize: "35px",margin:0}}></h5>
                    </Link>
                </nav>
                 
            </header>
            </div>
        )
    }
}

