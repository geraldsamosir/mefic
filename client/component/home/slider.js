import React ,{Component} from 'react'

//component
import Swiper from 'react-id-swiper';

export default  class slider extends Component{
    render () {
        //param slider
        let params = {
            paginationClickable: true,
            spaceBetween: 30,
            autoplay:5000
        }
        return (
             <div className="fl w-100 w-50-l pa3-m pa4-l mb3 mb5-l">
                   <Swiper {...params} >
                       <div>
                          <img className="sliderhome" src="https://static.keepo.me//keepo.me-4112.jpg" alt=""  style={{height:"250px",width:"900px"}}/>
                       </div>
                       <div>
                           <img className="sliderhome" src="https://ceritagie.files.wordpress.com/2013/04/9.jpg" alt="" style={{height:"250px",width:"900px"}}/>
                       </div>
                    </Swiper>
                    </div>
        )
    }
}

