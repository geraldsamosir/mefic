import React ,{Component} from 'react'

import {Gmaps, Marker, InfoWindow, Circle} from 'react-gmaps';

import {browserHistory} from 'react-router';


export default class beranda extends  Component{
     constructor(props){
        super(props)
        this.state ={
            info :{
                 name : "Medan",
                 url : "",
                 lat : 3.589129,
                 long :98.641836
            },
            gmaps :{
                    lat : 3.567680999999999,
                    lng  :98.64743
                },
            zoom :12,
            Marker : [],
        }
    }

    componentWillReceiveProps() {
      
       this.getreport()
    }

    componentDidMount() {
        this.getreport()
        this.getconfigroute()
         socket.on("reported",()=>{
            this.setState({
                Marker :[]
            })
            this.getreport()
        })

        socket.on("confupdate",()=>{
            this.getconfigroute()
        })
        socket.on("reportupdate",()=>{
            setTimeout(()=>{
                console.log("report update")
                this.getreport()
             },1000)    

        })
    }

    getconfigroute(){
        fetch("/routes/conf/confroute",{
            method :"GET",
             headers : {
                "Content-Type" :"application/json", 
                email  :localStorage.email,
                token :  localStorage.token,
                roles: localStorage.roles
            }
        })
         .then(response => response.json())
         .then((response)=>{
            
             if(response.code == 200){
                 localStorage.setItem("googlevalue",response.result.googlevalue)
                 localStorage.setItem("reportvalue",response.result.reportvalue)
             }
         })
    }
    
    getreport(){
        fetch("/reports/",{
            method :"GET"
        })
        .then((response)=>response.json())
        .then((response)=>{
            if(response.code ==200)
            
            this.setState({
                Marker : response.result
            })
            localStorage.setItem("client_report",JSON.stringify(response.result))

        })
    }

    onMapCreated(map) {
        if (navigator.geolocation) {
             setInterval(()=>{
                navigator.geolocation.getCurrentPosition((position) => {
                localStorage.setItem("lat",position.coords.latitude)
                localStorage.setItem("lng",position.coords.longitude)    
            });
            
            },5000)
            
        }
    
    }
     onClick(obj) {
   
        this.setState({
            info :{
                 name : "CCTV ASOKA GAGAK HITAM 1",
                 url :"/apps?action=cctv&url=test",
                 lat : 3.562668,
                 long : 98.62617
            },
            gmaps :{
                    lat :3.562668 ,
                    lng  :98.62617
                }
        })
    }
    windowclick(id){
        this.props.history.push(window.location.search+'&reportid='+id);

    }

    render(){
     let urlParams = new URLSearchParams(window.location.search);
    let routes =  urlParams.get('tracking')  || undefined


    const reportmark  =this.state.Marker.map((data)=>{return(<Marker                   
                    lat={data.reports_lat}
                    lng={data.reports_lng} 
                    icon ={data.category_reports_icon_url}
                    onClick={this.windowclick.bind(this,data.reports_id)}
                  //onClick={this.onClick.bind(this,{})}
                   data-toggle="tooltip"
                    title={data.category_reports_name_category_report}
                   />  )
    })
    if(routes != undefined){
         let _end  = JSON.parse(localStorage.step) ||""
        _end =  _end[_end.length -1].end_location  ||""
        //console.log(_end)
          const start  =(<Marker                   
                            lat={localStorage.start_lat}
                            lng={localStorage.start_lng} 
                            icon ="/images/car.png"
                        data-toggle="tooltip"
                            title={"Mulai"}/>  )
                const end = (<Marker                   
                            lat={_end.lat}
                            lng={_end.lng} 
                            icon ="/images/finish.png"
                        data-toggle="tooltip"
                            title={"Akir"}
                            />  )
            return(
              <Gmaps
                width={'100%'}
                height={'700px'}
                lat={this.state.gmaps.lat}
                lng={this.state.gmaps.lng}
                zoom={12}
                loadingMessage={"Mohon Tunggu"}
                params={{v: '3.exp', key: 'AIzaSyCuGd21zRRGSUYW2rvD-u14zMQXpyYG-OQ'}}
                onMapCreated={this.onMapCreated.bind(this)}
                onClick={()=>{console.log("update");this.props.history.replace(window.location.search)}}>
                {reportmark}
                {start}
                {end}
            </Gmaps>
        )
    }
    else{
        return(
              <Gmaps
                width={'100%'}
                height={'700px'}
                lat={this.state.gmaps.lat}
                lng={this.state.gmaps.lng}
                zoom={12}
                loadingMessage={"Mohon Tunggu"}
                params={{v: '3.exp', key: 'AIzaSyCuGd21zRRGSUYW2rvD-u14zMQXpyYG-OQ'}}
                onMapCreated={this.onMapCreated.bind(this)}
                onClick={()=>{console.log("update");this.props.history.replace(window.location.search)}}
               >
                {reportmark}

            </Gmaps>
        )
    }
        
    }

}