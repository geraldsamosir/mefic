import React ,{Component} from 'react'

import {Gmaps, Marker, InfoWindow, Circle} from 'react-gmaps';

import {browserHistory} from 'react-router';


export default class cctv extends  Component{
     constructor(props){
        super(props)
        this.state ={
            info :{
                 name : "Medan",
                 url : "",
                 lat : 3.589129,
                 long :98.641836
            },
            gmaps :{
                    lat : localStorage.lat - 0.004522000,
                    lng  :localStorage.lng - 0.085220000
                },
            zoom :13,
            Marker : []
        }
    }

    componentDidMount() {
        this.getcctvlist()
    }

     getcctvlist(){
        fetch('/cctv',{
            method : 'GET'
        })
        .then(response => response.json())
        .then((response)=>{
            if(response.code ==200){
                this.setState({
                    Marker : response.result
                })
                localStorage.setItem("client_list_cctv",JSON.stringify(response.result))

            }
        })
    }

    onMapCreated(map) {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition((position) => {      
                localStorage.setItem("lat",position.coords.latitude)
                localStorage.setItem("lng",position.coords.longitude)
            });
        }
    
    }
     onClick(obj) {
   
        this.setState({
            info :{
                 name : "CCTV ASOKA GAGAK HITAM 1",
                 url :"/apps?action=cctv&url=test",
                 lat : 3.562668,
                 long : 98.62617
            },
            gmaps :{
                     lat : 3.567680999999999,
                    lng  :98.64743
                }
        })
    }
    windowclick(id){
        //console.log("ok")
        this.props.history.push('/apps?action=cctv&url='+id);

    }

    render(){
        const cctvmark  = this.state.Marker.map((data)=>{
                     
                        return (<Marker
                            key={data.cctv_id}                   
                            lat={data.cctv_lat}
                            lng={data.cctv_long} 
                            icon ="/images/cctv.png"
                            onClick={this.windowclick.bind(this,data.cctv_url_cctv)}
                            data-toggle="tooltip"
                            title={data.cctv_name_cctv}/>  )

                    })
        const mylocation  = ( 
             <InfoWindow
              //key={1} 
              lat={localStorage.lat}
              lng={localStorage.lng}
              content={"Lokasi anda"}
              onCloseClick={this.onCloseClick}
              
              />  
        )

        return(
              <Gmaps
                width={'100%'}
                height={'700px'}
                lat={this.state.gmaps.lat}
                lng={this.state.gmaps.lng}
                zoom={this.state.zoom}
                loadingMessage={"Mohon Tunggu"}
                params={{v: '3.exp', key: 'AIzaSyCuGd21zRRGSUYW2rvD-u14zMQXpyYG-OQ'}}
                onMapCreated={this.onMapCreated.bind(this)}>
                {mylocation}
                {cctvmark}
            </Gmaps>
        )
    }

}