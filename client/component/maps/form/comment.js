import React ,{Component} from "react" 
import {Link} from 'react-router-dom'

export default class formcomment extends Component {
    constructor(props){
        super(props)
        this.state = {
            form : {
                body: "",
                report_id :"",
                comment_users_id :"",
                conten_creator_email:"",
                creator_users_id :""
            }
        }
    }

    messagehange(e){
        // console.log("change")
        // console.log(localStorage.conten_creator_id)
        let urlParams = new URLSearchParams(window.location.search);
        let reportid = urlParams.get("reportid")
         e.preventDefault();
         this.setState({
             form:{
                 users_name : localStorage.username,
                 users_urlphoto: localStorage.urlphoto,
                 body : e.target.value,
                 report_id  :reportid,
                 comment_users_id : JSON.parse(localStorage.id),
                 conten_creator_email :localStorage.conten_creator_email,
                 creator_users_id : localStorage.conten_creator_id

             }
         })

    }

    sendcomment(e){
        e.preventDefault()
        console.log(this.state.form.creator_users_id)
        fetch("/reports/comment",{
            method:"POST",
            headers: {
                "Content-Type" :"application/json",
                token : localStorage.token,
                email : localStorage.email
            },
            body : JSON.stringify(this.state.form)
        })
        .then((response)=>response.json())
        .then((response)=>{
           // console.log(response.code)
        })
    }

    closecomponent(){
        this.props.history.goBack()
        this.props.history.goBack()
    }   
    render(){
        return(
        <div className="Asap">
         <a onClick={this.closecomponent.bind(this)} className="fr" href="#"><i className="ion-close-round" style={{fontSize:"20px"}}></i></a>
         <form onSubmit={this.sendcomment.bind(this)}>
             {
                (localStorage.token != undefined)?
                <div className="">
                    <textarea onChange={this.messagehange.bind(this)} placeholder="Tulis komentarmu disini" className="form-control" rows="5"></textarea>
                    <button onClick={this.sendcomment.bind(this)} className="fr btn btn-primary">Post komentarmu</button>
                </div>
                :
                <Link to="/auth?action=login" className="f5 no-underline black  bg-animate hover-bg-black hover-white inline-flex items-center pa3 ba border-box mr4" style={{textDecoration:"none"}}>
                    <h5>Login untuk mengomentari</h5>
                </Link>
             }
             <br/>
         </form>
        </div>
        )
    }
}