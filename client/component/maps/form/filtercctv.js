import React ,{Component} from 'react'
import Autocomplete from 'react-google-autocomplete';



export default class  Filtercctv extends Component {
    componentDidMount() {
        this.getcctvlist()
    }

    getcctvlist(){
        fetch('/cctv',{
            method : 'GET'
        })
        .then(response => response.json())
        .then((response)=>{
            if(response.code ==200){
               // console.log(response.result)
                this.setState({
                    cctv :  response.result
                })
                
                localStorage.setItem("client_list_cctv",JSON.stringify(response.result))

            }
        })
    }

    searchcctv(e){
        this.props.history.push("/apps?action=cctv&search="+e.target.value)
    }
    render(){

        return(
           <div>
               <form >
                   <div className="form-group">
                       <div className="Artifika">
                          <input onChange={this.searchcctv.bind(this)} type="text" className="form-control" placeholder="Cari nama CCTV"/>
                        </div>
                    </div>
               </form>
            </div>
        )
    }
}


