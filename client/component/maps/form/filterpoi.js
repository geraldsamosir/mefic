import React ,{Component} from 'react'

export default class  Filtertop extends Component {
    constructor(props){
        super(props)
        this.state  = {
            category :[]
        }
    }

    componentWillMount() {
        fetch('/poi/category/all',{method:'GET'})
         .then(response => response.json())
         .then((response)=>{
             if(response.code ==200){
                this.setState({
                    category :  this.state.category.concat(response.result)
                })
               
             }
             
         })
    }

    _onselectchange(e){
        this.props.history.push("/apps?action=pointfinder&listpoi="+e.target.value)
    }
    render(){
        return(
           <div>
               <form >
                   <div className="form-group">
                       <div className="Artifika">
                            <select className="form-control" onChange={this._onselectchange.bind(this)}>
                                <option value="" selected disabled>Pilih Kategori</option>
                               {
                                this.state.category.map((data)=>{
                                    return(
                                        <option value={data.params} >{data.name}</option>
                                    )
                                })
                               }
                            </select>
                        </div>
                    </div>
               </form>
            </div>
        )
    }
}


