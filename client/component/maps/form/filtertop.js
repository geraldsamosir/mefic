import React ,{Component} from 'react'
import Autocomplete from 'react-google-autocomplete';

import linkState from 'react-link-state';


export default class  Filtertop extends Component {

    constructor(props){
        super(props)
        this.state = {
            form : {
                start : "",
                end  :""
            }

        }
    }

    getlocation(e){
        e.preventDefault();
        if(localStorage.lat != undefined &&  localStorage.lng != undefined){
            fetch('https://maps.googleapis.com/maps/api/geocode/json?latlng='+
                    localStorage.lat+","+localStorage.lng
                    +'&sensor=true',{method:"GEt"})
            .then(response => response.json())
            .then((response)=>{
                this.setState({
                    form:{
                        start : response.results[0].formatted_address

                    }
                })

               localStorage.setItem("start_lat", response.results[0].geometry.location.lat)
                localStorage.setItem("start_lng",response.results[0].geometry.location.lng)
            })

            
        }
    }
    getendname(e){
        this.setState({
            form :{
                start : this.state.form.start,
                end: document.getElementById("end").value
            }
        })
    }

    getstartlatlng(e){
        console.log("change")
        this.setState({
            form :{
                start : document.getElementById("start").value,
                end: this.state.form.end
            }
        })

        let  geocoder = new google.maps.Geocoder();
        geocoder.geocode( { 'address': this.state.form.start}, function(results, status) {

        if (status == google.maps.GeocoderStatus.OK) {
                localStorage.setItem("start_lat", results[0].geometry.location.lat())
                localStorage.setItem("start_lng",results[0].geometry.location.lng())
                
            } 
        })
    }
    adddestination(e) {
        e.preventDefault()
        if(this.state.form.end == "" || this.state.form.end  == undefined){
             swal("Error !","Tidak dapat menambahkan tujuan  kosong", "error")
        }
        else if(this.state.form.start == "" || this.state.form.start == undefined){
                swal("Error !","Titik mulai tidak boleh kosong", "error")
        }
        else{
            
            let _data = localStorage.unorder_destination
            let data  =  (_data!= undefined)?JSON.parse(_data): [];
            data =  data.concat({destination:this.state.form.end , islock:false})
            localStorage.setItem("unorder_destination",JSON.stringify(data) )

            //before this check if start and end not empty
            let urlParams = new URLSearchParams(window.location.search);
            let destination = urlParams.get("destination");
            this.props.history.push(window.location.search+'&destination=true')
        }
    
    }

    getsavedestination(){
        if(this.state.form.start == ""){
             swal("Error !","Titik mulai tidak boleh kosong", "error")
        }
        else{
            this.props.history.push(window.location.search+"&saveroute=true")
        }
        
    }

    submit(e){
        e.preventDefault()
    }

    render(){

        return(
           <div>
               <form onSubmit={this.submit.bind(this)}>
                   <div className="form-group">
                       <div className="">
                           <div className="fl w-80">
                          <Autocomplete
                                id="start"
                                className="form-control"
                                placeholder ="Titik Mulai"
                                types="address"
                                componentRestrictions={{country: "id"}}
                                valueLink={linkState(this,'form.start')}
                                onPlaceSelected={this.getstartlatlng.bind(this)}
                                
                            />
                            </div>
                            <div className="fl w-20">
                            <button onClick={this.getlocation.bind(this)} className="f6 fr no-underline black  bg-animate hover-bg-black hover-white inline-flex items-center pa3 ba border-box mr4" tooltip="Lokasi sekarang">
                                <i className="ion-android-radio-button-on" style={{fontSize:"15px"}}></i>
                            </button>
                            </div>
                        </div>
                    </div>
                    <br/><br/><br/>
                    <div className="form-group">
                        <div className="">

                          <Autocomplete
                                id="end"
                                className="form-control"
                                placeholder ="Titik Tujuan"
                                types="address"
                                componentRestrictions={{country: "id"}}
                                onPlaceSelected={this.getendname.bind(this)} 
                                
                            />
                        </div>
                    </div>
                    <div className="form-group">
                    {
                    (localStorage.token != undefined)?
                    <button    onClick={this.getsavedestination.bind(this)} className="f6 fl w-30 no-underline black  bg-animate hover-bg-black hover-white inline-flex items-center pa3 ba border-box " >
                        <h5 className="pl1 Asap">Rute Tersimpan</h5> 
                    </button>
                    :""
                    }
                    <button onClick={this.adddestination.bind(this)} className="f6 w-50 fr no-underline black  bg-animate hover-bg-black hover-white inline-flex items-center pa3 ba border-box ">
                        <h5 className="pl1 Asap">Tambahkan ke Antrian</h5> 
                    </button>
                    </div>
               </form>
            </div>
        )
    }
}


