import React ,{Component} from "react" 
import {Link} from 'react-router-dom'


export default class formallroute extends Component {

    constructor(props){
        super(props)
        this.state =  {
            item : [],
            form :{
                id_route_group: ""
            }
        }

    }

    componentDidMount() {
        this.getgrouproute()
    }

    componentWillReceiveProps(){
        this.getgrouproute()
    }
    closecomponent(e){
         e.preventDefault();
        this.props.history.goBack()
    }
    
    caculate(e){

        //send to api berfore
         e.preventDefault();
          this.props.history.goBack()
    }

    handlerselectchage(e){
       this.setState({
           form :{
               id_route_group : e.target.value
           }
       })

    }

    deletegrouproute(e){
        e.preventDefault()
        console.log("delete")
        console.log(this.state.form.id_route_group)
        fetch("/routes/"+this.state.form.id_route_group,{
            method :"delete",
            headers :{
               "Content-Type" :"application/json",
                token : localStorage.token,
                email : localStorage.email
            }}
        )
        .then((response)=>response.json())
        .then((response)=>{
            if(response.code ==200){
                localStorage.removeItem("routes_group_id")
                this.props.history.push("/apps?action=beranda")
            }
        })
    }
    getalldestination(e){
        e.preventDefault()
        fetch("/routes/destination/"+this.state.form.id_route_group,{
            method :"GET",
            headers :{
               "Content-Type" :"application/json",
                token : localStorage.token,
                email : localStorage.email
            }
        })
        .then((response)=>response.json())
        .then((response)=>{
            if(response.code == 200){
               
                let _data =  response.result.map((__data)=>{

                    return  ({
                        destination:__data.detail_routes_name_routes,
                        islock : __data.detail_routes_is_lock
                    })
                })
                localStorage.setItem("routes_group_id",JSON.stringify(response.result[0].detail_routes_routes_id))
                localStorage.setItem("unorder_destination",JSON.stringify(_data))
                this.props.history.push('/apps?action=beranda&destination=true')
            }
        })
    }
    getgrouproute(){
          fetch("/routes/"+localStorage.id+"/edit",{
           method:"GET",
           headers :{
               "Content-Type" :"application/json",
                token : localStorage.token,
                email : localStorage.email
            }
        })
        .then((response)=>response.json())
        .then((response)=>{
            console.log(response)
            if(response.code == 200){
                this.setState({
                    item:  response.result
                })
            }
        })

    }
    render(){
        return(
        <div className >
            <h5>Rute Tersimpan</h5> 
            <div className="" style={{padding:"2%"}}>
                <form action="" className="">
                     <div className="">
                         <div className="fl w-40 Artifika" >
                             Pilih rute Tersimpan
                          </div> 
                         <div  className="fl w-60 Artifika" >
                            <select  className="form-control" onChange={this.handlerselectchage.bind(this)}>
                                <option value="" selected disabled>Pilih rute tersimpan</option>
                                {

                                    this.state.item.map((data)=>{
                                        return (
                                            <option value={data.routes_id}>{data.routes_name}</option>    
                                        )

                                    })
                                    
                                }
                                
                                
                            </select>
                        </div>
                    </div>
                    <br/><br/><br/>
                    <div>
                        <button onClick={this.getalldestination.bind(this)}  className="f5 no-underline black bg-animate hover-bg-black hover-white inline-flex items-center pa3 ba border-box mr4 fr">
                            <h5>Pilih</h5> 
                        </button>
                        <button  onClick={this.deletegrouproute.bind(this)} className="f5 no-underline black bg-animate hover-bg-black hover-white inline-flex items-center pa3 ba border-box mr4 fr">
                            <h5>Hapus</h5>
                        </button>
                        <button  onClick={this.closecomponent.bind(this)} className="f5 no-underline black bg-animate hover-bg-black hover-white inline-flex items-center pa3 ba border-box mr4 fr">
                            <h5>Batal</h5>
                        </button>
                    </div>
                </form>
            </div>
        </div>
        )
    }
}