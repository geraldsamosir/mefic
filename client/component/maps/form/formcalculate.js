import React ,{Component} from "react" 
import {Link} from 'react-router-dom'


export default class userprofil extends Component {

    closecomponent(e){
         e.preventDefault();
        this.props.history.goBack()
    }

  caculate(e){

        //do request to api before
         e.preventDefault();
         this.getorderdestination()
    }

    getorderdestination(){
        let  _data  =  JSON.parse(localStorage.unorder_destination || "") 

        _data  =  _data.filter((data)=>{
            data.destination =  data.destination.replace(/,/g,'')
            
            return data.islock == false
        })
        

        //reset localstorage if data false object = undefined
        let __data =  JSON.parse(localStorage.unorder_destination || "")
        
        __data =  __data.map((data)=>{
             return  (data.islock == false)?undefined:data
        })

        localStorage.setItem("ordered_destination",JSON.stringify(__data))

        //start and end destination to sorted by api
        let start = localStorage.start_lat +","+localStorage.start_lng
        let end  =  _data.map((data)=>{
              return data.destination
        })

        fetch("/routes/destination/filter/"+localStorage.typeroute+"?start="+start+"&end="+end.toString(),{
            method:"GET",
        })
        .then(response => response.json())
        .then((response)=>{
            if(response.code ==200){
                let _result =  response.result;
                let _final  = []
                let _ordered = []
                _result =  _result.map((data)=>{
                    return {
                        destination:  data.result[0].legs[0].end_address ,
                        islock :false ,
                        distance : data.result[0].legs[0].distance.text,
                        time :  data.result[0].legs[0].duration.text
                        
                    }
                })

                _ordered =  JSON.parse(localStorage.ordered_destination|| "")
                for(let i=0;i<_ordered.length;i++){
                    if(_ordered[i] != null){
                        _final =  _final.concat(_ordered[i])
                    }
                    else{
                        _final =_final.concat(_result[0])
                        delete _result [0]
                        _result  = _result.filter((data)=>{ return  data != undefined})
                    }
                }

                _final  = _final.map((data)=>{
                      data.isvisited =  false;
                      return data
                      
                })
            
                localStorage.setItem("ordered_destination",JSON.stringify(_final))
                let url  = window.location.search.split("?")[0]+"?action=beranda"
                this.props.history.push(url+"&destinationorder=true")
                
            }
            
        })
       
    }

    handleradiochange(e){
        localStorage.setItem("typeroute",e.target.value)
    }

    render(){
        return(
            <div className="">
        <div >
                <center>
                <h5 className="Bowlby">Pilih Prioritas</h5>
                </center>
                <br/>
                <form action="" className="Asap" style={{paddingLeft:"8%"}}>
                     <div className="">
                        <input id="kemacetan" type="radio" value="bytraffic" name="prioritas" onChange={this.handleradiochange.bind(this)} /> Kemacetan
                        <br/>
                        <input id="jarak" type="radio" value="bydistance"  name="prioritas" onChange={this.handleradiochange.bind(this)}/> Jarak
                         <br/>
                        <input id="Waktu" type="radio" value="bytime"  name="prioritas" onChange={this.handleradiochange.bind(this)}/> Waktu
                    </div>
                    <br/>
                    <div>
                        <button  onClick={this.closecomponent.bind(this)} className="f5 no-underline black bg-animate hover-bg-black hover-white inline-flex items-center pa3 ba border-box mr4 fl">
                            <h5>Batal</h5>
                        </button>
                        <button onClick={this.caculate.bind(this)}  className="f5 no-underline black bg-animate hover-bg-black hover-white inline-flex items-center pa3 ba border-box mr4 fr">
                            <h5>Pilih</h5> 
                        </button>
                    </div>
                </form>
            </div>
        </div>
        )
    }
}