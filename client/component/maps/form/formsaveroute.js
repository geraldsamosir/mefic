import React ,{Component} from "react" 
import {Link} from 'react-router-dom'
import linkState from 'react-link-state';

export default class formsaveroute extends Component {

    constructor(props){
        super(props)
        this.state = {
            form :{
                name :"",
            }
        }
    }

    closecomponent(e){
         e.preventDefault();
        this.props.history.goBack()
    }

    // caculate(e){
    //      e.preventDefault();
    //       this.props.history.goBack()
    // }

   createdestinationgroup(e){
      e.preventDefault();
       fetch("/routes",{
           method:"POST",
           headers :{
               "Content-Type" :"application/json",
                token : localStorage.token,
                email : localStorage.email
            },
            body : JSON.stringify({
                name  : this.state.form.name,
                created_users_id :localStorage.id
            })
       })
       .then((response)=>response.json())
       .then((response)=>{
           if(response.code ==201){
               localStorage.setItem("routes_group_id",JSON.stringify(response.result.id))
               this.saveallsedtinatmyion(e)
           }
       })

   }
   
   saveallsedtinatmyion(e){
       e.preventDefault()
        let data = JSON.parse(localStorage.ordered_destination)
        let final =  data.map((_data,index)=>{
            return {
                name_routes : _data.destination,
                index : index,
                is_lock :_data.islock,
                routes_id : localStorage.routes_group_id
            }
        })
        fetch("/routes/destination",{
           method:"POST",
           headers :{
               "Content-Type" :"application/json",
                token : localStorage.token,
                email : localStorage.email
            },
            body : JSON.stringify(final)
       })
       .then((response)=>response.json())
            .then((response)=>{
                if(response.code ==201){
                    this.props.history.goBack()
                }
            })

   }


    handleradiochange(e){
    }

    render(){
        return(
        <div className>
            <h5 className="Bowlby">Simpan rute</h5> 
            <div className="">
               
                <br/>
                <form action="" className="">
                     <div className="">
                        <input valueLink={linkState(this,'form.name')} type="" className="form-control" placeholder={"Masukan nama kelompok rute"}/>
                    </div>
                    <br/>
                    <div>
                        <button  onClick={this.closecomponent.bind(this)} className="f5 no-underline black bg-animate hover-bg-black hover-white inline-flex items-center pa3 ba border-box mr4 fl">
                            <h5 className="Asap">Batal</h5>
                        </button>
                        <button onClick={this.createdestinationgroup.bind(this)}  className="f5 no-underline black bg-animate hover-bg-black hover-white inline-flex items-center pa3 ba border-box mr4 fr">
                            <h5 className="Asap">Simpan</h5> 
                        </button>
                    </div>
                </form>
            </div>
        </div>
        )
    }
}