import React ,{Component} from "react" 
import {Link} from 'react-router-dom'

const moment = require('moment')

export default class report extends Component {
    
    constructor(props){
        super(props)
        this.state = {
            content : {},
            isnear :false,
            havelikeordislike : false,
        }
    }

    componentWillReceiveProps(){
        let urlParams = new URLSearchParams(window.location.search);
        let id = urlParams.get("reportid")
        this.getcontent(id)
    }

    componentDidMount() {
        let urlParams = new URLSearchParams(window.location.search);
        let id = urlParams.get("reportid")
        this.getcontent(id)
        socket.on("reportupdate",()=>{
            console.log("update like or dislike")
            setTimeout(()=>{
                 this.getcontent(id)
            },2000)
           
        })

        
    }

    

    getcontent(id){
        let _data =  JSON.parse(localStorage.client_report || "");
        _data  = _data.filter((data)=>{
           return  data.reports_id == id
        })
        this.setState({
            content : _data[0]
        })
        console.log(_data)
        this.checkisnearme(_data[0])
        this.checkhavelikeordislike(_data[0])
        localStorage.setItem("conten_creator_email",_data[0].users_email)
        localStorage.setItem("conten_creator_id",_data[0].users_id)

        
    }

    dolikedislike(data , action){
        
        let _data = {
            id :data.reports_id,
            like :data.reports_like,
            dislike :data.reports_dislike
        }

        if(action =="like"){
            _data.like = _data.like + 1
        }
        else{
            _data.dislike = _data.dislike +1
        }
        fetch("/reports/"+_data.id,{
             method :"PUT",
             headers :{
                "Content-Type" :"application/json",
                token : localStorage.token,
                email : localStorage.email
             },
             body : JSON.stringify(_data)
         })
         .then((response)=>response.json())
         .then((response)=>{
             console.log(response)
             if(response.code == 200){
                let  _likeordislike =  (localStorage.listlikedislike !=undefined)?JSON.parse(localStorage.listlikedislike ):[]
                 if(localStorage.listlikedislike == undefined){
                     console.log([_data])
                     localStorage.setItem("listlikedislike",JSON.stringify([_data]))
                 }
                 else{
                     _likeordislike = _likeordislike.concat(_data)
                     localStorage.setItem("listlikedislike",JSON.stringify(_likeordislike))
                 }
                 let __client_report = JSON.parse(localStorage.client_report)
                  __client_report =   __client_report.map((data)=>{
                       if(data.reports_id  == _data.id){
                            data.reports_like =  _data.like
                            data.reports_dislike = _data.dislike
                       }
                       return data
                 })
                 localStorage.setItem("client_report",JSON.stringify(__client_report))
                 this.props.history.push(window.location.search)
             }
         })

    }

    checkhavelikeordislike(_data){
       let  data = (localStorage.listlikedislike != undefined)?JSON.parse(localStorage.listlikedislike):[]
     
        data  = data.filter((data)=>{
           
           return data.id == _data.reports_id
        })
        console.log(data)
        
        this.setState({
            havelikeordislike: ( data != "")?true:false
        })
    }

    checkisnearme(data){

       let reportCord =  new google.maps.LatLng(data.reports_lat,data.reports_lng)
       let mylocationCord =   new google.maps.LatLng(localStorage.lat,localStorage.lng)
       let distance = google.maps.geometry.spherical.computeDistanceBetween(mylocationCord,reportCord)  
       
       this.setState({
         isnear : (distance < 500)
       })
    }

    closecomponent(){
        this.props.history.goBack()
    }
    render(){
        
        let urlParams = window.location.search
        
        return(
        <div className="Asap">
        {
        (this.props.commentstatus == undefined)?
        <a onClick={this.closecomponent.bind(this)} className="fr" href="#"><i className="ion-close-round" style={{fontSize:"20px"}}></i></a>
        :""
        }
            <center>
                <img src={this.state.content.reports_photo_url} alt=""
                 style={{width:350,height:300}}/>
                 <br/>
                 <Link to={urlParams+"&usersid="+this.state.content.users_email}>
                 <img src={this.state.content.users_urlphoto || "/images/user-default.png"} alt=""
                 style={{width:100,height:100,position:"absolute",top:0,left:0,borderRadius:"50%",border:"3px solid white"}}/>
                 </Link>
                 <p className="Bowlby">{this.state.content.reports_name_report}</p>
                <p className="Artifika"><b>Dilaporkan oleh <Link to={urlParams+"&usersid="+this.state.content.users_email}>{this.state.content.users_username}</Link> </b></p>
                
                <p>{ moment(this.state.content.reports_created_at).format(' DD-MM-YYYY HH:mm:ss')}</p>
                 <p className="Bowlby">Tipe  : {this.state.content.category_reports_name_category_report}</p>
                 <br/>
                 <hr style={{color:"black"}}/>
                 
                 <div className="col-md-12">
                     {(this.state.isnear == true 
                     && localStorage.token!= undefined 
                     && this.state.content.users_id != localStorage.id
                     && this.state.havelikeordislike == false )?
                     <div className="col-md-6">
                         <button className="btn btn-success" onClick={this.dolikedislike.bind(this,this.state.content,"like")}>
                         <span className="ion-thumbsup" style={{fontSize:20}}></span> &nbsp;{this.state.content.reports_like}
                         </button>
                         &nbsp;
                         <button className="btn btn-danger" onClick={this.dolikedislike.bind(this,this.state.content,"dislike")}>
                          <span className="ion-thumbsdown" style={{fontSize:20,marginTop:"1%"}}></span>&nbsp;{this.state.content.reports_dislike}
                          </button>
                          
                     </div>
                     :<div className="col-md-6">
                         <span className="ion-thumbsup" style={{fontSize:30}}></span> &nbsp;{this.state.content.reports_like}
                         &nbsp;
                          <span className="ion-thumbsdown" style={{fontSize:30,marginTop:"1%"}}></span>&nbsp;{this.state.content.reports_dislike}
                          
                     </div>
                     }
                     <div className="col-md-6">
                         <Link to={window.location.search+"&comment=true"}>
                         <p  style={{fontSize:20,marginTop:"1%"}} >
                         Komentar 
                         </p>
                         </Link>
                     </div>
                 </div>
                 
            </center>
        </div>
        )
    }
}