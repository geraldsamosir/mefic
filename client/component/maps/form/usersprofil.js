import React ,{Component} from "react" 
import {Link} from 'react-router-dom'


export default class userprofil extends Component {

    constructor(props){
        super(props)
        this.state = {
            content : {}
        }

    }

    componentDidMount() {
        this.getprofilbyemail()
    }

    getprofilbyemail(){
        let urlParams = new URLSearchParams(window.location.search);
        let email  = urlParams.get("usersid")
         fetch("/users/"+email,{
            method : "get"
        })
        .then((response)=>response.json())
        .then((response)=>{
            if(response.code ==200){
                this.setState({
                    content :  response.result       
                })
                console.log(response.result)
            }
        })
    }

    closecomponent(){
        this.props.history.goBack()
    }
    render(){
        return(
        <div className="Asap">
        <a href="#" onClick={this.closecomponent.bind(this)} className="fr" to="#"><i className="ion-close-round" style={{fontSize:"20px"}}></i></a>
         
         <div style={{height:"500px",display:"block",overflowY:"auto"}}>
         <center >
             <img 
                src={this.state.content.urlphoto || "/images/user-default.png"}
                style={{width:"400px",height:"350px"}}
                alt=""/>
                <br/>
                <h3><b className="Artifika">{this.state.content.username}</b></h3>
                <br/>
                <p>
                    Reputasi
                     &nbsp; &nbsp;
                    <span className="ion-thumbsup" style={{fontSize:30}}></span> &nbsp;{this.state.content.like}
                     &nbsp;
                    <span className="ion-thumbsdown" style={{fontSize:30,marginTop:"1%"}}></span>&nbsp;{this.state.content.dislike}

                </p>
                <p>Jenis Kelamin &nbsp; {(this.state.content.gender == 1) ?"Laki-Laki" :"Perempuan"}</p>
                <p>Umur &nbsp; {this.state.content.age}</p>
                {
                    (this.state.content.privacy == 0)?
                    
                    <div>
                        <p>Alamat &nbsp; {this.state.content.address}</p>
                        <p>Telepon &nbsp; {this.state.content.phone}</p>
                        <p>Email &nbsp; {this.state.content.email}</p>
                    </div>
                    :""
                }
         </center>
         </div>
        </div>
        )
    }
}