import React ,{Component} from "react" 
import {Link} from 'react-router-dom'

const moment = require('moment')


export default class commentlist extends Component {

    constructor(props){
        super(props)
        this.state = {
            comment : []
        }
    }


    componentWillReceiveProps() {
      this.getcommentbyreportid()
    }

    componentDidMount() {
        this.getcommentbyreportid()
         socket.on("comment",(data)=>{
            console.log(data)
            let urlParams = new URLSearchParams(window.location.search);
            let reportid = urlParams.get("reportid")
            //if(data.)
            if(reportid == data.report_id){
                console.log("sini")
                this.getcommentbyreportid()
                 console.log(data)
            }
           
        })
    }

    getcommentbyreportid(){
       
        let urlParams = new URLSearchParams(window.location.search);
        let reportid = urlParams.get("reportid")

        fetch("/reports/comment/"+reportid,{
            method : "get"
        })
        .then((response)=>response.json())
        .then((response)=>{
            if(response.code ==200){
                this.setState({
                    comment :  response.result
                })
            }
        })

    }

    deletecomment(id){
        fetch("/reports/comment/"+id,{
           method :'delete',
           headers :{
                "Content-Type" :"application/json",
                token : localStorage.token,
                email : localStorage.email
             }

        })
        .then((response)=>response.json())
        .then((response)=>{
            if(response.code == 200){
                this.getcommentbyreportid()
            }
        })
    }

    render(){
         let urlParams = window.location.search
        return(
            <div className="Asap" style={{height:"380px",display:"block",overflowY:"scroll"}}>
                <div style={{maxheight:"500px"}}>
                    {
                    this.state.comment.map((data)=>{
                        return (
                            <div className="col-md-12" style={{marginBottom:"4%"}} >
                                <div className="col-md-2" style={{display:"inline"}}>
                                <Link to={urlParams+"&usersid="+data.users_email}>
                               
                                <img 
                                    style={{width:50,height:50,borderRadius:"50%"}}
                                    src={data.users_urlphoto || "/images/user-default.png"}
                                    alt=""/>   
                                </Link>
                                </div>
                                <div className="col-md-10" style={{display:"inline",paddingLeft:"1%"}}>
                                     {
                                    (data.users_id == localStorage.id)?
                                    <a onClick={this.deletecomment.bind(this,data.comment_reports_id)} href="#" className="fr"><i className="ion-close-round" style={{fontSize:"15px"}}></i></a>:""
                                    }
                                    <Link className="Artifika" to={urlParams+"&usersid="+data.users_email}>{data.users_username}</Link><br/>
                                    {data.comment_reports_body}
                                </div>
                                 
                                <p className="fr">{ moment(data.comment_reports_created_at).format('DD-MM-YYYY HH:mm:ss')}</p>
                            </div>
                        )  
                    })
                    }
                </div>
            </div>
        )
    }
}