import React ,{Component} from  'react'
import Stars from 'react-stars'


export default class detailpoi extends Component{

    constructor(props){
        super(props)
        this.state = {
            detail : {}
        }
    }
    
    componentDidMount() {
        this.getdetail()
    }

    componentWillReceiveProps() {
        this.getdetail()
    }

    getdetail(){
       let urlParams = new URLSearchParams(window.location.search);
       let id =  urlParams.get('tracking')
        let data  =  JSON.parse(localStorage.listpoi || "")
        let final  =  data.filter((data)=>{
            return data.id == id
        })
        this.setState({
            detail : final[0] ||{}
        })
    }
    

    closecomponent(){
        this.props.history.goBack()
    }

    

    render(){
        return (
            <article className="Asap">
                  <a href="#" onClick={this.closecomponent.bind(this)} className="fr" to="#"><i className="ion-close-round" style={{fontSize:"20px"}}></i></a>
                  {
                    (this.state.detail != {})?
                    <div>
                        <h3 className="Artifika"><b>{this.state.detail.name}</b></h3>
                        <p className="Artifika">{this.state.detail.vicinity}</p>
                        <p>{this.state.detail.distance} Km</p>
                        <p>Rating :{this.state.detail.rating || 0}</p>
                          <Stars 
                            size ={30}
                            value={this.state.detail.rating ||0}
                            edit={false} 
                           />
                    </div>
                    
                    :""
                  }
            </article>
        )
    }
}