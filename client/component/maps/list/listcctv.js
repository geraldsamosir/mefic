import React ,{Component} from  'react'
import {Link} from 'react-router-dom'

export default class Listcctv extends Component{

    constructor(props){
        super(props)
        this.state = {
            cctv :[]
        }
    }

    componentDidMount() {
        this.getcctvlist()
        socket.on("cctvbroken",()=>{
            console.log("in")
            this.getcctvlistapi()
        })
    }
    componentWillReceiveProps() {
        this.getcctvlist()
    }


    getcctvlist(){
         let urlParams = new URLSearchParams(window.location.search);
        let name =  urlParams.get('search') || "";
        if(localStorage.client_list_cctv !=undefined){
            let _data =  JSON.parse(localStorage.client_list_cctv).filter((data)=>{
                return  data.cctv_name_cctv.match(name.toUpperCase())
            })

            _data  =  _data.filter((data)=>{
                return data.cctv_cctv_condition != 2
            })

            this.setState({
                cctv : _data 
            })
        }
        else{
           this.getcctvlistapi()
        }
    }

    getcctvlistapi(){
        fetch('/cctv',{
            method : 'GET'
        })
        .then(response => response.json())
        .then((response)=>{
            if(response.code ==200){
                response.result = response.result.filter((data)=>{
                    return data.cctv_cctv_condition != 2
                })
                
                this.setState({
                    cctv :  response.result
                })
                
                localStorage.setItem("client_list_cctv",JSON.stringify(response.result))

            }
        })
    }

    render(){
        
        return (
            <article className="Asap">
                <div className="" style={{width:"480px",background:"white", maxHeight: "500px", display: "block", overflowY: "scroll"}}>
                    {
                        
                        this.state.cctv.map((data,index)=>{
                            return (
                                <div key={index} style={{border: "1px solid rgba(128, 128, 128, 0.32)" ,padding: "5%"}}>
                                    <h5 className="Artifika"><b>{data.cctv_name_cctv}</b></h5> 
                                    <br/>
                                    <p style={{display:"inline",width:"50%"}}>{data.cctv_address}</p>
                                    <p  style={{textAlign:"right",display:"inline",width:"50%"}}>
                                        <Link to={"/apps?action=cctv&url="+data.cctv_url_cctv}>
                                            <i className="fr ion-play" style={{fontSize:"30px"}}></i>
                                        </Link>
                                    </p>
                                </div>
                            )
                        })
                    }
                </div>
            </article>
        )
    }
}