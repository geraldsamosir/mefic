import React, { Component } from 'react';

import {SortableContainer, SortableElement, arrayMove} from 'react-sortable-hoc';



const SortableItem = SortableElement(({value}) =>
  <div style={{background:"white",listStyleType:"none"}} className="ph3 pv3 bb b--light-silver formbox">
     
     <div className="w-100" style={{display:"inline"}}>
       <center>
      {value.destination}
      </center>
      </div>
      
  </div>
);



const SortableList = SortableContainer(({items,history}) => {
  const deletedestiantion=(destination)=>{
      let _data = JSON.parse(localStorage.unorder_destination)

      if(_data.length > 1){
          _data  =   _data.filter((data)=>{
                        return data.destination != destination;
                    })
          
        localStorage.setItem("unorder_destination",JSON.stringify(_data) )
        history.push(window.location.search)
        
      }
      else if(_data.length <=1){
        localStorage.removeItem("unorder_destination")
        history.push("/apps?action=beranda")
      } 
  }

  const updatelock = (destination) =>{
   
    let _data = JSON.parse(localStorage.unorder_destination)
    console.log(destination)
    _data  = _data.map((data)=>{ 
       
      if(data.destination == destination){
          console.log(data.islock)
         if(data.islock == true) {
           
            data.islock = false
         }
         else if(data.islock == false){
           console.log("masuk")
           data.islock =  true
         }
      }
      return data

    })
    localStorage.setItem("unorder_destination",JSON.stringify(_data) )
    history.push(window.location.search)

  }

  
  return (
    <div className="list pl0 ml0 center mw6 ba b--light-silver br2" >
      {items.map((value, index) => (
        <div className="Artifika">
          {(value.islock == true)?
          <i className="fl ion-locked" onClick={()=>updatelock(value.destination)}></i>
          :
          <i className="fl ion-unlocked" onClick={()=>updatelock(value.destination)}></i>
          }
          <i className="fr ion-close-circled" onClick={()=>deletedestiantion(value.destination)}></i>  
          <SortableItem key={`item-${index}`} index={index} value={value} />
        </div>
      ))}
    </div>
  );
});

export default class Listdestination extends Component {
  
  constructor(props){
    super(props)
    this.state = {
     
       items: []
    }

  }

  componentDidMount() {
    this.getvaluefromlocalstorage()     
  }

  componentWillReceiveProps() {
     this.getvaluefromlocalstorage()     
    }
  getvaluefromlocalstorage(){
     this.setState({
       items  : JSON.parse(localStorage.unorder_destination)
     })
  }

  onSortEnd ({oldIndex, newIndex}) {
    
    this.setState({
      items: arrayMove(this.state.items, oldIndex, newIndex)
    })
     localStorage.setItem("unorder_destination",JSON.stringify(this.state.items) )
  }

  closecomponent(){
      
      //clear all before destiantion 
      localStorage.removeItem("unorder_destination")
      this.props.history.push("/apps?action=beranda")
  }

  calculate(){
    this.props.history.push(window.location.search+'&calculate=true')
  }
  render() {
    return (
      <div>
       
          <div style={{height:"250px",overflowY:"scroll"}}>
            <SortableList items={this.state.items} onSortEnd={this.onSortEnd.bind(this)} history={this.props.history} />
          </div>
         <hr/>
            <div>
              <div className="w5 fl">
                  <button onClick={this.closecomponent.bind(this)} className="btn-block f6 no-underline black  bg-animate hover-bg-black hover-white inline-flex items-center pa2 ba border-box mr4">
                      <center>
                          <h5  style={{marginLeft:"30%"}} className="pl1 Asap"> Hapus Semua</h5>
                      </center>
                  </button>
              </div>
              <div className="w5 fr">
                  <button onClick={this.calculate.bind(this)} className="btn-block f6 no-underline black  bg-animate hover-bg-black hover-white inline-flex items-center pa2 ba border-box mr4">
                    <center>
                      <h5 style={{marginLeft:"30%"}} className="pl1 center Asap"> Urutkan  Destinasi  </h5>
                    </center>
                  </button>
              </div>
          </div>
      </div>

    )
  }
}