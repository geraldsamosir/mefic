import React, { Component } from 'react';
import {Link} from 'react-router-dom'

export default class Listdestinationorders extends Component {

  constructor(props){
      super(props)
      this.state = {
            items: []
      }
  }

  componentDidMount() {
     this.getdataorder()

     socket.on("reported",()=>{
        setTimeout(()=>{ 
            let _checkisdisturb =  this.anydisturbreport(JSON.parse(localStorage.step))
            //chcek is broadcast triger your route
            if(_checkisdisturb.distrub == true){

                //set realtime location to start lat and lng
                localStorage.setItem("start_lat",localStorage.lat)
                localStorage.setItem("start_lng",localStorage.lng)
                this.getroute(JSON.parse(localStorage.last_end))
            }

        },5000)
     })

     socket.on("reportupdate",()=>{
            setTimeout(()=>{ 
            let _checkisdisturb =  this.anydisturbreport(JSON.parse(localStorage.step))
            //chcek is broadcast triger your route
            if(_checkisdisturb.distrub == true){
                 //set realtime location to start lat and lng
                localStorage.setItem("start_lat",localStorage.lat)
                localStorage.setItem("start_lng",localStorage.lng)
                this.getroute(JSON.parse(localStorage.last_end))
            }

            },1000)
        })
  }



  componentWillReceiveProps() {
    this.getdataorder()
  }
  

  getdataorder(){
      this.setState({
          items : JSON.parse(localStorage.ordered_destination)
      })
      
  }


 closecompoent(e){
     e.preventDefault();
     this.props.history.goBack()
     this.props.history.goBack()
     //this.props.history.push("/apps?action=beranda")  
  }
 
  getroute(destination){
      
      
      let start = localStorage.start_lat +","+localStorage.start_lng
      let end =  destination

      //cheked has visited

      let orderdestination  =  JSON.parse(localStorage.ordered_destination) || ""

      orderdestination  =  orderdestination.map((data)=>{
           if(data.destination.replace(/,/g,'') == destination){

               data.isvisited  = true;
           }
           return data
      })

      localStorage.setItem("ordered_destination",JSON.stringify(orderdestination))

      //get routes
        fetch("/routes/destination/filter/"+localStorage.typeroute+"?start="+start+"&end="+end.toString(),{
                method:"GET",
        })
        .then(response => response.json())
        .then((response)=>{
            if(response.code ==200){
                let all =  response.result[0].result;
                let data = response.result[0].result[0].legs[0].steps;
                
                let _alternative  = all.filter((data)=>{
                    return data != undefined
                })

                 _alternative = _alternative.map((data)=>{
                     console.log(data.legs[0].steps)
                     return data.legs[0].steps
                 })

                 
                 //check if main route disturb set value from google 10 so report point and like must  > google value
                 let mainroute =this.anydisturbreport(data)
                 console.log("here main")
                 console.log(mainroute)
                 if(mainroute.distrub ==true && (mainroute.reportpoint + mainroute.likedislike) >= localStorage.googlevalue){
                    
                    let status_alternativ =  _alternative.map((data,_index)=>{
                        let result =this.anydisturbreport(data) 
                        result  = Object.assign({index:_index },result)
                        return result
                    })

                    //check if there is not disturb
                    let findnotdsiturb = status_alternativ.filter((data)=>{
                        data.totalpoint =  data.reportpoint + data.likedislike
                        console.log(data)
                        return data.distrub ==  false
                    })
                    
                    if(findnotdsiturb != ""){
                        data  =  _alternative[findnotdsiturb[0].index]
                    }
                    else{
                        status_alternativ =  status_alternativ.sort((a,b)=>{
                           return  a.totalpoint - b.totalpoint
                        })
                    
                        data = _alternative[status_alternativ[0].index]
                       
                    }


                    
                 }
                 console.log("alt here")
                 console.log(_alternative)
                localStorage.setItem("step",JSON.stringify(data))

                localStorage.setItem("step_alternative",JSON.stringify(_alternative))

                //set last end for waiting if there is update route
                localStorage.setItem("last_end", JSON.stringify(end))

                 console.log("locastorage update")
                
                this.props.history.push(window.location.search+"&tracking=true&end="+destination)
            }
        })

    //this.props.history.push(e)  &tracking=true&end="+
  }

  anydisturbreport(track){
    let client_report =  JSON.parse(localStorage.client_report) || "";
    client_report =  client_report.filter((data)=>{
        return data.category_reports_is_disturb_route ==  1
    })
    let start = new google.maps.LatLng(track[0].end_location.lat, track[0].end_location.lng)
    let end =   new  google.maps.LatLng(track[track.length - 1].end_location.lat, track[track.length - 1].end_location.lng)
    let distance_start_end =  google.maps.geometry.spherical.computeDistanceBetween(start,end)  
    let radius  = (distance_start_end > 7000)?2000:500
    
    let isfind =  client_report.map((data)=>{
         let chekpath =   track.map((_data)=>{
             
            let reportpath =  new google.maps.LatLng(data.reports_lat,data.reports_lng)
            let  trackpath =   new google.maps.LatLng(_data.end_location.lat,_data.end_location.lng)
            let distance = google.maps.geometry.spherical.computeDistanceBetween(reportpath,trackpath)  
            return (distance <= radius)?{likedislike : data.reports_like - data.reports_dislike}:undefined;
         })

         chekpath =  chekpath.filter((data)=>{
            return data != undefined
         })
         return chekpath
    })
    
    isfind =  isfind.filter((data)=>{
        return data != ""
    })
    let  _likedislike = 0
   
    if(isfind != ""){
        isfind = isfind.map((data)=>{
            return data[0]
        })

        
        isfind.map((data)=>{
            console.log(data)
            _likedislike += data.likedislike
        })
    }
    console.log("sini")
    console.log(_likedislike)
    return (isfind.length > 0)?{distrub : true ,
                            reportpoint :isfind.length * localStorage.reportvalue,
                            likedislike :_likedislike || 0
                          } :{distrub : false}
  }

  
  
  recalculate(){    

      //reset with realtime location
      localStorage.setItem("start_lat",localStorage.lat)
      localStorage.setItem("start_lng",localStorage.lng)

    //get last order_destinatioan
    let order_dest = JSON.parse(localStorage.ordered_destination)
      
      
    let  _data  =  JSON.parse(localStorage.unorder_destination || "") 



    _data  =  _data.filter((data)=>{
            data.destination =  data.destination.replace(/,/g,'')
            return data.islock == false
    })
    //reset localstorage if data false object = undefined
    let __data =  JSON.parse(localStorage.unorder_destination || "")


    

    __data =  __data.map((data)=>{
            return  (data.islock == false)?undefined : data
    })


    localStorage.setItem("ordered_destination",JSON.stringify(__data))

      //start and end destination to sorted by api
     let start = localStorage.start_lat +","+localStorage.start_lng
     let end  =  _data.map((data)=>{
              return data.destination
    })
   
    fetch("/routes/destination/filter/"+localStorage.typeroute+"?start="+start+"&end="+end.toString(),{
            method:"GET",
        })
        .then(response => response.json())
        .then((response)=>{
           
            if(response.code ==200){
                
                let _result =  response.result;
                let _final  = []
                let _ordered = []
                _result =  _result.map((data)=>{
                    return {
                            destination:  data.result[0].legs[0].end_address ,
                            islock :false ,
                            distance:data.result[0].legs[0].distance.text,
                            time:data.result[0].legs[0].duration.text
                        }
                })
                
                _ordered =  JSON.parse(localStorage.ordered_destination|| "")
                

                for(let i=0;i<_ordered.length;i++){
                    if(_ordered[i] != null){
                        _final =  _final.concat(_ordered[i])
                    }
                    else{

                        _final =_final.concat(_result[0])
                        delete _result [0]
                        _result  = _result.filter((data)=>{ return  data != undefined})
                    }
                }


                
                order_dest  = order_dest.filter((data)=>{
                    return data.isvisited == true
                })

               _final =  _final.map((data)=>{
                    order_dest.map((_data)=>{
                       data.isvisited =  (data.destination  == _data.destination)? true :false
                   })

                   return data
               }) 

            //    console.log("filter name by true")
            //     console.log(_final)
                this.setState({
                    items : _final
                })

                localStorage.setItem("ordered_destination",JSON.stringify(_final))
               
                
            }
            
        })


  }
  deltedestination(destination){
   
    let data =  JSON.parse(localStorage.ordered_destination).filter((_data)=>{
        return _data.destination != destination
    })
    

    this.setState({
        items :  data  
    })
    localStorage.setItem("ordered_destination",JSON.stringify(data))
    localStorage.setItem("unorder_destination",JSON.stringify(data))
     //recalculate here

     this.recalculate()
  }


  deletedestinantionbygroup(){
      let  _id =  JSON.parse(localStorage.routes_group_id)
     return fetch("/routes/destination/"+_id ,{
          method :"delete",
            headers :{
               "Content-Type" :"application/json",
                token : localStorage.token,
                email : localStorage.email
            }
      })
      .then((response)=>response.json())
     
  }

  createdestination(){
     let data = JSON.parse(localStorage.ordered_destination)
        let final =  data.map((_data,index)=>{
            return {
                name_routes : _data.destination,
                index : index,
                is_lock :_data.islock,
                routes_id : localStorage.routes_group_id
            }
        })
        fetch("/routes/destination",{
            method:"POST",
            headers :{
                "Content-Type" :"application/json",
                    token : localStorage.token,
                    email : localStorage.email
                },
                body : JSON.stringify(final)
        })
        .then((response)=>response.json())
                .then((response)=>{
                    if(response.code ==201){
                    swal("updated!", "Destinasi berhasi di update!", "success")
                    }
                })
    }

  updatesaveroute(e){
      e.preventDefault()
      this.deletedestinantionbygroup()
      .then((response)=>{
          this.createdestination()
      })
      
  }

  newsavedestination(e){
      this.props.history.push(window.location.search+"&newroute=true")
  }

  render() {
    let url  = window.location.search.split("?")[0]+"?action=beranda&destinationorder=true"
    return (
      <div>
        <div className="Asap" style={{height:"400px",overflowY:"scroll"}}>
            {  
                this.state.items.map((data)=>{
                    let _style = (data.isvisited == true)?{background:"#d9d9ea"}:{}
                    
                    return(
                        <div className="list-group-item" style={_style}>
                            <div>
                            <a className="fr " to="#" onClick={this.deltedestination.bind(this,data.destination)} ><i className="ion-close-round" style={{fontSize:"20px"}}></i></a>
                            <span className="Artifika">{data.destination}</span><br/><br/>
                            {(data.distance != undefined)?
                                data.distance
                                :<div>
                                    <i className="ion-locked"></i>
                                </div>
                            } <br/>
                            {data.time || ""}
                            <br/>
                            <p style={{textAlign:"right"}}>
                                
                                <a  onClick={this.getroute.bind(this,data.destination.replace(/,/g,''))}>Lihat rute</a>
                            </p>
                        </div>
                    </div>
                    )

                 })
                
            }
          </div>
          
          **)&nbsp;Hapus rute yang berwarna biru
         <hr/>
            <div>
              <div className="fl" style={{width:"30%",padding:"1%"}}>
                  <button onClick={this.closecompoent.bind(this)} className="f6 no-underline black  bg-animate hover-bg-black hover-white inline-flex items-center pa3 ba border-box mr4 btn-block ">
                      <center>
                           <h5  style={{marginLeft:"40%"}} className="pl1">Batal</h5>
                      </center>
                         
                  </button>
              </div>
              {
             (localStorage.token != undefined )?
             <div>
                {
                    (localStorage.routes_group_id != undefined)?
              <div className="fl" style={{width:"30%",padding:"1%"}}>
                  <button  onClick={this.updatesaveroute.bind(this)}  className="f6 no-underline black  bg-animate hover-bg-black hover-white inline-flex items-center pa3 ba border-box mr4 btn-block ">
                      <center>
                          <h5 style={{marginLeft:"20%"}} className="pl1">Simpan</h5>
                      </center>
                  </button>
              </div>:""
                }
               <div className="fl" style={{width:"30%",padding:"1%"}}>
                  <button onClick={this.newsavedestination.bind(this)} className="btn-block f6 no-underline black  bg-animate hover-bg-black hover-white inline-flex items-center pa3 ba border-box mr4">
                    <center>
                      <h5 style={{marginLeft:"40%"}} className="pl1 center">Baru</h5>
                    </center>
                  </button>
              </div>
              </div>
              :""   
              }
             
          </div>
      </div>

    );
  }
}