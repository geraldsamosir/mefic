import React ,{Component} from  'react'
import {Link} from 'react-router-dom'

export default class Listnotification extends Component{

    constructor(props){
        super(props)
        this.state = {
            notif : []
        }
    }

   componentWillReceiveProps() {
       this.getnotif()
   }

    componentDidMount() {
        this.getnotif()
    }
    getnotif(){
        let id =  localStorage.id
        console.log(id)
        fetch("/reports/notfication/"+id,{
            method :"GET",
            headers :{
                token : localStorage.token,
                email : localStorage.email
            }
        })
        .then((response)=>response.json())
        .then((response)=>{
             if(response.code ==200){
                 console.log(response.result)
                 this.setState({
                     notif : response.result
                 })
             }
        })


    }

    deletenotif(id){
        console.log(id)
        fetch("/reports/notfication/"+id,{
                method :"DELETE",
                headers :{
                token : localStorage.token,
                email : localStorage.email
            }
        })
        .then((response)=>response.json())
        .then((response)=>{
            if(response.code ==200){
                this.getnotif()
            }
        })
    }

    closecomponent(){
        this.props.history.goBack()
    }


    render(){
        let _notif =  this.state.notif.filter((data)=>{
            return data.notification_reports_comment_users_id != localStorage.id
        })
        console.log("notif ")
        console.log(_notif == "")
        return (
            <article className="Asap">
             <a href="#" onClick={this.closecomponent.bind(this)} className="fr" to="#"><i className="ion-close-round" style={{fontSize:"20px"}}></i></a>
             <br/>   
            <center>
                <h5 className="Artifika">Notifikasi</h5>
            </center>
            <div className="list-group" style={{height:"350px",overflowY:"scroll"}}>
                {
                (_notif != "")?
                this.state.notif.map((data)=>{
                    return(data.notification_reports_comment_users_id != localStorage.id)?
                    
                        <div className="list-group-item">
                            <a href="#" onClick={this.deletenotif.bind(this,data.notification_reports_id)}  className="fr" to="#"><i className="ion-close-round" style={{fontSize:"20px"}}></i></a>
                            <div>
                            {data.users_username} mengomentari " {data.notification_reports_body} " di report mu yang berjudul 
                            &nbsp;<Link to={"/apps?action=beranda&comment=true&reportid="+data.reports_id}>{data.reports_name_report}</Link>
                            <br/>
                            
                            </div>
                        </div>
                        :""    
                     })
                     :<div className="list-group-item">
                         <center>
                        Anda tidak memiliki notifikasi saat ini
                        </center>
                     </div>
                }
            </div>
            </article>
        )
    }
}