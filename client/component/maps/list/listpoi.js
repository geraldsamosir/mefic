import React ,{Component} from  'react'

export default class Listpoi extends Component{
    constructor(props){
        super(props)
        this.state = {
            result  : []
        }
    }
    componentDidMount() {
           this.getdatafromapi()
    }
    componentWillReceiveProps() {
      this.getdatafromapi()
    }

    //action
    closecomponent(){
        this.props.history.goBack()
    }

     getlistpoi(id){
        
        let data  =  JSON.parse(localStorage.listpoi);
        let final =   data.filter((data)=>{
            return data.id == id
        })
       
        //return  final[0].vicinity.replace(/,/g,'')
        return  (final[0].geometry.location.lat+","+final[0].geometry.location.lng)
    }

    getroute(e){
       
         let routetype =  "~bytraffic"
         let end = this.getlistpoi(e)
        let start = localStorage.lat+","+localStorage.lng
         fetch('/routes/destination/filter/'+routetype+"?start="+start+"&end="+end,
             {method:"GET"}
         )
         .then(response => response.json())
         .then((response)=>{
             if(response.code ==200){
                localStorage.setItem("routepoi",JSON.stringify(response.result[0].result[0].legs[0].steps))
               //console.log(localStorage.routepoi)
              this.props.history.push("/apps?action=pointfinder&tracking="+e)  
                    
             }
             
         })
    }

    getdatafromapi(){
      let urlParams = new URLSearchParams(window.location.search);
      let typepoi =  urlParams.get('listpoi')
       fetch('/poi/search/'+typepoi+'?mylocation='+localStorage.lat+","+localStorage.lng,
       {method:"GET"})
       .then(response => response.json())
       .then((response)=>{
           if(response.code == 200){
              // console.log(response.result)
               this.setState({
                   result : []
               })
                this.setState({
                    result :  this.state.result.concat(response.result)
                })
                localStorage.setItem("listpoi",JSON.stringify(response.result))
           }
          
       })
    }


    render(){
      //  console.log(this.state.result)
        return (
            
            <article className="Asap" style={{height:"300px"}}>
            <div className="" style={{height: "380px", display: "block", overflowY: "scroll"}}>
                {   
                    this.state.result.map((data)=>{
                            
                        return  (
                                <div style={{border: "3px solid rgba(128, 128, 128, 0.32)" ,padding: "5%"}}>
                                <h5 className="Bowlby">{data.name}</h5><br/>
                                <p className="Artifika">{data.vicinity}</p>
                                Status: Buka<br/>
                                <p  style={{textAlign:"left",width:"50%",display:"inline"}}>Jarak : {data.distance } Km</p>
                                <a  onClick={this.getroute.bind(this,data.id)} className="fr" style={{textAlign:"right",width:"50%",display:"inline"}}>Pilih</a>
                                </div>
                        )
                    })
                }
            </div>
            </article>
        )
    }
}