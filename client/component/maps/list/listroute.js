import React ,{Component} from  'react'
import renderHTML from 'react-render-html';


export default class Listroute extends Component{

    constructor(props){
        super(props)
        this.state = {
            routes :[]
        }
    }

    componentDidMount() {
        this.getroute()  
        socket.on("reportupdate",()=>{
            setTimeout(() =>{
                console.log("get text routes")
                this.getroute()  
            }, 5000);
        })
    }

    componentWillReceiveProps() {
      
        this.getroute()
    }

    //action

    getlistpoi(){
        let urlParams = new URLSearchParams(window.location.search);
        let id =  urlParams.get('tracking')
        let data  =  JSON.parse(localStorage.listpoi);
        let final =   data.filter((data)=>{
            return data.id == id
        })
       return  (final[0].geometry.location.lat+","+final[0].geometry.location.lng)
    }

    getdestination(){
         let urlParams = new URLSearchParams(window.location.search);
         let end =  urlParams.get('end')
         return end
    }


    
    getroute(){
         let urlParams = new URLSearchParams(window.location.search);
         let poi =  urlParams.get('tracking')  || undefined
         let page = urlParams.get('action') || undefined
         let routetype =  (page =='beranda')?localStorage.typeroute :"~bytraffic"
         let end = (page == "beranda")? this.getdestination() :this.getlistpoi()
        let start = localStorage.lat+","+localStorage.lng
        if(page == "pointfinder"){
         fetch('/routes/destination/filter/'+routetype+"?start="+start+"&end="+end,
             {method:"GET"}
         )
         .then(response => response.json())
         .then((response)=>{
             if(response.code ==200){
                if(page == "pointfinder"){
                   
                    this.setState({
                        routes : response.result[0].result[0].legs[0].steps
                    })
                    localStorage.setItem("routepoi",JSON.stringify(response.result[0].result[0].legs[0].steps))
                }
             }
             
         })
        }
        else{
            // console.log("step")
            //  console.log(JSON.parse(localStorage.step))
                this.setState({
                         routes :JSON.parse(localStorage.step)
             })
            //   socket.on("reportupdate",()=>{
            //     setTimeout(()=>{
            //         if(page == "beranda"){
            //             this.setState({
            //                 routes :JSON.parse(localStorage.step)
            //             })
            //         }
            //     },2000)
            //   })

        }
    }

    closecomponent(){
         this.props.history.goBack()
    }

    render(){
        
        let rute = 
         rute  = this.state.routes.map((data)=>{
                return(
                     <div key={data.html_instructions} style={{border: "3px solid rgba(128, 128, 128, 0.32)" ,padding: "5%"}}>
                          {renderHTML(data.html_instructions)}
                     </div>
                )
         })
        return (
            <article className="Asap" style={{height:"300px"}}>
             <a className="fr" href="#" onClick={this.closecomponent.bind(this)}><i className="ion-close-round" style={{fontSize:"20px"}}></i></a>
              <center>
                    <h5 className=""><b>Rute</b></h5>
            </center>
            <div className="" style={{height: "380px", display: "block", overflowY: "scroll"}}>
                {rute}
            </div>
            </article>
        )
    }
}