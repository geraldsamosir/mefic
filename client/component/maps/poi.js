import React ,{Component} from 'react'

import {Gmaps, Marker, InfoWindow, Circle} from 'react-gmaps';

export default class beranda extends  Component{

constructor(props){
    super(props)
    this.state ={
          gmaps :{
                    lat : 3.567680999999999,
                    lng  :98.64743
                },
            zoom :12
    }

}
 componentDidMount() {
    let urlParams = new URLSearchParams(window.location.search);
    let poi =  urlParams.get('tracking')  || undefined
    if(poi != undefined){
        this.setState({
            gmaps :{
                        lat : localStorage.lat,
                        lng  :localStorage.lng
                    },
                zoom :17
        });
   }
     
 }
  onMapCreated(map) {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition((position) => {
                localStorage.setItem("lat",position.coords.latitude)
                localStorage.setItem("lng",position.coords.longitude)
            });
        }
  }
    
  
    render(){
      let urlParams = new URLSearchParams(window.location.search);
      let poi =  urlParams.get('tracking')  || undefined
      
        if(poi != undefined){
             let _end  = JSON.parse(localStorage.routepoi) ||""
            _end =  _end[_end.length -1].end_location  ||""
            
                const start  =(<Marker                   
                            lat={localStorage.lat}
                            lng={localStorage.lng} 
                            icon ="/images/car.png"
                        data-toggle="tooltip"
                            title={"Mulai"}/>  )
                const end = (<Marker                   
                            lat={_end.lat}
                            lng={_end.lng} 
                            icon ="/images/finish.png"
                        data-toggle="tooltip"
                            title={"Akir"}/>  )
            return(
                <Gmaps
                    width={'100%'}
                    height={'700px'}
                    lat={this.state.gmaps.lat}
                    lng={this.state.gmaps.lng}
                    zoom={this.state.zoom}
                    loadingMessage={"Mohon Tunggu"}
                    params={{v: '3.exp', key: 'AIzaSyCuGd21zRRGSUYW2rvD-u14zMQXpyYG-OQ'}}
                    onMapCreated={this.onMapCreated}>
                    {start}
                    {end}
                </Gmaps>
            )
        }
        else{
            return(
                <Gmaps
                    width={'100%'}
                    height={'700px'}
                    lat={this.state.gmaps.lat}
                    lng={this.state.gmaps.lng}
                    zoom={this.state.zoom}
                    loadingMessage={"Mohon Tunggu"}
                    params={{v: '3.exp', key: 'AIzaSyCuGd21zRRGSUYW2rvD-u14zMQXpyYG-OQ'}}
                    onMapCreated={this.onMapCreated}>
             
                </Gmaps>
            )
        }
    }

}