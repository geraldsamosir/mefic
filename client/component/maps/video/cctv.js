import React ,{Component} from "react" 
import ReactSWF from 'react-swf'
import {Link} from 'react-router-dom'

export default class Cctv extends Component {
    constructor(props){
        super(props)
        this.state = {
            cctv : {}
        }
    }
    componentDidMount() {
        this.getcctv()
        socket.on("cctvbroken",()=>{
            setTimeout(()=>{ 
                 this.getcctv()
            },500)
        })
    }

    reportcctv(){
        console.log(this.state.cctv.cctv_cctv_condition)
        fetch("/cctv/broken/"+this.state.cctv.cctv_id,{
            method : "PUT",
            headers : {
                "Content-Type" :"application/json"
            },
            body : JSON.stringify({
               cctv_status: this.state.cctv.cctv_cctv_status + 1,
               cctv_condition :(this.state.cctv.cctv_cctv_condition == 0)?1:this.state.cctv.cctv_cctv_condition
            })
        })
        .then(response => response.json())
        .then((response)=>{
            console.log(this.state.cctv)
            if(response.code == 200){

               swal("Terimakasih", "Terimakasih Laporan anda akan segera kami proses , ", "success")
            }
        })  

    }

    getcctv(){

       let urlParams = new URLSearchParams(window.location.search);
       let url =  urlParams.get('url')

       let _cctv = JSON.parse(localStorage.client_list_cctv).filter((data)=>{
           return data.cctv_url_cctv == url
       })
       this.setState({
           cctv : _cctv[0]
       })
    }
    render(){
       let urlParams = new URLSearchParams(window.location.search);
        let url =  urlParams.get('url')
        return(
        <div className="Asap">
        <Link className="fr" to="/apps?action=cctv"><i className="ion-close-round" style={{fontSize:"20px"}}></i></Link>
        <ReactSWF
        src = "./flash/GrindPlayer.swf"
        width = "650"
        height = "300"
        wmode = "transparent"
        flashVars = {
            {
                autoPlay: true,
               // src:   "http://117.102.112.123:1935/live/nakula.stream/playlist.m3u8",//"http://110.232.82.176:80/live/" + this.state.cctv + ".m3u8", //"http://110.232.82.176:80/live/" + this.state.cctv + ".m3u8",
              // src :"http://medan.marktel.co/live/SC_GATSU-MAJESTIC.m3u8",
               src : url,
                scaleMode: 'letterbox',
                plugin_hls: './flash/flashlsOSMF.swf',
                hls_debug: false,
                hls_debug2: false,
                hls_minbufferlength: -1,
                hls_lowbufferlength: 2,
                hls_maxbufferlength: 60,
                hls_startfromlowestlevel: false,
                hls_seekfromlowestlevel: false,
                hls_live_flushurlcache: false,
                hls_seekmode: 'ACCURATE',
                hls_capleveltostage: false,
                hls_maxlevelcappingmode: 'downscale',

            }
        }
        allowFullScreen = 'true'
        allowFullScreenInteractive = 'true'
        bgcolor = '#000' / >
            <center>
                <h5 className="Artifika"><b>{this.state.cctv.cctv_name_cctv}</b></h5>
                <p><b>{this.state.cctv.cctv_address}</b></p>
                <br/>
                <button onClick={this.reportcctv.bind(this)} className="btn btn-danger">Report CCTV Rusak</button>
            </center>
        </div>
        )
    }
}