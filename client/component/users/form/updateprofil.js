import React ,{Component} from 'react'
import {Link} from "react-router-dom"
import linkState from 'react-link-state';


export default class Updateprofil extends Component{
    constructor(props){
        super(props)
        this.state = {
            imagePreviewUrl: '',
            form : {
            file: '',
                gender : '',
                age : '',
                phone :'',
                email : '',
                address :'',
                privacy :'',
                urlphoto : '',
                like : '',
                dislike :''
            }
        };
    }

    componentDidMount() {
        let email  = localStorage.email
        if(localStorage.token == undefined){
             this.props.history.push('/auth?action=login');
        }
        else{
            fetch('/users/'+email,{
                method : "GET",
                headers : {
                    email  :localStorage.email,
                    token :  localStorage.token
                }

            })
            .then(response => response.json())
            .then((response)=>{
                if(response.code == 200){
                    this.setState({
                        form:{
                            gender : response.result.gender,
                            age : response.result.age,
                            phone : response.result.phone,
                            email : response.result.email,
                            address :  response.result.address,
                            urlphoto : response.result.urlphoto,
                            privacy  : (response.result.privacy == 0)?true :false,
                            like  : response.result.like,
                            dislike : response.result.dislike
                                                
                        }
                    })

                    //check for gender
                    if(response.result.gender == 1) {
                        let  radiobtn = document.getElementById("lk");
                        radiobtn.checked  = true;
                    }
                    else if(response.result.gender == 2) {
                        let  radiobtn = document.getElementById("pr");
                        radiobtn.checked  = true;
                    }

                    if(response.result.privacy == 0){
                    //check for privacy
                        let  radiobtn = document.getElementById("privasi");
                        radiobtn.checked  = true;
                    }

                    
                }
                else if(response.code == 404){
                    console.log(response.message)
                }
            })
        }

    }

     imageclick(){
         document.querySelector("input[type='file']").click();
     }
    
     _handleImageChange(e) {
        e.preventDefault();

        let reader = new FileReader();
        let file = e.target.files[0];

        reader.onloadend = () => {
        this.setState({
            file: file,
            imagePreviewUrl: reader.result
        });
        }

        reader.readAsDataURL(file)
    }

    handleradiochange(e){
       
        this.setState({
            file: this.state.file,
            imagePreviewUrl: this.state.imagePreviewUrl,
            form : {
                gender : e,
                age :  this.state.form.age,
                phone : this.state.form.phone,
                email :  this.state.form.email,
                address : this.state.form.address,
                privacy : this.state.form.privacy,
                urlphoto :  this.state.form.urlphoto,
            }
        })

        
       
    }

    doupdateprofil(e){
        e.preventDefault();
        let formdata  = new FormData()
        //do check form 
        if(this.state.form.gender =='' || 
            this.state.form.email =='' ||
            this.state.form.phone == ''
           ){
                 swal("Error !","Selain alamat dan umur  tidak boleh kosong!", "error")
        }
        else{
            let data = {};
            let headers_content = {
                token : localStorage.token,
                email : localStorage.email
            };
            let url   ="";

            if(document.getElementById('profilimage').files[0] !=  undefined){
                formdata.append("gender" , this.state.form.gender)
                formdata.append("urlphoto",document.getElementById('profilimage').files[0] )
                formdata.append("age" , this.state.form.age)
                formdata.append("phone" , this.state.form.phone)
                formdata.append("email" , this.state.form.email)
                formdata.append("address" , this.state.form.address)
                formdata.append("privacy" , (!this.state.form.privacy == true)?1:0)
                data =  formdata
                url = "/users/withimage"
            }
            else{
                Object.assign(headers_content,{"Content-Type" :"application/json"})
                data = JSON.stringify({
                    gender : this.state.form.gender,
                    urlphoto :this.state.form.urlphoto,
                    age : this.state.form.age,
                    phone : this.state.form.phone,
                    email : this.state.form.email,
                    address : this.state.form.address,
                    privacy : (!this.state.form.privacy == true)?1:0
                })
                url = "/users"
            }
            


            fetch(url+"/"+localStorage.id,{
               method: 'PUT',
               headers :headers_content,
                body :   data
            })
            .then(response => response.json())
            .then((response)=>{
                console.log(response);
            })
        }
        
    }

    render(){
        let {imagePreviewUrl} = this.state;
        let $imagePreview = null;

        if (imagePreviewUrl) {
            $imagePreview = (<img className="img-responsive" 
                                src={imagePreviewUrl} style={{height:"350px"}} 
                                onClick={this.imageclick.bind(this)}
                                />);
        }
        else if(this.state.form.urlphoto !="" && this.state.form.urlphoto != null){

            $imagePreview = (<img className="img-responsive" 
                                src={this.state.form.urlphoto} style={{height:"350px"}} 
                                onClick={this.imageclick.bind(this)}
                                />);
        }
        else{
            $imagePreview = (<img className="img-responsive" 
                            src={"/images/user-default.png"} 
                            style={{height:"350px"}} 
                            onClick={this.imageclick.bind(this)}
                            />);
        }

        return(
            <div className="container Asap">
                <br/><br/><br/>   
                <form   onSubmit={this.doupdateprofil.bind(this)}>
                <div className="col-md-6 animated fadeInDown" style={{borderRight:"1px solid rgba(128, 128, 128, 0.29)"}}>

                    <div className="">
                    {$imagePreview}
                    Klik Gambar untuk mengganti photo profil anda
                    <br/>
                    <input  id="profilimage"  type="file" onChange={this._handleImageChange.bind(this)} style={{display:"none"}}/>      
                     <br/>
                     <input id="privasi" checkedLink={linkState(this,'form.privacy')} type="checkbox" /> Tampilkan informasi pribadi?
                    </div>
                </div>    
                <div className="col-md-6 animated fadeInUp">
                        <div className="form-group">
                            <div className="col-md-4">
                                Jenis Kelamin 
                            </div>
                            <div className="col-md-8">
                                <input id="lk" type="radio"  name="gender" onChange={this.handleradiochange.bind(this,1)} /> Laki - Laki 
                                &nbsp;&nbsp;&nbsp;
                                <input id="pr" type="radio"  name="gender" onChange={this.handleradiochange.bind(this,2)}/> Perempuan
                            </div>
                        </div>
                        <br/><br/>
                        <div className="form-group">
                            <div className="col-md-4">
                                Umur
                            </div>
                            <div className="col-md-8">
                                <input valueLink={linkState(this,'form.age')} type="number" className="form-control" />
                            </div>
                        </div>
                        <br/><br/>
                        <div className="form-group">
                            <div className="col-md-4">
                                Telepon
                            </div>
                            <div className="col-md-8">
                                <input valueLink={linkState(this,'form.phone')} type="text" className="form-control" />
                            </div>
                        </div>
                        <br/><br/>
                        <div className="form-group">
                            <div className="col-md-4">
                                Email
                            </div>
                            <div className="col-md-8">
                                <input valueLink={linkState(this,'form.email')} type="email" className="form-control" />
                            </div>
                        </div>
                        <br/><br/>
                        <div className="form-group">
                            <div className="col-md-4">
                                Alamat
                            </div>
                            <div className="col-md-8">
                                <input valueLink={linkState(this,'form.address')} type="text" className="form-control" />
                            </div>
                        </div>
                        <br/><br/>
                        <div className="form-group">
                            <div className="col-md-4">
                                Reputasi
                            </div>
                            <div className="col-md-8">
                                <span className="ion-thumbsup" style={{fontSize:30}}></span> &nbsp;{this.state.form.like}
                                &nbsp;&nbsp;&nbsp;
                                <span className="ion-thumbsdown" style={{fontSize:30,marginTop:"1%"}}></span>&nbsp;{this.state.form.dislike}
                            </div>
                        </div>
                            
                     <br/><br/><br/>
                        <button onClick={this.doupdateprofil.bind(this)} className="f5 no-underline black bg-animate hover-bg-black hover-white inline-flex items-center pa3 ba border-box mr4 fr">
                            <h5>Simpan perubahan</h5>
                        </button>
                        <button  onClick={()=> this.props.history.goBack()} className="f5 no-underline black bg-animate hover-bg-black hover-white inline-flex items-center pa3 ba border-box mr4 fr">
                            <h5>Cancel</h5>
                        </button>
                    </div>
                 </form>
            </div>
        )
    }
}