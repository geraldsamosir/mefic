import React ,{Component} from 'react'
import {Link} from "react-router-dom"

//component
import Header  from  '../component/home/header'
import Slider  from  '../component/home/slider'



export default  class Home extends Component{
    
 
    render () {
        return (
            <div>
                <Header />
                <br/><br/><br/><br/>
                <article className="fl w-50 pa10">
                <header className="fl w-100 w-50-l pa3-m pa4-l ">
                    <div className="lh-title f3 b mt0">
                        <center>
                            <h1 className="Pacifico">Selamat Datang</h1>
                        </center>
                        <hr/>
                         <b className="Bowlby">Haloo...</b><br/><br/>
                         <p className="Asap">
                         Selamat datang di Meffic adalah aplikasi perencanaan dan navigasi rute Medan.
                         Mari bergabung bersama pengguna Meffic untuk membantu mengurangi kemacetan kota Medan.
                         Rencana perjalanan ? Meffic aja 
                         </p>

                        <br/><br/>
                        <center>
                            <a target="blank" href="https://drive.google.com/file/d/0B8zE0aI1m6LgZ3J1X19QaXdpcmM/view?usp=sharing" className="f5 no-underline black bg-gray bg-animate hover-bg-black hover-white inline-flex items-center pa3 ba border-box mr4" style={{textDecoration:"none"}}>
                                <h5 className="pl1 Bowlby">Download untuk <br/><i className="ion-social-android" style={{fontSize:30}}></i></h5>
                            </a>
                        </center>
                    </div>
                </header>
                <Slider/>
                </article>
                <article className="fl w-50 ">
                    <br/><br/>
                    <center>
                        <iframe width="500" height="315" 
                                src="https://www.youtube.com/embed/Pkr1EZo5zWM?rel=0&amp;controls=0&amp;showinfo=0">
                        </iframe>
                    </center>
                    <br/> 

                  
                    <div className="fl w-100">
                        <center>
                        <Link to="/apps?action=beranda" className="f6 no-underline black bg-gray bg-animate hover-bg-black hover-white inline-flex items-center pa3 ba border-box mr4" style={{padding:"15%",textDecoration:"none",width:"250px",height:"50px"}}>
                                <h1 style={{marginLeft:"-10%"}}className="pl1 Bowlby"> Maps <i className="ion-map" style={{fontSize:30}}></i></h1>
                        </Link>
                        </center>
                    </div>
                </article>
            </div>
        )
    }
}

