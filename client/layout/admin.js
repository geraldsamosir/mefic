import React ,{Component} from 'react'
import {Link} from "react-router-dom"

//component
import Header  from  '../component/admin/header'
import  Tableusers from '../component/admin/table/users'
import  Tablereports from '../component/admin/table/reports'
import  Tablecctv from '../component/admin/table/cctv'

import Formcctv from '../component/admin/form/cctv'
import FormConfif from  '../component/admin/form/configroute'

export default  class Admin extends Component{

    componentDidMount() {
       if(localStorage.roles != 2){
           this.props.history.push('/')
       }
    }

    render () {
        let urlParams = new URLSearchParams(window.location.search);
        let page =  urlParams.get('action')||undefined
        switch (page) {
            case "users":
                return(
                     <div>
                         
                        <Header history={this.props.history} />
                        <Tableusers/>
                    </div>
                    )
            case "config":
              return(
                     <div>
                        <Header history={this.props.history} />
                         
                         <FormConfif />
                    </div>
                    )

            case "post":
                return(
                        <div>
                            <Header history={this.props.history}/>
                            <Tablereports/>
                        </div>
                    )
            case "cctv":
             let _formcctv  =  urlParams.get('form')||undefined
            return(
               
                     <div>
                         <Header history={this.props.history}/>
                         <Tablecctv />
                         {(_formcctv !=undefined)?
                             <div  style={{ width:"100%",height:"650px",position:"absolute",top:100,zIndex:11,background:"rgba(128, 128, 128, 0.2)"}}>
                                 <div className="animated rubberBand center bg-white formboxreport" style={{width:"300px",height:"450px", position:"absolute",top:10,padding:"1%",left:"40%",zIndex:"11"}} >       
                                    <Formcctv  history={this.props.history}/>
                                </div>
                            </div>
                            :""

                         }
                    </div>
                    )
            
            default:
                break;
        }
       
       
    }
}

