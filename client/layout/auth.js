import React ,{Component} from 'react'

//component
import Header  from  '../component/home/header'
import Authform  from  '../component/auth/loginregister'
import Forgotform  from  '../component/auth/form/forgotpassword'
import FormUpdatepassword from '../component/auth/form/updatepassword'

import Formupdateprofil from  '../component/users/form/updateprofil'

import Listnotification  from  '../component/maps/list/listnotification'


//notfication
import Notif from  '../action/notif'

export default class Auth extends Component{ 
 componentDidMount() {
       let urlParams = new URLSearchParams(window.location.search);
       let page =  urlParams.get('action')
       if(page != "updateprofil"  ){
                if(localStorage.token != undefined){
                    this.props.history.goBack()
                }
        }
         Notif.comment(this.props.history)

    }

render(){

    let urlParams = new URLSearchParams(window.location.search);
    let page =  urlParams.get('action')
     let notif = urlParams.get('notif');
    switch (page) {
        case "login":               
            return(
                <div>
                    <Header />
                    <br/><br/><br/><br/><br/><br/>
                    <Authform history={this.props.history} />
                </div>
            )          
            //break;

        case "forgotpassword":               
            return(
                <div>
                    <Header />
                    <br/><br/><br/><br/><br/><br/>
                    <Forgotform/>
                </div>
            )          
            //break;
        case "updateprofil":
            return(
                <div>
                    <Header />
                    <br/><br/><br/><br/><br/><br/>
                    <Formupdateprofil history={this.props.history}/>
                     {
                        (notif != undefined)?
                       <div className="animated fadeIn center bg-white formbox" style={{width:"355", position:"absolute",top:100,padding:"1%",right:"0%",height:"500px",zIndex:"12"}} >
                            <Listnotification history={this.props.history}/>
                        </div>
                        :""
                        }
                </div>
            )      

        case "updatepassword":  
        let token =  urlParams.get('token')  || '' ;
        if(token != ''){
            return(
                <div>
                    <Header />
                    <br/><br/><br/><br/><br/><br/>
                    <FormUpdatepassword history={this.props.history}/>
                </div>
            )         
        }
        else{
            return(
                <div>
                    <Header />
                    <br/><br/><br/><br/><br/><br/>
                    <div className="container">
                        <center>
                            <br/><br/><br/>
                            <h3> <b> Token tidak valid </b> </h3>
                        </center>
                    </div>
                </div>
            )      
        } 
            //break;
    
        default:
        
            return(
                <div>
                    <Header />
                </div>
            )          
            break;
    }

    }
}
 
//export default Auth

