import React ,{Component} from 'react'

//component
import Header  from  '../component/home/header'
import Authform  from  '../component/auth/loginregister'
import FiltertopForm from  '../component/maps/form/filtertop'
import FilterpoiForm from  '../component/maps/form/filterpoi'
import Forgotform  from  '../component/auth/form/forgotpassword'
import Filtercctvform  from  '../component/maps/form/filtercctv'
import  Formreport from   '../component/maps/form/report'
import  Formcoment from   '../component/maps/form/comment'
import  Formuserprofil from   '../component/maps/form/usersprofil'
import  Forcalculate from   '../component/maps/form/formcalculate'
import  Formsaveroute from   '../component/maps/form/formsaveroute'
import  Formallroute from   '../component/maps/form/formallroute'


import Berandamaps from  '../component/maps/beranda'
import Poimaps  from '../component/maps/poi'
import Cctvimaps  from '../component/maps/cctv'

import Listdestination  from  '../component/maps/list/listdestiantion'
import Listdestinationorder  from  '../component/maps/list/listdestinationorder'
import Listroute  from  '../component/maps/list/listroute'
import Listnotification  from  '../component/maps/list/listnotification'
import Listcctv  from  '../component/maps/list/listcctv'
import Listcoment from  '../component/maps/list/comment'
import Listpoi from '../component/maps/list/listpoi'
import Detailpoi from  '../component/maps/list/detailpoi'

import VideoCctv from   '../component/maps/video/cctv'

//notifikasi
import Notif from  '../action/notif'

// const Maps  = ({ match }) => { 



export default class  Maps extends Component{

    componentDidMount() {
        Notif.comment(this.props.history)
    }
 
    render(){
    let urlParams = new URLSearchParams(window.location.search);
    let page =  urlParams.get('action')
    let notif = urlParams.get('notif');
    switch (page) {
        case "beranda":    

            //get beranda report
            let reportid =  urlParams.get('reportid') || undefined
            let commentstatus =  urlParams.get('comment') || undefined
            let userid  = urlParams.get('usersid')  || undefined
            let caluculate  = urlParams.get('calculate')  || undefined
            let destiantion  = urlParams.get('destination')  || undefined
            let destiantionorder  = urlParams.get('destinationorder')  || undefined
            let tracking =  urlParams.get('tracking') || undefined
            let newroute  = urlParams.get('newroute') || undefined
            let saveroute  = urlParams.get("saveroute") ||  undefined
            return(
                <div>
                    <Header />
                    <div>
                    {
                        (tracking == undefined)?
                        <Berandamaps history={this.props.history}/>
                        :""
                    }
                    
                    <div className="animated fadeInLeft center bg-white formbox" style={{position:"absolute",top:100,padding:"1%",width:"350px"}} >
                        {/*{{filterin top}}*/}
                    <FiltertopForm history={this.props.history}/>
                    </div>
                        {
                            //if route not null  do show  list dynamic desination 
                            (destiantion != undefined)?
                                /*list dynamic for indexing destination*/
                                <div className="animated fadeInLeft center bg-white formbox" style={{width:"355", position:"absolute",top:330,padding:"1%",height:"400px"}} >
                                    <div className="">
                                    <Listdestination  history={this.props.history}/>
                                    </div>
                                    
                                </div>
                            :
                            ""
                        }

                        {
                            //ordered destination
                            (destiantionorder != undefined)?
                                /*list dynamic for indexing destination*/
                                <div className="animated fadeInLeft center bg-white formbox" style={{width:"355", position:"absolute",top:100,padding:"1%",height:"550px"}} >
                                    <div className="">
                                    <Listdestinationorder  history={this.props.history}/>
                                    </div>
                                    
                                </div>
                            :
                            ""
                        }
                        {
                            //if tracking not null  do show  list dynamic desination 
                            (tracking !=undefined)?
                            <div>
                            <Berandamaps history={this.props.history}/>
                            
                            {/*list show tracking to route*/}
                            <div className="animated fadeInRight center bg-white formbox" style={{width:"355", position:"absolute",top:100,padding:"1%",right:"0%",height:"500px"}} >
                                <Listroute history={this.props.history}/>
                            </div>
                            </div>
                            :
                            ""
                        }
                        {
                            //if tracking not null  do show  list dynamic desination 
                            (reportid != undefined)?
                                /*list show tracking to route*/
                            <div  style={{ width:"100%",height:"650px",position:"absolute",top:80,zIndex:10,background:"rgba(128, 128, 128, 0.2)"}}>
                            <div className="animated zoomIn center bg-white formboxreport" style={{maxWidth:"950px",height:"580px", top:10, position:"absolute",padding:"1%",left:"15%",}} > 
                                    <Formreport commentstatus={commentstatus} history={this.props.history}/>
                                </div>
                            </div>
                            :
                            ""
                        }
                        {
                            (commentstatus != undefined)?
                            <div className="animated fadeIn center bg-white formboxreport" style={{width:"550px",height:"580px", position:"absolute",top:90,padding:"1%",left:"43%",zIndex:11}} > 
                                    <Formcoment history={this.props.history}/>
                                     <br/>
                                    <Listcoment />
                                </div>
                            :
                            ""
                        }
                        {
                            (userid!=undefined)?
                            <div  style={{ width:"100%",height:"650px",position:"absolute",top:80,zIndex:12,background:"rgba(128, 128, 128, 0.2)"}}>
                            <div className="animated fadeIn center bg-white formboxreport" style={{width:"550px",height:"580px", position:"absolute",top:10,padding:"1%",left:"30%",zIndex:"12"}} > 
                                <Formuserprofil history={this.props.history}/>
                            </div>
                            </div>
                            :
                            ""
                        }
                        {
                        (notif != undefined)?
                       <div className="animated fadeIn center bg-white formbox" style={{width:"355", position:"absolute",top:100,padding:"1%",right:"0%",height:"500px",zIndex:"12"}} >
                            <Listnotification history={this.props.history} />
                        </div>
                        :""
                        }
                        {
                            (caluculate !=undefined)?
                            <div  style={{ width:"100%",height:"650px",position:"absolute",top:80,zIndex:11,background:"rgba(128, 128, 128, 0.2)"}}>
                                 <div className="animated fadeInDown  center bg-white formboxreport" style={{width:"300px",height:"250px", position:"absolute",top:10,padding:"1%",left:"40%",zIndex:"11"}} >       
                                    <Forcalculate history={this.props.history}/>
                                </div>
                            </div>
                            :
                            ""
                        }
                        {
                            (newroute != undefined)?
                                <div  style={{ width:"100%",height:"650px",position:"absolute",top:80,zIndex:11,background:"rgba(128, 128, 128, 0.2)"}}>
                                 <div className="animated fadeInDown center bg-white formboxreport" style={{width:"380px",height:"200px", position:"absolute",top:10,padding:"1%",left:"35%",zIndex:"11"}} >       
                                    <Formsaveroute history={this.props.history}/>
                                </div>
                            </div>
                            :""
                        }
                        {
                            (saveroute != undefined)?
                             <div  style={{ width:"100%",height:"650px",position:"absolute",top:80,zIndex:11,background:"rgba(128, 128, 128, 0.2)"}}>
                                 <div className="animated fadeInDown center bg-white formboxreport" style={{width:"500px",height:"250px", position:"absolute",top:10,padding:"1%",left:"30%",zIndex:"11"}} >       
                                    <Formallroute history={this.props.history}/>
                                </div>
                            </div>
                            :""
                        }
                    </div>
                </div>
            )
            // break;
            case "pointfinder":    
                let  listpoi =  urlParams.get('listpoi') || undefined
                let trackingpoi = urlParams.get('tracking') || undefined
                return(
                    <div>
                        <Header />
                        <div className="">
                        {
                        (trackingpoi == undefined)?
                            <Poimaps />
                            :""
                        }
                        <div className="animated fadeInLeft center bg-white formbox" style={{width:"350px", position:"absolute",top:100,padding:"1%"}} >
                            <FilterpoiForm  history={this.props.history}/>
                        </div>
                         {
                        (notif != undefined)?
                       <div className="animated fadeIn center bg-white formbox" style={{width:"355", position:"absolute",top:100,right:"0%",height:"500px",zIndex:"12"}} >
                            <Listnotification   history={this.props.history}/>
                        </div>
                        :""
                        }
                        {
                          (listpoi !=undefined)?
                                  <div className="animated fadeIn center bg-white formbox" style={{width:"355", position:"absolute",top:200,padding:"1%",height:"400px"}} >
                                    <Listpoi  history={this.props.history}/>
                                </div>
                          :""
                        }
                        {
                         (trackingpoi != undefined)?
                             <div>
                                <Poimaps />
                                <div className="animated fadeInLeft center bg-white formbox" style={{width:"355", position:"absolute",top:100,padding:"1%",height:"300px"}} >
                                    <Detailpoi  history={this.props.history}/>
                                </div>
                                <div className="animated fadeInRight center bg-white formbox" style={{width:"355", position:"absolute",top:100,padding:"1%",right:"0%",height:"500px"}} >
                                    <Listroute history={this.props.history}/>
                                </div>
                            </div>
                         :""
                        }

                        </div>
                    </div>
                )
                // break;
        
            case "cctv":
                let cctvurl =  urlParams.get('url') || undefined
                return(
                    <div>
                        <Header />
                        <div className="">
                            <Cctvimaps  history={this.props.history} />           
                        </div>
                        <div className="animated fadeInUp center bg-white formbox" style={{width:"500px", position:"absolute",top:100,padding:"1%",background:"white"}} >
                            <Filtercctvform  history={this.props.history}/>
                        </div>
                        <div className="animated fadeInUp center " style={{width:"500px", position:"absolute",top:180, background:"white"}} >
                            <Listcctv history={this.props.history}/>
                        </div>
                        {
                            //if tracking not null  do show  list dynamic desination 
                            (cctvurl != undefined)?
                                /*list show tracking to route*/
                            <div  style={{ width:"100%",height:"650px",position:"absolute",top:80,zIndex:10,background:"rgba(128, 128, 128, 0.2)"}}>
                            <div className="animated fadeInDown center bg-white formbox" style={{width:"700px",height:"450px", position:"absolute",top:10,padding:"1%",left:"25%"}} >
                                        <br/>
                                    <VideoCctv />
                                </div>
                            </div>
                            :
                            ""
                            
                        }
                         {
                        (notif != undefined)?
                       <div className="animated fadeIn center bg-white formbox" style={{width:"355", position:"absolute",top:100,padding:"1%",right:"0%",height:"500px",zIndex:"12"}} >
                            <Listnotification history={this.props.history}/>
                        </div>
                        :""
                        }
                    </div>
                )
                // break;
    
        default:
        
            return(
                <div>
                    <Header />
                </div>
            )          
        //  break;
    }
 }
}
 

