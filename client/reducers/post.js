import { SET_ARTICLE, SET_ARTICLE_BY_ID } from '../action/articleAction';

let INITIAL_STATE = {
    article: {},
    articles: []
}

export default function article(state = INITIAL_STATE, action = {}) {
    switch (action.type) {
        case SET_ARTICLE:
            state.article = {}
            return action.articles;
            break;

        case SET_ARTICLE_BY_ID:
            state.article = action.article

            return state
            break;
        default:
            return state.articles;
            break;
    }
}