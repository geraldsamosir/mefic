import React ,{Component} from 'react'
import {BrowserRouter ,Route ,RouteWithLayout } from 'react-router-dom'
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { Provider } from 'react-redux';
import rootReducer from  './reducers/index'


//pages
import Home from  './layout/Home'
import Auth from './layout/auth'
import Apps from  './layout/maps'
import Admin from './layout/admin'

const store = createStore(
  rootReducer,
  composeWithDevTools(
    applyMiddleware(thunk)
  )
);



export default  () => (
  <BrowserRouter>
    <Provider store={store}>
    <div>
        <Route exact path="/" component={Home}/>
         <Route exact path="/auth" component={Auth}/>
         <Route exact path="/apps" component={Apps} />
         <Route exact path="/management" component={Admin}/>
    </div>
    </Provider>
  </BrowserRouter>
  
)
