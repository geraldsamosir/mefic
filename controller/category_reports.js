const Controller = require('../crud/controller')
const pathmodel = '../model/Category_reports'
const model = require(pathmodel)

module.exports = new(
    class category_reports extends Controller {
        constructor() {
            super();
        }

        modelpath(req, res, next) {
            req.headers.pathmodel = pathmodel;
            req.headers.itemname  = 'Kategori laporan'
            next()
        }

    }
)