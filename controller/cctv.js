const Controller = require('../crud/controller')
const pathmodel = '../model/Cctv'
const model = require(pathmodel)

module.exports = new(
    class cctv extends Controller {
        constructor() {
            super();
        }

        modelpath(req, res, next) {
            req.headers.pathmodel = pathmodel;
            req.headers.itemname  = 'CCTV'
            next()
        }

        addreportcctvstatus(req,res,next){
            if(req.body.cctv_status == -1){
                model.get({id:req.params.id})
                .then((result)=>{
                    console.log(result)
                    result =  result[0]
                    req.body.cctv_status =  result.cctv_cctv_status + 1;
                    req.body.cctv_condition  = 1;
                    next()
                })
            }
            else{   
                  next();
            }
            
          

        }


    }
)