const Controller = require('../crud/controller')
const pathmodel = '../model/Cctv_group'
const model = require(pathmodel)

module.exports = new(
    class cctv_group extends Controller {
        constructor() {
            super();
        }

        modelpath(req, res, next) {
            req.headers.pathmodel = pathmodel;
            req.headers.itemname  = 'Group CCTV'
            next()
        }

    }
)