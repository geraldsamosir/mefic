const Controller = require('../crud/controller')
const pathmodel = '../model/Comment_reports'
const model = require(pathmodel)
const helpercontroller =   require("../crud/conf/helpercontroler")

module.exports = new(
    class comments_reports extends Controller {
        constructor() {
            super();
        }

        modelpath(req, res, next) {
            req.headers.pathmodel = pathmodel;
            req.headers.itemname  = 'komentar laporan'
            next()
        }

        getbyreport(req,res){
            let filter =  req.params
            model.getcommentbyreport(filter)
            .then((result)=>{
                res.status(200)
                res.json(helpercontroller.resulthandle(result));
                res.end()
            })

        }

    }
)