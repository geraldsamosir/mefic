const Controller = require('../crud/controller')
const helpercontroller = require('../crud/conf/helpercontroler')
const pathmodel = '../model/Detail_routes'
const model = require(pathmodel)

var fs = require('fs');



//controller 
const report_model = require('../model/Reports')

//aysn to gmapskey
const request = require('request');
const async = require('async');

//moment js time library
const moment = require('moment')


const routes_all = () => {}

module.exports = new(
    class detail_routes extends Controller {
        constructor() {
            super();

        }

        modelpath(req, res, next) {
            req.headers.pathmodel = pathmodel;
            req.headers.itemname  = 'Rute tujuan'
            req.headers.gmapskey = 'AIzaSyCuGd21zRRGSUYW2rvD-u14zMQXpyYG-OQ'
            req.headers.gmapsdirectionapi = 'https://maps.googleapis.com/maps/api/directions/json?'
            next()
        }

        get_all_routes_bylatlng(req,res,next){
            let data = {}
            data.start = req.query.start;

            data.end = req.query.end.split('+')

            data.request = data.end.map((_data) => {
                return (
                    req.headers.gmapsdirectionapi +
                    'origin=' + data.start + '&destination=' +
                    _data + '&alternatives=true&departure_time=now&traffic_model=optimistic&key=' +
                    req.headers.gmapskey
                    +"&language=id"
                )
            })

            async.map(data.request, (url, callback) => {
                request(url, (error, response, body) => {
                    if (!error && response.statusCode == 200) {
                        var body = JSON.parse(body);
                        callback(null, body);  
                        
                    } 
                })

            }, (err, results) => {
                let _result = results.map((data) => {
                    if(data != undefined){
                        return data.routes.map((data) => {
                            return data
                        })
                    }
                }) 

                req.headers.all_routes = _result ;
                next()
            })
        }
        
        get_all_routes(req, res, next) {
            let data = {}
            data.start = req.query.start;

            data.end = req.query.end.split(',')

            data.request = data.end.map((_data) => {
                return (
                    req.headers.gmapsdirectionapi +
                    'origin=' + data.start + '&destination=' +
                    _data + '&alternatives=true&departure_time=now&traffic_model=optimistic&key=' +
                    req.headers.gmapskey
                    +"&language=id"
                )
            })

            async.map(data.request, (url, callback) => {
                request(url, (error, response, body) => {
                    if (!error && response.statusCode == 200) {
                        try{
                            var body = JSON.parse(body);
                            callback(null, body);
                        }
                        catch(err){
                            
                        }
                        
                    }
                    else{
                        try{
                            callback(error || response.statusCode);
                        }
                         catch(err){
                            
                        }
                    } 
                })

            }, (err, results) => {
                let _result = results.map((data) => {
                    if(data != undefined){
                        return data.routes.map((data) => {
                            return data
                        })
                    }
                }) 

                req.headers.all_routes = _result ;
                next()
            })
        }

         get_routes_by_distance(req, res) {
            let __result = []
            let _result = req.headers.all_routes

            //sorting per cluster destination
            for (let i = 0; i < _result.length; i++) {
                __result[i] = _result[i].sort((a, b) => {
                    return (a.legs[0].distance.value - b.legs[0].distance.value)
                })
            }

            //filter with desination
            __result = __result.sort((a, b) => {
                return a[0].legs[0].duration_in_traffic.value - b[0].legs[0].duration_in_traffic.value
            })


            __result  = __result.map((data,index)=>{
                 return  { result : data }
            })

            let final  = (__result[0].result != "")
                        ?helpercontroller.resulthandle(__result)
                        :helpercontroller.notfoundhandle(req.headers.itemname)
            res.status(200)

            res.json(final)
            res.end()

        }

        get_routes_by_time(req, res) {
            let __result = []
            let _result = req.headers.all_routes

            //sorting per cluster destination
            for (let i = 0; i < _result.length; i++) {
                __result[i] = _result[i].sort((a, b) => {
                    return (a.legs[0].duration.value - b.legs[0].duration.value)
                })
            }

            //filter with desination
            __result = __result.sort((a, b) => {
                return a[0].legs[0].duration_in_traffic.value - b[0].legs[0].duration_in_traffic.value
            })

             __result  = __result.map((data,index)=>{
                 return  { result : data }
            })

            let final  = (__result[0].result != "")
                        ?helpercontroller.resulthandle(__result)
                        :helpercontroller.notfoundhandle(req.headers.itemname)

            
            res.status(200)
            res.json(final)
            res.end()
        }

        get_routes_by_traffic(req, res) {

            // note : 
            //from http://stackoverflow.com/questions/1253499/simple-calculations-for-working-with-lat-lon-km-distance
            // Latitude: 1 deg = 110.574 km
            //0,000004522 deg   =  0.5 km             
            // Longitude: 1 deg = 110.574 km
            // 0,000004522 deg   =  0.5 km 
            //sorting per cluster destination

            //hitung jarak lat dan lng di google map google.maps.geometry.spherical.computeDistanceBetween  satuan keluar meter

            let __result = []
            let _result = req.headers.all_routes


            // const time_now = new Date();
            // time_now.setHours(time_now.getHours() - 1);
            // const time_min = moment(time_now).format('YY-MM-DD hh:mm:ss')

            // report_model.filterbytimeminimal({
            //     minimaltime: time_min
            // }).then((report) => {

                //filter optimal for an destination
                for (let i = 0; i < _result.length; i++) {
                    __result[i] = _result[i].sort((a, b) => {
                        return (a.legs[0].duration_in_traffic.value - b.legs[0].duration_in_traffic.value)
                    })
                }

                //filter with desination
                __result = __result.sort((a, b) => {
                    return a[0].legs[0].duration_in_traffic.value - b[0].legs[0].duration_in_traffic.value
                })

                 __result  = __result.map((data,index)=>{
                     return  { result : data }
                 })
                let final  = (__result[0].result != "")
                            ?helpercontroller.resulthandle(__result)
                            :helpercontroller.notfoundhandle(req.headers.itemname)
                res.status(200)
                res.json(final)
                res.end()
            //})


        }

        //make value of route by google map and report by users who disturb
        get_conf_value(req,res){
            fs.readFile("./appconf/routesconf.json",function(err,content){
                 res.status(200)
                 res.json(helpercontroller.resulthandle(JSON.parse(content)))
                 res.end()
            })
        }

        update_conf_value(req,res){
           
            let data  =  req.body;
            fs.writeFile("./appconf/routesconf.json",JSON.stringify(data),(err)=>{
                res.status(200)
                res.json(helpercontroller.updatehandle("reportvalue"))
                res.end()
            })
        }

        

    }
)