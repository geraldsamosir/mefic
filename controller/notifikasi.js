const Controller = require('../crud/controller')
const pathmodel = '../model/Notification_reports'
const model = require(pathmodel)
const helpercontroller =   require("../crud/conf/helpercontroler")

module.exports = new(
    class comments_reports extends Controller {
        constructor() {
            super();
        }

        modelpath(req, res, next) {
            req.headers.pathmodel = pathmodel;
            req.headers.itemname  = 'komentar laporan'
            next()
        }

        costumcreate(req,res,next){
            let data = {};
            Object.assign(data,req.body)

            delete data.conten_creator_email
            delete data.users_name;
            delete data.users_urlphoto;
            model.create(data)
            .returning('id')
            .then((id)=>{
               // delete  req.body.creator_users_id;
               req.body.notifid = id[0]
                next()
             })
        }

        getbyusers(req,res){
            let filter =  req.params
            model.getallnotiftomebyuserid(filter)
            .then((result)=>{
                res.status(200)
                res.json(helpercontroller.resulthandle(result));
                res.end()
            })

        }

    }
)