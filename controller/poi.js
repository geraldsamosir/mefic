const Controller = require('../crud/controller')
const helpercontroller = require('../crud/conf/helpercontroler')
const pathmodel = '../model/Poi'
const model = require(pathmodel)

const request = require('request');

module.exports = new(
    class Poi extends Controller {
        constructor() {
            super();
        }

        modelpath(req, res, next) {
            req.headers.pathmodel = pathmodel;
            req.headers.itemname  = 'Point of interest'
            req.headers.gmapskey = 'AIzaSyCuGd21zRRGSUYW2rvD-u14zMQXpyYG-OQ'
            req.headers.gmapsnearbysearchapi = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?'
            next()
        }


        category (req,res){
            // atm, restaurant, hospital, bank, shopping mall,  gas station, supermarket, pharmacy, hotel, car repair
            let category = [
                {name :'ATM',params : 'atm' , icon:"https://maps.gstatic.com/mapfiles/place_api/icons/atm-71.png"},
                {name :'Bank' , params : 'bank',icon:"https://maps.gstatic.com/mapfiles/place_api/icons/bank_dollar-71.png" },
                {name :'Bengkel' , params : 'car_repair' ,icon :"https://maps.gstatic.com/mapfiles/place_api/icons/repair-71.png" },
                {name :'Hotel',params:'hotel',icon:"https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png"},
                {name : 'Minimarket' , params : 'minimarket' ,icon:'https://maps.gstatic.com/mapfiles/place_api/icons/shopping-71.png'},
                {name :'Pom Bensin',params : 'gas_station',icon:"https://maps.gstatic.com/mapfiles/place_api/icons/gas_station-71.png"},
                {name :'Rumah Sakit',params : 'Rumah sakit umum',icon:"https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png" },
                {name : 'Restaurant',params:'restaurant', icon:"https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png"},
                {name : 'Toko' , params : 'shoping', icon :"https://maps.gstatic.com/mapfiles/place_api/icons/shopping-71.png"},
                {name : 'Toko Obat / Apotik' , params :'apotik' ,icon:"https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png"},
                
               
            ]
            res.status(200);
            res.json(helpercontroller.resulthandle(category))

        }

        getpoi(req, res) {
            let data = {}
            let result = {}
            let radius = 1500;
            let _mylocation =[]
                  
            data.type = req.params.types


            let opennow = (data.type == 'gas_station' )?
                            "" : "&opennow=true"
            
            if( data.type=="bank" || data.type=="apotik" || data.type=="rumah sakit umum"){
                radius = 3000
            }
            if(data.type == "hotel" || data.type=="supermarket"){
                radius = 5000
            }

            if( data.type =="minimarket"){
                data.type = ['indomaret','minimarket']
                opennow =""
            }

            data.mylocation = req.query.mylocation

            _mylocation = data.mylocation.split(',')


            let optionfilter  =  (data.type == 'gas_station' )? "&type="+data.type:"&name="+data.type

            const get = (callback) => {
                request(req.headers.gmapsnearbysearchapi +
                    "location=" + data.mylocation +
                    "&radius=" + radius +
                    opennow
                    +optionfilter+
                    "&key=" + req.headers.gmapskey
                     +"&language=id",
                    (err, response, body) => {

                        let _body = JSON.parse(body)
                        console.log(_body.status)
                        if (_body.status == "OK") {
                            callback(_body)
                        } else {
                            radius = radius + 1000
                          
                            get(callback)
                        }

                    }

                )
            }
            get((data) => {
                let _data =  data

                const rad = (x)=>{
                    return x * Math.PI / 180;
                };

                const getDistance = (p1, p2) =>{
                    var R = 6378137; // Earth’s mean radius in meter
                    var dLat = rad(p2.lat - p1.lat);
                    var dLong = rad(p2.lng - p1.lng);
                    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                        Math.cos(rad(p1.lat)) * Math.cos(rad(p2.lat)) *
                        Math.sin(dLong / 2) * Math.sin(dLong / 2);
                    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
                    var d = R * c;
                    return  (d /1000).toFixed(2) //in km
                };
                _data  = _data.results.map((data)=>{
                    data.distance = getDistance({lat:_mylocation[0],lng:_mylocation[1]} ,data.geometry.location)
                    return data
                })
                
                data  =  _data.sort((a,b)=>{
                    return (
                      a.distance  -   b.distance
                    )
                })
                let result =(data !=[]) ? helpercontroller.resulthandle(data) :helpercontroller.notfoundhandle(req.headers.itemname)
                res.json(result)
            })

            
        }


    }
)