const Controller = require('../crud/controller')
const pathmodel = '../model/Reports'
const model = require(pathmodel)
const helpercontroller =   require("../crud/conf/helpercontroler")

module.exports = new(
    class report extends Controller {
        constructor() {
            super();
        }

        modelpath(req, res, next) {
            req.headers.pathmodel = pathmodel;
            req.headers.itemname ="Report"
            next()
        }

        reportdetailall(req,res){
            let mymodel = model.reportdetailall()
            if(req.query.type == 'admin'){
                mymodel =  mymodel.orderBy('reports.created_at' ,'DESC')
            }
            else{
                //change with report time
                mymodel =  mymodel.orderBy('reports.created_at')
                mymodel.whereRaw(
                    "created_at >= date_sub(NOW(), interval 1 hour)"
                )
            }
            
            mymodel.then((result)=>{
                result.map((data)=>{
                    data.reports_likedislike_id  =  JSON.parse(data.reports_likedislike_id)
                })
                res.json(helpercontroller.resulthandle(result))
            })
        }

        getbyid(req,res){
            let filter =  { id : req.params.id}
            let mymodel = model.reportdetailall()
            mymodel.whereRaw(
                "created_at >= date_sub(NOW(), interval 1 hour)"
            ).where({
                "reports.id" :filter.id
            })

            mymodel.then((result)=>{
                 result.map((data)=>{
                    data.reports_likedislike_id  =  JSON.parse(data.reports_likedislike_id)
                })
                res.status(200)
                res.json(helpercontroller.resulthandle(result))
                res.end()
            })

        }


        mobilereportlike(req,res){
            let data = req.params ;
            let user_likedislike_id =  {id_users: req.body.id_users , status: "like"} 
            let userslikedislike_id = []
            model.get(data).then((result)=>{
                if(result == ""){
                    res.status(404)
                    res.json(helpercontroller.notfoundhandle(req.headers.itemname))
                    res.end()   
                }
                else{
                    let _result  = {}
                    userslikedislike_id =  JSON.parse(result[0].reports_likedislike_id)
                    userslikedislike_id =  userslikedislike_id.concat(user_likedislike_id)
                    
                    _result.like = result[0].reports_like
                    _result.like = _result.like + 1 
                    _result.likedislike_id = JSON.stringify(userslikedislike_id) 
                    model.update(_result,data)
                    .then((response)=>{
                        res.status(200)
                        res.json(helpercontroller.updatehandle(req.headers.itemname))
                        res.end()
                    })
                }
                
            })

        }

        mobilereportdislike(req,res){
            let data = req.params ;
            let user_likedislike_id =  {id_users: req.body.id_users ,status: "dislike"}
            let userslikedislike_id = []
            model.get(data).then((result)=>{
                if(result == ""){
                    res.status(404)
                    res.json(helpercontroller.notfoundhandle(req.headers.itemname))
                    res.end()
                }
                else{
                    let _result  = {}
                    userslikedislike_id =  JSON.parse(result[0].reports_likedislike_id)
                    userslikedislike_id =  userslikedislike_id.concat(user_likedislike_id)
                    _result.dislike = result[0].reports_dislike
                    _result.dislike = _result.dislike + 1 

                    _result.likedislike_id = JSON.stringify(userslikedislike_id) 
                    model.update(_result,data)
                    .then((response)=>{
                        res.status(200)
                        res.json(helpercontroller.updatehandle(req.headers.itemname))
                        res.end()
                    })
                }
                
            })
        }

        mobilecancelrating(req,res){
            let data = req.params ;
            let type =  data.type
            let user_likedislike_id =  {id_users: req.body.id_users}
            let userslikedislike_id = []
            delete data.type;
            model.get(data).then((result)=>{
                if(result == ""){
                    res.status(404)
                    res.json(helpercontroller.notfoundhandle(req.headers.itemname))
                    res.end()
                }
                else{
                    let _result  = {}
                    userslikedislike_id =  JSON.parse(result[0].reports_likedislike_id)
                    userslikedislike_id =  userslikedislike_id.filter((data)=>{
                        return data.id_users != user_likedislike_id.id_users
                    })
                    _result.likedislike_id =  JSON.stringify(userslikedislike_id)
                    if(type == "dislike"){
                        _result.dislike = result[0].reports_dislike
                        _result.dislike = _result.dislike - 1 
                    }
                    else if(type == "like"){
                        _result.like = result[0].reports_like
                        _result.like = _result.like - 1 
                        
                    }

                   model.update(_result,data)
                    .then((response)=>{
                        res.status(200)
                        res.json(helpercontroller.updatehandle(req.headers.itemname))
                        res.end()
                    })
                }
                
            })
        }

        sum_like_dislike(req,res, next){
            let data  =  req.params
            model.sum_like_dislike(data)
            .then((result)=>{
                req.headers.userslike = {
                    like : result[0].totallike || 0,
                    dislike : result[0].totaldislike || 0
                }
                next();
            })
        }
    }
)