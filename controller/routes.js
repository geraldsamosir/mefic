const Controller = require('../crud/controller')
const pathmodel = '../model/Routes'
const model = require(pathmodel)
const helpercontroller = require('../crud/conf/helpercontroler')

module.exports = new(
    class routes extends Controller {
        constructor() {
            super();
        }

        modelpath(req, res, next) {
            req.headers.pathmodel = pathmodel;
            req.headers.itemname ="Group Rute"
            next()
        }

        costumcreate(req,res){
            let data  =  req.body;
            model.costumcreate(data)
            .then((id)=>{
                let result = {
                    id : id[0]
                }
                let final =  helpercontroller.createdhandle(req.headers.itemname)
                final.result = result
                res.status(201)
                res.json(final)
                res.end()
            })
        }

    }
)