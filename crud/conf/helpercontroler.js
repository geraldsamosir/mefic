module.exports = new (class helpercontroller{

    errhandle(_err){

        return({
              code : 500 ,
              result : [],
              message : "server error",
              err :_err
        })
        
    }

    resulthandle(result){
        return({
              code  :200,
              result : result,
              message : "ok",
              err :"no"
        })
    }

    notfoundhandle(nama_table){
        return({
              code  :404,
              result : [],
              message : "yang dicari di "+nama_table+" tidak di temukan",
              err :"no"
        })
    }

    errlogin(){
        return({
              code  :403,
              result : [],
              message : "email atau password salah",
              err :"no"
        })
    }

    notauth(){
        return({
              code  :401,
              result : [],
              message : "anda tidak memiliki hak mengakses menu ini",
              err :"no"
        })
    }
    createdhandle(nama_table){
        return({
             code  :201,
             result : [],
            message : nama_table+" berhasil di buat",
            err :"no"

        })        
    }

    updatehandle(nama_table){
        return({
            code  :200,
            result : [],
            message : nama_table+" berhasil di update",
            err :"no"

        })        
    }

    deletehandle(nama_table){
        return({
            code  :200,
            result : [],
            message : nama_table+" berhasil di hapus",
            err :"no"

        })      
    }

})