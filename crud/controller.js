const helpercontroller =   require("./conf/helpercontroler")


module.exports = class controller {


    //get alldata from the table
    all(req, res) {
        let pathmodel = req.headers.pathmodel
        let model = require(pathmodel)
        model.all()
            .then((result) => {
                res.status(200)
                res.json(helpercontroller.resulthandle(result))
                res.end()
            })
            .catch((err) => {
                res.status(500)
                console.log(err)
                res.json(helpercontroller.errhandle(err))
            })
    }



    //get data with filter by params
    /* get the object params as filter */
    get(req, res) {
        let pathmodel = req.headers.pathmodel
        let model = require(pathmodel)
        let filter = req.params;
        model.get(filter)
            .then((result) => {
               
                res.status(200)
                let final = (result != "")? 
                            helpercontroller.resulthandle(result):
                            helpercontroller.notfoundhandle(req.headers.itemname)

                res.json(final)
                res.end()
            })
            .catch((err) => {
                res.status(500)
                res.json(helpercontroller.errhandle(err))
                res.end()
            })

    }


    //create  or add data to table database
    create(req, res) {

        let pathmodel = req.headers.pathmodel
        let model = require(pathmodel)
        let data = req.body
        
        model.create(data)
            .then(() => {
                res.status(201)
                res.json(helpercontroller.createdhandle(req.headers.itemname))
                res.end()
            })
            .catch((err) => {
                console.log(err)
                res.status(500)
                res.json(helpercontroller.errhandle(err))
                res.end()
            })
    }

    //update an item data
    /* udpate data of item in database using filter by params */
    update(req, res) {
        let pathmodel = req.headers.pathmodel
        let model = require(pathmodel)
        let data = req.body
        let filter = req.params
        model.update(data, filter)
            .then(() => {
                res.status(200)
                res.json(helpercontroller.updatehandle(req.headers.itemname))
                res.end()
            })
            .catch((err) => {
                res.status(400)
                res.json(helpercontroller.errhandle(err))
            })

    }

    //delete data 

    /* delete an item in table by params as filter*/
    delete(req, res) {
        let pathmodel = req.headers.pathmodel
        let model = require(pathmodel)
        let filter = req.params
        model.delete(filter)
            .then(() => {
                res.status(200)
                res.json(helpercontroller.deletehandle(req.headers.itemname))
            })
            .catch((err) => {
                res.status(500)
                res.json(helpercontroller.errhandle(err))
            })
    }


}