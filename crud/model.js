const knex = require('../db')

module.exports = class model {

    constructor() {

        //abstract of tablename

        this.tablename = ''

        //all fieldname tablename
        /**
         * abstract of table filed
         * example : 
         *  [
         *     'filed1',
         *     'filed 2'
         *       ...
         *  ]
         * 
         */
        this.fieldname = []


        //abstract of filed allow to show
        /**
         * filed  show  in rest or request api
         * example : 
         *  [
         *     'filed1',
         *     'filed 2'
         *       ...
         *  ]
         * in clinet and exceute by  mapingandprotected
         */
        this.fieldnameshow = []



        //protected
        /**
         * filed not show  in rest or request api
         * example : 
         *  [
         *     'filed1',
         *     'filed 2'
         *       ...
         *  ]
         * in clinet and exceute by  mapingandprotected
         */
        this.protectedfiled = []



        //table joins
        /**
         * this value example:
         *  [{
         *      tablename  : //table parent of child file 
         *      parent : //id join from parent of child file 
         *      target ://id join from  another file parent (foreign key )
         *  }]
         * 
         * note  parent  is  same with table
         */
        this.tablejoin = []

    }


    //inhetience function
    /**
     * this function can  use wihout combine with
     * model function just extend this file to model
     * you can use this function wihout  rewrite or combine it 
     * with child  model funcion
     */

    //get all in table  except  proteted
    all() {
        return knex.select(this.fieldnameshow)
            .from(this.tablename)

    }

    //filter table by table field
    get(filter) {
        return knex.select(this.fieldnameshow)
            .from(this.tablename)
            .where(filter)
    }

    //add table item
    create(data) {
        return knex(this.tablename)
            .insert(data)
    }

    //update table data using filter by table field
    update(data, filter) {
        return knex(this.tablename)
            .where(filter)
            .update(data)
    }

    //delete table data using filter by table field
    delete(filter) {
        return knex(this.tablename)
            .where(filter)
            .del()
    }

    //get all the field database information
    validation() {
        return knex(this.tablename).columnInfo(this.tablename)
    }



    ///function must use and combine 
    /** 
     * this function canot use stand alone must combine with 
     * model child function to solve probelms
     */
    //maping table name and protected field


    //re use knex js function
    table(filter) {
        return knex
            .select(filter)
            .from(this.tablename)
    }

    mapingandprotected() {
        this.fieldnameshow = this.fieldname.filter((field) => {
            if (this.protectedfiled.indexOf(field) == -1) {
                return field
            }
        })
        this.fieldnameshow = this.fieldnameshow.map((field) => {
            return this.tablename + '.' + field + ' as ' + this.tablename + '_' + field
        })
    }

    // unlimited join table
    joining(filter, table) {
        const joins = knex(this.tablename)
        joins.select(filter)
        joins.from(this.tablename)
        table.map((isi) => {
            joins.join(isi.tablename, isi.parent, isi.target)
        })
        return joins

    }


}