const helpercontroller =   require("./conf/helpercontroller")
module.exports = new(class validation extends helpercontroller{
    constructor() {

    }

    //validate data field
    validate(req, res, next) {
        let pathmodel = req.headers.pathmodel
        let model = require(pathmodel)

        //for value validation
        let status = true

        //get req.body property name  in object
        let key = Object.keys(req.body)
        model.validation()
            .then((_validate) => {
                status = status && key.map((field) => {
                    if (_validate[field].type == 'int' && status == true) {
                        let isi = parseInt(req.body[field])
                        return (isi == req.body[field]) ? true : false;
                    } else if (status == true) {
                        let isi = typeof field
                        if (_validate[field].type == 'text' || 'varchar') {
                            _validate[field].type = 'string'
                        }
                        return isi == _validate[field].type &&
                            req.body[field].length <= _validate[field].maxLength
                    }
                })

                status = status.reduce((value, xvalue) => { return value && xvalue })
                if (status) {
                    next()
                } else {
                    res.status(400)
                    res.json(this.notfoundhandle(req.headers.itemname))
                    res.end()
                }
            })

    }
})