const usermodel = require('../model/users')

const hash = require('password-hash')
const jwt = require('jwt-simple');

//mail
const mail = require('../service/mail')

//crud helpercontroller
const helpercontroller  = require("../../../../crud/conf/helpercontroler")


module.exports = new(

    class auth  {

        login(req, res) {
            let data = req.body

            if(!data.email  || !data.password ){
                res.status(403)
               res.json({
                    code  :403,
                    result : [],
                    message : "password dan username tidak boleh kosong",
                    err :"no"
                })
                res.end()
            }
            
                let is_auth = ""
                usermodel.get_by_email(data)
                    .then((user) => {
                        if(user != undefined){
                            is_auth = hash.verify(
                                data.password,
                                user.password
                            )
                            if(is_auth == false){
                                res.status(403)
                                res.json(helpercontroller.errlogin())
                                res.end()
                            }
                            else{
                                if (is_auth && user.token == '' || is_auth && user.token == null) {
                                    user.authtoken = jwt.encode(
                                        data.password,
                                        req.app.get('key')
                                    )
                                    console.log(user)
                                    let response = {
                                        id: user.id,
                                        username: user.username,
                                        email: user.email,
                                        roles: user.roles,
                                        authtoken: user.authtoken,
                                        urlphoto : user.urlphoto
                                    }
                                    res.status(200)
                                    res.json(helpercontroller.resulthandle(response))
                                    res.end()
                                } else if (user.token != '' || is_auth && user.token != null) {

                                    res.status(401)
                                    res.json({
                                                code  :401,
                                                result : [],
                                                message : "silahkan cek email anda untuk mengaktifasi akun anda",
                                                err :"no"
                                            })
                                } else {
                                    res.status(403)
                                    res.json(helpercontroller.errlogin())
                                    res.end()
                                }
                            }
                     }
                      else {
                                res.status(403)
                                res.json(helpercontroller.errlogin())
                                res.end()
                            }
                   })
            
        }


        getdatatoken(req, res) {
            let data = {}
            data.token = req.params.token



            usermodel.get_by_token(data)
                .then((user) => {
                    res.status(200)
                    res.json(helpercontroller.resulthandle(user[0]))
                    res.end()
                })
        }

        forgotpassword(req, res) {
            let data = req.params
            let domain = req.headers.host
            usermodel.get_by_email(data)
                .then((result) => {
                    let _data = {
                        email: result.email,
                        token: hash.generate(result.email)
                    }

                    const _result = mail.sendmail(_data, domain, 'forgot')
                    if (_result) {
                        usermodel.update_token(_data)
                            .then(() => {
                                res.status(200);
                                res.json({
                                    code  :200,
                                    result : [],
                                    message : "silahkan cek email anda untuk mereset password anda",
                                    err :"no"
                                })
                                res.end()
                            })
                    }
                })

        }
        updatepassword(req, res) {
            
            let data = req.body;
            data.password = hash.generate(
                data.password
            );
            usermodel.update_password(data)
                .then(() => {
                    res.status(200)
                    res.json(helpercontroller.updatehandle("user"))
                    res.end()
                })

        }

        register(req, res) {
            let _data = req.body;
            let domain = req.headers.host
            _data.token = hash.generate(_data.email)
            _data.password = hash.generate(
                _data.password
            );
            usermodel.add(_data)
                .then(() => {
                    const result = mail.sendmail(_data, domain, 'activate')
                        //   const result = true
                    if (result == true) {
                        res.status(201)
                        res.json(helpercontroller.createdhandle("user"))
                    } else {
                        res.json(helpercontroller.errhandle("user"))
                    }
                })
        }

    }
)