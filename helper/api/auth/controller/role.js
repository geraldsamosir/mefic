const knex = require('../db.js')

module.exports = new(

    class roles {
        all() {
            return knex('roles')

        }
        get(_data) {
            return knex('roles')
                .where({
                    id: _data.id
                })
        }
        update(_data) {
            return knex('roles')
                .update({
                    status: _data.status
                })
                .where({
                    id: _data.id
                })
        }
        delete(_data) {
            return knex('roles')
                .where({
                    id: _data.id
                })
                .del()
        }

        add(_data) {
            return knex('roles')
                .insert(_data)
        }


    })