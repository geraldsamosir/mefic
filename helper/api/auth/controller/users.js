const usermodel = require('../model/users')
const hash = require('password-hash');

//crud helpercontroller
const helpercontroller  = require("../../../../crud/conf/helpercontroler")


module.exports = new(
    class users {

        all(req, res) {
            usermodel.all()
                .select(
                    'users.id  as id',
                    'username',
                    'email',
                    'address',
                    'phone',
                    'roles',
                    'like',
                    'dislike'

                )
                .sum('like as  Like')
                .sum('dislike as Dislike')
                .join('reports','users.id','reports.users_id')
                .groupBy('username')
                .then((users) => {
                    res.status(200)
                    res.json(helpercontroller.resulthandle(users))
                    res.end()
                })
        }

        getbyemail(req, res) {
            let _data = (req.body.email != undefined) ? req.body : req.params
            usermodel.get_by_email(_data)
                .then((user) => {
                    if(user != undefined ){
                        res.status(200)
                        user  =  Object.assign(user,req.headers.userslike)
                        res.json(helpercontroller.resulthandle(user))
                        res.end()
                    }
                    else{
                        res.status(404)
                        res.json({
                            code: 404,
                            message: "User tidak di temukan",
                            err: "no"
                        })
                        res.end()
                    }
                })

        }


        updateprofil(req, res) {
            let _data = req.body
            
            if(_data.password != undefined) {
                _data.password = hash.generate(_data.password)
            }
            console.log(_data.password)
            let id = req.params.id
            usermodel.update_profil(_data, id)
                .then(() => {
                    res.status(200)
                    res.json(helpercontroller.updatehandle("user profil"))
                    res.end()
                })
        }

        delete(req, res) {
            let _data = req.params
            usermodel.delete(_data)
                .then(() => {
                    res.status(200)
                    res.json(helpercontroller.deletehandle("user profil"))
                    res.end();
                })

        }

    }
)