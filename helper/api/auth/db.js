var knex = {};
knex.local = require('knex')({
    client: 'mysql',
    connection: {
        host: 'localhost',
        user: 'root',
        password: '',
        database: 'Mefic-TA'
    }
});

module.exports = knex.local;