const hash = require('password-hash')
const jwt = require('jwt-simple');

const usermodel = require('../model/users')

//crud helpercontroller
const helpercontroller  = require("../../../../crud/conf/helpercontroler")

module.exports = new(
    class auth {

        //check if email has used
        is_exist(req, res, next) {
            let data = req.body;
            usermodel.get_by_email(data)
                .then((users) => {
                    if (users == undefined) {
                        next()
                    } else {
                        res.status(409)
                        res.json({
                              code  :409,
                              result : [],
                              message : "email ini telah digunakan",
                              err :"no"
                        })
                        res.end()
                    }

                })
        }

        is_mail_exist(req, res, next) {
            let data = req.params;
            usermodel.get_by_email(data)
                .then((users) => {
                    if (users != undefined) {
                        next()
                    } else {
                        res.status(404)
                        res.json({
                              code  :404,
                              result : [],
                              message : "email ini tidak di temukan",
                              err :"no"
                        })
                        res.end()
                    }

                })
        }

        //check  administrator

        is_admin(req, res, next) {

            let data = req.headers;
            usermodel.get_by_email(data)
                .then((result) => {
                    if (result.roles == 2) {
                        next()
                    } else {
                        res.status(403);
                        res.json("anda tidak memiliki hak akses halaman ini");
                        res.end();
                    }
                })
        }

        //check auth to access page or method
        is_auth(req, res, next) {
            
            let data = {
                email: req.headers.email || null,
                token: req.headers.token || null,
            }
            let result = (data.email != null && data.token != null) ? true : false;
            if (result == true) {
                try {
                    data.token = jwt.decode(
                    data.token,
                    req.app.get('key')
                )    
                } catch (error) {
                }
                
                usermodel.get_by_email(data)
                    .then((user) => {
                        let cektoken = hash.verify(
                            data.token,
                            user.password
                        )
                        if (cektoken == true) {
                            next();
                        } else {
                            res.status(401)
                            res.json(helpercontroller.notauth())
                            res.end()
                        }

                    })
                    .catch((e)=>{
                        res.status(401)
                        res.json(helpercontroller.notauth())
                        res.end()
                    })
            } else {
                res.status(401)
                res.json(helpercontroller.notauth())
                res.end()
            }



        }

    }
)