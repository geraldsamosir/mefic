const knex = require('../db.js')

module.exports = new(

    class roles {
        //get all roles model
        all() {
            return knex('roles')

        }

        //get roles by id
        get(_data) {
            return knex('roles')
                .where({
                    id: _data.id
                })
        }

        //update data roles
        update(_data) {
            return knex('roles')
                .update({
                    status: _data.status
                })
                .where({
                    id: _data.id
                })
        }

        //delete data roles
        delete(_data) {
            return knex('roles')
                .where({
                    id: _data.id
                })
                .del()
        }

        //add data roles
        add(_data) {
            return knex('roles')
                .insert(_data)
        }


    })