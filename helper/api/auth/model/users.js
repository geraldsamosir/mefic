const knex = require('../db.js')

module.exports = new(

    class UserModel {


        //get all users data
        all() {
            return knex('users')
        }

       
       

        //get user data by email
        get_by_email(_data) {
            return knex('users')
                .where({
                    email: _data.email
                })
                .first()
        }

        //get user data by token
        get_by_token(_data) {
            return knex('users')
                .where({
                    token: _data.token
                })
        }

        //update user profil
        update_profil(_data, id) {
            return knex('users')
                .update(_data)
                .where({
                    id: id
                })
        }

        //update user password
        update_password(_data) {
            return knex('users')
                .update({
                    password: _data.password,
                    token: ''
                })
                .where({
                    token: _data.token
                })
        }

        //update token

        update_token(_data) {
            return knex('users')
                .update({
                    token: _data.token
                })
                .where({
                    email: _data.email
                })
        }

        //add users data
        add(_data) {
            return knex('users')
                .insert(_data)
        }

        //delete user
        delete(_data) {
            return knex('users')
                .where({
                    id: _data.id
                })
                .del()
        }

    }
)