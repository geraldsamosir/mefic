const fs = require('fs');
const file = require('../crud/file')
//model
const Users_model = require('../model/Users')

module.exports = new(
    class reportfile extends file {
        constructor() {
            super()
            this.conf = {
                folderpath: './public/client/profil',
                filetype: '.png',
                fieldname: 'urlphoto'
            }
        }
        env() {
            return this.conf
        }

        deleteimage(req, res, next) {
            if (req.file.originalname) {
                let data = {}
                data.id = req.params.id
                Users_model.get(data)
                    .then((result) => {
                        if (result[0].users_urlphoto != '' && result[0].users_urlphoto != null ) {
                            fs.unlinkSync('./public' + result[0].users_urlphoto);
                        }
                        next()
                    })
            }
            else{
                next()
            }
            
        }

        getfilename(req, res, next) {
            if (req.file.originalname) {
                req.body.urlphoto = '/client/profil/' + req.file.newname
            }
            next();
        }

    }
)