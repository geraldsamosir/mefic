const file = require('../crud/file')


module.exports = new(
    class reportfile extends file {
        constructor() {
            super()
            this.conf = {
                folderpath: './public/client/report',
                filetype: '.png',
                fieldname: 'photo_url'
            }
        }
        env() {
            return this.conf
        }

        getfilename(req, res, next) {
            if (req.file.originalname) {
                req.body.photo_url = '/client/report/' + req.file.newname
            }
            next();
        }

    }
)