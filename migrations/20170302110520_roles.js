exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('roles', function(table) {
            table.increments('id').primary();
            table.string('status');

        })
        //insert data roles
        .then(() => {
            return knex('roles').insert([
                { id: 1, status: 'User' },
                { id: 2, status: 'Admin' }
            ]);
        })
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTable('roles')
};