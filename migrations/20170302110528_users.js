const hash = require('password-hash')

exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('users', function(table) {
            table.increments('id').primary();
            table.string('username');
            table.string('password');
            table.string('email').unique().collate('utf8_unicode_ci');
            table.integer('roles').defaultTo(1).unsigned();
            table.integer('gender').defaultTo(1);
            table.string('age').defaultTo("");
            table.string('phone').defaultTo("");
            table.string('address').defaultTo("");
            table.string('urlphoto').defaultTo("");
            table.integer('privacy').defaultTo(0);
            table.text('token').defaultTo("");

            table.foreign('roles')
                .references('roles.id')
                .onDelete('cascade')
                .onUpdate('cascade');

        })
        .then(() => {
            return knex('users').insert([{
                id: 1,
                username: 'mefic',
                password: hash.generate('mdntraffic2017'),
                email: "mefic2017@gmail.com",
                roles: 2,
                //gender 1 lk  2 pr
                gender: 1,
                privacy: 1,

            }])
        })
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTable('users')
};