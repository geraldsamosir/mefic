exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('category_reports', function(table) {
            table.increments('id').primary();
            table.string('name_category_report').notNullable();
            table.text('icon_url').notNullable();
            table.integer('is_disturb_route').defaultTo(0);

        })
        .then(() => {
            return knex('category_reports').insert([{
                id: 1,
                name_category_report: "road hazard",
                is_disturb_route: 1
            }])
        })
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTable('category_reports');
};