exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('reports', function(table) {
            table.increments('id').primary();
            table.string('name_report').notNullable();
            table.text('photo_url').notNullable();
            table.string('lat').notNullable()
            table.string('lng').notNullable()
            table.integer('like').defaultTo(0);
            table.string("likedislike_id").defaultTo("[]")
            table.integer('dislike').defaultTo(0);
            table.timestamp('created_at').defaultTo(knex.fn.now());

            table.integer('users_id').unsigned();
            table.foreign('users_id')
                .references('users.id')
                .onDelete('cascade')
                .onUpdate('cascade');

            table.integer('category_report_id').unsigned();
            table.foreign('category_report_id')
                .references('category_reports.id')
                .onDelete('cascade')
                .onUpdate('cascade');
        });
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTable('reports')
};