exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('notification_reports', function(table) {
            table.increments('id').primary();
            table.text('body')

            table.integer('report_id').unsigned();
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.foreign('report_id')
                .references('reports.id')
                .onDelete('cascade')
                .onUpdate('cascade');

            table.integer('comment_users_id').unsigned();
            table.string('creator_users_id').unsigned();

            table.foreign('comment_users_id')
                .references('users.id')
                .onDelete('cascade')
                .onUpdate('cascade');
            
            table.foreign('creator_users_id')
                .references('users.id')
                .onDelete('cascade')
                .onUpdate('cascade');

        });
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTable('notification_reports');
};