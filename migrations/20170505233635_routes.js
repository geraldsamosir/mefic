exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('routes', function(table) {
            table.increments('id').primary();
            table.string('name')

            table.integer('created_users_id').unsigned();
            table.foreign('created_users_id')
                .references('users.id')
                .onDelete('cascade')
                .onUpdate('cascade');

        });
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTable('routes');
};