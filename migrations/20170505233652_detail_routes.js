exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('detail_routes', function(table) {
            table.increments('id').primary();
            table.string('name_routes')
            table.string('lat')
            table.string('long')
            table.integer('index')
            table.integer('is_lock')

            table.integer('routes_id').unsigned();
            table.foreign('routes_id')
                .references('routes.id')
                .onDelete('cascade')
                .onUpdate('cascade');

        });
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTable('detail_routes');
};