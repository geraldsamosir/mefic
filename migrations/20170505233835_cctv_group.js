exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('cctv_groups', function(table) {
            table.increments('id').primary();
            table.string('name_cctv_group')

            table.integer('created_users_id').unsigned();
            table.foreign('created_users_id')
                .references('users.id')
                .onDelete('cascade')
                .onUpdate('cascade');

        })
        .then(() => {
            return knex('cctv_groups').insert(
                [{
                        id: 1,
                        name_cctv_group: 'ATCS Medan',
                        created_users_id: 1
                    },
                    {
                        id: 2,
                        name_cctv_group: 'ATCS Bandung',
                        created_users_id: 1
                    },
                    {
                        id: 3,
                        name_cctv_group: 'ATCS Bali',
                        created_users_id: 1
                    },
                    {
                        id: 4,
                        name_cctv_group: 'ATCS Pekalongan',
                        created_users_id: 1
                    }

                ])
        })
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTable('cctv_groups');
};