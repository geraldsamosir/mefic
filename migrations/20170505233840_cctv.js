exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('cctv', function(table) {
            table.increments('id').primary();
            table.string('name_cctv')
            table.string('url_cctv')

            //error >1  ,good =0
            table.integer('cctv_status').defaultTo(0)
            table.string('address')
            table.string('lat')
            table.string('long')

            table.integer('cctv_groups_id').unsigned();
            //cctv condition if  0 =  save ,1= warning ,2=brokens
            table.integer('cctv_condition').defaultTo(0);
            table.foreign('cctv_groups_id')
                .references('cctv_groups.id')
                .onDelete('cascade')
                .onUpdate('cascade');

        })
        .then(() => {
            return knex('cctv').insert(
                [{
                    
                }])
        });
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTable('cctv');
};