const Model = require('../crud/model')

module.exports = new(
    class Category_reports extends Model {

        constructor() {
            super()
            this.tablename = 'category_reports';

            this.fieldname = [
                'id',
                'name_category_report',
                'icon_url',
                'is_disturb_route'
            ]

            this.protectedfiled = []

            this.mapingandprotected()

        }

    }
);