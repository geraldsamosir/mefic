const Model = require('../crud/model')


module.exports = new(
    class Cctv extends Model {

        constructor() {
            super()
            this.tablename = 'cctv';

            this.fieldname = [
                'id',
                'name_cctv',
                'url_cctv',
                'cctv_status',
                'address',
                'lat',
                'long',
                'cctv_groups_id',
                'cctv_condition'
            ]

            this.protectedfiled = []

            this.mapingandprotected()

        }

    }
);