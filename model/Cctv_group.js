const Model = require('../crud/model')

//model joining
const Cctv = require('./Cctv');

module.exports = new(
    class Cctv_group extends Model {

        constructor() {
            super()
            this.tablename = 'cctv_groups';

            this.fieldname = [
                'id',
                'name_cctv_group',
                'created_users_id'
            ]


            this.protectedfiled = []

            this.mapingandprotected()

        }


    }
);