const Model = require('../crud/model')


//joining model

const Users = require('./Users');

module.exports = new(
    class Comment_reports extends Model {

        constructor() {
            super()
            this.tablename = 'comment_reports';

            this.fieldname = [
                'id',
                'body',
                'created_at',
                'report_id',
                'comment_users_id'
            ]

            this.protectedfiled = []
            this.tablejoin = [

                {
                    tablename: Users.tablename,
                    parent: Users.tablename + '.' + Users.fieldname[0],
                    target: this.tablename + '.' + this.fieldname[4]
                }

            ]

            this.mapingandprotected()

        }

        getcommentbyreport(data) {
            const costum_fieldnameshow = this.fieldnameshow.concat(Users.fieldnameshow)
            const table = this.tablejoin
            return this.joining(costum_fieldnameshow, table)
                .where(data)
                .orderBy("created_at");
        }

    }
);