const Model = require('../crud/model')


module.exports = new(
    class Detail_routes extends Model {

        constructor() {
            super()
            this.tablename = 'detail_routes';

            this.fieldname = [
                'id',
                'place_name',
                'name_routes',
                'is_lock',
                'routes_id'
            ]

            this.protectedfiled = []

            this.mapingandprotected()

        }

    }
);