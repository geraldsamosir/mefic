const Model = require('../crud/model')

//model joining
const Users = require('./Users');
const Reports = require('./Reports');

module.exports = new(
    class Notification_reoprts extends Model {

        constructor() {
            super()
            this.tablename = 'notification_reports';

            this.fieldname = [
                'id',
                'body',
                'report_id',
                'comment_users_id',
                'creator_users_id'
            ]

         

            this.protectedfiled = []

            this.tablejoin = [

                {
                    tablename: Reports.tablename,
                    parent: Reports.tablename + '.' + Reports.fieldname[0],
                    target: this.tablename + '.' + this.fieldname[2]
                },
                {
                    tablename: Users.tablename,
                    parent: Users.tablename + '.' + this.fieldname[0],
                    target: this.tablename + '.' + this.fieldname[3]
                }
            ]

            this.mapingandprotected()

        }

        getallnotiftomebyuserid(data) {
            const costum_fieldnameshow = this.fieldnameshow.concat(
                Reports.fieldnameshow,
                Users.fieldnameshow
            )
            const table = this.tablejoin
            return this.joining(costum_fieldnameshow, table)
                   .where(data)
                   .whereRaw(
                        "reports.created_at >= date_sub(NOW(), interval 1 hour)"
                    )
                   .orderBy("notification_reports.id","desc")
        }

    }
);