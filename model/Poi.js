const Model = require('../crud/model')


module.exports = new(
    class POI extends Model {

        constructor() {
            super()
            this.tablename = 'poi';

            this.fieldname = [
                'id',
                'geometry',
                'icon',
                'opening_hours',
                'types',
                'rating',
                'vicinity'
            ]

            this.protectedfiled = []

            this.mapingandprotected()

        }

    }
);