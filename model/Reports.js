const Model = require('../crud/model')


//model join
const Category_reports = require('./Category_reports')
const Users = require('./Users')


module.exports = new(
    class Report extends Model {

        constructor() {
            super()
            this.tablename = 'reports';

            this.fieldname = [
                'id',
                'name_report',
                'photo_url',
                'lat',
                'lng',
                'like',
                'dislike',
                'created_at',
                'category_report_id',
                'users_id',
                'likedislike_id'
            ]

            this.protectedfiled = []

            this.tablejoin = [

                {
                    tablename: Category_reports.tablename,
                    parent: Category_reports.tablename + '.' + this.fieldname[0],
                    target: this.tablename + '.' + this.fieldname[8]
                },
                {
                    tablename: Users.tablename,
                    parent: Users.tablename + '.' + this.fieldname[0],
                    target: this.tablename + '.' + this.fieldname[9]
                }
            ]

            this.mapingandprotected()

        }

        reportdetailall() {
            const costum_fieldnameshow = this.fieldnameshow.concat(
                Category_reports.fieldnameshow,
                Users.fieldnameshow
            )
            const table = this.tablejoin
            return this.joining(costum_fieldnameshow, table)
        }

        filter_report_Detail(data) {
            //data must object
            const costum_fieldnameshow = this.fieldnameshow.concat(
                Category_reports.fieldnameshow,
                Users.fieldnameshow
            )
            const table = this.tablejoin
            return this.joining(costum_fieldnameshow, table)
                .where(data);
        }


        filterbytimeminimal(data) {
            return this.all()
                .where(
                    "created_at", ">", data.minimaltime
                )
        }

         sum_like_dislike(data){
            const costum_fieldnameshow = this.fieldnameshow
            const table = this.tablejoin
            return this.joining(costum_fieldnameshow, table)
                    .sum('like as totallike')
                    .sum('dislike as totaldislike')
                    .where(data)
        }

    }
);