const Model = require('../crud/model')

//model joining
const Detail_routes = require('./Detail_routes');

module.exports = new(
    class Routes extends Model {

        constructor() {
            super()
            this.tablename = 'routes';

            this.fieldname = [
                'id',
                'name',
                'created_users_id'
            ]

            this.protectedfiled = []


            this.mapingandprotected()

        }
        costumcreate(data){
            return this.create(data)
                   .returning('id')
        }

    }
);