const Model = require('../crud/model')

module.exports = new(
    class Users extends Model {
        constructor() {
            super()
            this.tablename = 'users'
            this.fieldname = [
                'id',
                'username',
                'email',
                'gender',
                'age',
                'phone',
                'address',
                'urlphoto',
                'privacy'
            ]

            this.protectedfiled = [
                'password',
                'token',
                'roles'
            ]

            this.mapingandprotected()
            
        }
    }
)