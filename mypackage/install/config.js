//instruksi: "silahkan di isi sesuai path folder anda",
module.exports = {

    controller: "../controller",
    model: "../model",
    router: "../router",
    socket: "../socket",
    midleware: "../midleware",
    packages: '../helper',
    api: '../helper/api',
    sockets: '../helper/socket',
    public: '../public',
    frontend: '../public/client',

}