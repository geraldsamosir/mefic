//instruksi: "silahkan di isi sesuai path folder anda",
module.exports = {

    controller: "../controller",
    model: "../model",
    router: "../router",
    socket: "../socket",
    packages: '../packages',
    api: '../packages/api',
    frontend: '../packages/frontend',
    sockets: '../packages/socket',
    midleware: "../midleware",
}