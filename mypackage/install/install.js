const fs = require('fs')

const conf = require('./config')

module.exports = {
    doinstall: function() {
        const folder = Object.values(conf)
        folder.map((data, index) => {
            try {
                if (!fs.existsSync(data)) {
                    fs.mkdirSync(data);
                }
            } catch (error) {
                console.log(data + 'sudah ada')
            }

        })
    },
    isinstall: function() {
        return conf.controller != ''
    },
    isfolderexis: function() {
        const folder = Object.values(conf)
        const result = folder.map((data, index) => {
            return ((fs.existsSync(data)) ?
                { nama: data, status: true } :
                { nama: data, status: false })
        })


        const error = result.find(function(data) {
            return data.status == false
        }) || ""

        return {
            _result: result,
            _error: ((error == "") ? true : false)
        }
    }

}