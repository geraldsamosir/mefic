const fs = require('fs')
const install = require('./install/install')
const rmdir = require('rmdir');


//source
const frontend = require('./src/frontend')
const api = require('./src/api')
const socket = require('./src/socket')

const args = process.argv.slice(2);

switch (args[0]) {
    case 'install':
        //controller
        install.doinstall()
        break;

    case 'api':
        api.clone(args[1], args[2])
        break;

    case 'frontend':
        frontend.clone(args[1], args[2])
        break;
    case 'socket':
        socket.clone(args[1], args[2])
        break;

    case '-h':
        fs.readFile('./info/help.txt', 'utf8', function(err, data) {
            console.log("\n" + data + '\n')
        })
        break;

    default:
        const result = install.isfolderexis()
        if (install.isinstall() && result._error != false) {
            fs.readFile('./info/logo.txt', 'utf8', function(err, data) {
                console.log("\n\n" + data)
                console.log('\n \n perlu bantuan gunakan perintah node package -h \n\n')
                console.log('created by : Gerald Halomoan Samosir \n')
            })
        } else if (!install.isinstall() || result._error == false) {
            console.log("\n\n Silahkan isi configurasi di ./install/config.js")
            console.log("cek status path folder tidak di temukan (false) : \n")
            console.log(result._result)
            console.log("\n apabila sudah di isi kemudian node package install cek dan pastikan " +
                "kembali path masih false atau tidak \n\n")
        }
        break;

}