const command = require('node-cmd')

module.exports = {
    clone: (url, name) => {
        command.get(
            `git clone ` + url + ` ../helper/api/` + name,

            function(data, err, stderr) {
                if (!err) {
                    console.log('berhasil di clone ' + name)
                } else {
                    console.log('error', err)
                }

            }
        );

    },
}