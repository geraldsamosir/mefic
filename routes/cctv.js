const express = require('express');
const router = express.Router();


//controller
const cctv_controller = require('../controller/cctv');
const cctv_group = require('../controller/cctv_group');

//socket
const cctv_socket = require('../socket/cctv')



//get controller

//get all  cctv
router.get('/', cctv_controller.modelpath, cctv_controller.all);

router.get('/:id/edit', cctv_controller.modelpath, cctv_controller.get);

router.post('/', cctv_controller.modelpath, cctv_controller.create);

router.put('/:id', cctv_controller.modelpath, cctv_controller.update);

router.put('/broken/:id', cctv_controller.modelpath, cctv_socket.notification ,cctv_controller.addreportcctvstatus, cctv_controller.update)

router.delete('/:id', cctv_controller.modelpath, cctv_controller.delete);

router.get('/filterbygroupid/:cctv_groups_id', cctv_controller.modelpath, cctv_controller.get);



//get all group cctv
router.get('/group/', cctv_group.modelpath, cctv_group.all);

router.post('/group/', cctv_group.modelpath, cctv_group.create);

router.get('/group/:id/edit', cctv_group.modelpath, cctv_group.get)

router.put('/group/:id', cctv_group.modelpath, cctv_group.update);

router.delete('/group/:id', cctv_group.modelpath, cctv_group.delete);




module.exports = router