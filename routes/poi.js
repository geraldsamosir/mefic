const express = require('express');
const router = express.Router();

//controller 
const poi_controller = require('../controller/poi')

router.get('/', poi_controller.modelpath, poi_controller.all);

router.get('/:types', poi_controller.modelpath, poi_controller.get)

//filter get poi optimal  ?mylocation
router.get('/search/:types', poi_controller.modelpath, poi_controller.getpoi)

router.get('/:id/edit', poi_controller.modelpath, poi_controller.get)

router.post('/', poi_controller.modelpath, poi_controller.create)

router.put('/:id', poi_controller.modelpath, poi_controller.update)

router.delete('/:id', poi_controller.modelpath, poi_controller.delete)

router.get('/category/all', poi_controller.modelpath,poi_controller.category )

// router.get('/coba/testing', (req, res) => {
//     res.io.emit("socketToMe", "users");
//     res.send("test");
// })


module.exports = router