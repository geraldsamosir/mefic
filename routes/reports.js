const express = require('express');
const router = express.Router();

//get controller
const category_reports_controller = require('../controller/category_reports');
const reports_controller = require('../controller/report');
const comment_reports_controller = require('../controller/comments_reports');
const notification_controler = require('../controller/notifikasi');

//midleware
const reportfile_midleware = require('../middleware/reportfile')

//socket
const comment_reports_socket = require('../socket/Comment_reports')
const reports_socket = require('../socket/report')

//get all report
router.get('/', reports_controller.modelpath, reports_controller.reportdetailall);

router.get('/:id/detail', reports_controller.modelpath, reports_controller.getbyid);



router.post('/',
    reportfile_midleware.upload_single(reportfile_midleware.env()),
    reportfile_midleware.getfilename,
    reports_controller.modelpath,
    reports_socket.notification,
    reports_controller.create);

router.put('/:id',   
    reports_controller.modelpath, 
    reports_socket.notificationupdate, 
    reports_controller.update);


//costum for mobile like and dislike
router.put('/:id/like' ,
    reports_controller.modelpath , 
    reports_socket.notificationupdate,
    reports_controller.mobilereportlike)

router.put('/:id/dislike' ,
    reports_controller.modelpath , 
    reports_socket.notificationupdate,
    reports_controller.mobilereportdislike)


//type like or dislike
router.put('/:id/cancel/:type',
    reports_controller.modelpath,
    reports_socket.notificationupdate,
    reports_controller.mobilecancelrating)
//......end costum


router.delete('/:id', reports_controller.modelpath, reports_controller.delete);


//get all kategory reports
router.get('/category/', category_reports_controller.modelpath, category_reports_controller.all);

router.get('/category/:id/edit', category_reports_controller.modelpath, category_reports_controller.get);

router.post('/category/', category_reports_controller.modelpath, category_reports_controller.create);

router.put('/category/:id', category_reports_controller.modelpath, category_reports_controller.update);

router.delete('/category/:id', category_reports_controller.modelpath, category_reports_controller.delete);


//get comment belongs to reports
router.get('/comment/:report_id',  comment_reports_controller.modelpath, comment_reports_controller.getbyreport)

router.get('/comment/:id/edit', comment_reports_controller.modelpath, comment_reports_controller.get)

router.post('/comment', 
    comment_reports_controller.modelpath,
    notification_controler.costumcreate,
    comment_reports_socket.firebase,
    comment_reports_socket.notification,
    comment_reports_controller.create);

// router.put('/comment', comment_reports_controller.modelpath, comment_reports_controller.update);

 router.delete('/comment/:id', comment_reports_controller.modelpath, comment_reports_controller.delete);

//notif

router.get("/notfication/:creator_users_id", notification_controler.getbyusers)

router.delete("/notfication/:id", notification_controler.modelpath , notification_controler.delete)



module.exports = router