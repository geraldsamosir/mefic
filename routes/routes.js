const express = require('express');
const router = express.Router();

//socket
const confsocket =  require("../socket/config");

//controller
const routes_controller = require('../controller/routes')
const detail_routes = require('../controller/detail_routes')


//routes
router.get('/', routes_controller.modelpath, routes_controller.all);

router.get('/:created_users_id/edit', routes_controller.modelpath, routes_controller.get);



router.post('/', routes_controller.modelpath, routes_controller.costumcreate);

router.put('/:id', routes_controller.modelpath, routes_controller.update);

router.delete('/:id', routes_controller.modelpath, routes_controller.delete);

//conf value of googlemap and report

router.get('/conf/confroute',detail_routes.get_conf_value);

router.put('/conf/confroute', confsocket.notification ,detail_routes.update_conf_value);


//all route in routes [item destination] 
router.get('/destination/:routes_id', detail_routes.modelpath, detail_routes.get)

router.get('/destination/:id/edit', detail_routes.modelpath, detail_routes.get)

//use this ?/start=test&end=test2,test3,test4 ...
router.get('/destination/filter/bydistance/',
    detail_routes.modelpath,
    detail_routes.get_all_routes,
    detail_routes.get_routes_by_distance);

router.get('/destination/filter/bytime/',
    detail_routes.modelpath,
    detail_routes.get_all_routes,
    detail_routes.get_routes_by_time);

router.get('/destination/filter/bytraffic/',
    detail_routes.modelpath,
    detail_routes.get_all_routes,
    detail_routes.get_routes_by_traffic);

router.get('/destination/filter/~bytraffic/',
    detail_routes.modelpath,
    detail_routes.get_all_routes_bylatlng,
    detail_routes.get_routes_by_traffic
)    

router.post('/destination/', detail_routes.modelpath, detail_routes.create)


router.delete('/destination/:routes_id', detail_routes.modelpath, detail_routes.delete);


module.exports = router