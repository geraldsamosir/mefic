const express = require('express');
const router = express.Router();

const helper_userscontroller = require('../helper/api/auth/controller/users')
const authcontroller = require('../helper/api/auth/controller/auth')

const authmidleware = require('../helper/api/auth/midleware/auth')
const profilefilemidleware = require('../middleware/profilfile')


//controller report
const reports_controller = require('../controller/report');

/**
 * User data router get user data, 
 * udpate as role as users
 * using auth helper for user data
 */


/**
 * this code is role by admin
 */

//get all users 
router.get('/', authmidleware.is_auth, authmidleware.is_admin, helper_userscontroller.all)

//delete users
router.delete('/:id', authmidleware.is_auth, authmidleware.is_admin, helper_userscontroller.delete)


/**
 * this code is role by user 
 */

//update  profile

//update data
router.put('/:id',
    authmidleware.is_auth,
    helper_userscontroller.updateprofil
)

//update data with image
router.put('/withimage/:id',
    profilefilemidleware.upload_single(profilefilemidleware.env()),
    profilefilemidleware.getfilename,
    profilefilemidleware.deleteimage,
    authmidleware.is_auth,
    helper_userscontroller.updateprofil
)

/**
 * this code role by user and admin and guest
 */
//get user 
router.get('/:email',  reports_controller.sum_like_dislike, helper_userscontroller.getbyemail);




/**
 * Authentication process
 * form registration ,login and update passowrd 
 * using auth helper
 */


//access token to update password (token send to email)
router.get('/password/:token', authcontroller.getdatatoken)

//register router
router.post('/',
//    profilefilemidleware.upload_single(profilefilemidleware.env()),
//    profilefilemidleware.getfilename,
    authmidleware.is_exist,
    authcontroller.register);


//forgotpassword to update password (token send to email)
router.get('/forgotpassword/:email', authmidleware.is_mail_exist, authcontroller.forgotpassword);
//for mobile reason
router.post('/forgotpassword/:email', authmidleware.is_mail_exist, authcontroller.forgotpassword);

//update password
router.put('/password/update', authcontroller.updatepassword )



//login router
router.post('/login', authcontroller.login)




module.exports = router;