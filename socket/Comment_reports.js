const request = require('request');

//firebase conf
const firebaseconf = require("./firebase/config")


module.exports = new(
    class comment_report {
        notification(req, res, next) {
            res.io.emit("comment", req.body);
            
            delete req.body.users_name;
            delete req.body.users_urlphoto;
            delete req.body.conten_creator_email;   
            delete  req.body.creator_users_id;
            next();
        }

        firebase(req,res,next){
            let data  = req.body;
            request({
                uri :firebaseconf.uri  ,
                method :"POST",
                headers:firebaseconf.headers,
                json :{
                    "to" :"/topics/"+data.creator_users_id,
                    "priority":"high",
                    "data" :{
                        //notif_id get from notification id created
                        "notif_id":data.notifid,
                        "title" : data.users_name+" mengomentari "+ data.body,
                        "body" :data.body,
                        "report_id": data.report_id,
                        "comment_user_id": data.comment_users_id
                    }
                }
            },(err,response,body)=>{
                console.log(body)
                delete req.body.notifid
                next(); 
            })
        }
    }
)